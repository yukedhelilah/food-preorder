$(document).ready(function() {
    get_city();
	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Profile/get_details/_',
    data: {
      id: kurirID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    	$('#id').val(response.data.id);
    	$('#companyName').val(response.data.companyName); empty('companyName');
    	$('#username').val(response.data.username); empty('username');
    	$('#usernameCopy').val(response.data.username);
    	$('#cityID').val(response.data.cityID); empty('cityID');
    	$('#address').val(response.data.address); empty('address');
    	$('#phone').val(response.data.phone); empty('phone');
    	$('#note').val(response.data.note); empty('note');
    	$('#contactName').val(response.data.contactName); empty('contactName');
    	$('#contactEmail').val(response.data.contactEmail); empty('contactEmail');
    	$('#contactEmailCopy').val(response.data.contactEmail);
    	if (response.data.contactEmail != '') {$('#iscontactEmail').val(1);}
    	$('#contactWa').val(response.data.contactWa); empty('contactWa');
    	$('#contactWaCopy').val(response.data.contactWa);
    	if (response.data.contactWa != '') {$('#iscontactWa').val(1);}
    },
  });
}

function get_city(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Profile/get_city/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        $('#cityID').append(`
          <option value="`+val.id+`">`+val.cityName+`</option>
        `);
      });
    },
  });
}

function requiredField(val,field){
    var value 	= $.trim(val);

    if (field == 'username'){
    	if(value.length > 2){
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Profile/check/_',
	            data: {
	            	check : value,
	            	type  : 'username',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
	                	if (value == $('#usernameCopy').val()) {
							valid(field);
							$('#snackbar-error').toast('hide');
	                	} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Username tidak tersedia');
	                	}
	                }
	            }
	        });	
    	} else {
    		invalid(field);
			$('#snackbar-error').toast('hide');
    	}
    } else if (field == 'contactEmail') {
    	var re = /\S+@\S+\.\S+/;
    	if (re.test(value) == true){
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Profile/check/_',
	            data: {
	            	check : value,
	            	type  : 'contactEmail',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
	                	if (value == $('#contactEmailCopy').val()) {
							valid(field);
							$('#snackbar-error').toast('hide');
						} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Email tidak tersedia');
						}
	                }
	            }
	        });
        } else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'contactWa') {
    	if (value.length == 1 && value == '0') { $('#contactWa').val('62');}
    	if (value.length > 9) { 
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Profile/check/_',
	            data: {
	            	check : value,
	            	type  : 'contactWa',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
	                	if (value == $('#contactWaCopy').val()) {
							valid(field);
							$('#snackbar-error').toast('hide');
						} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Nomor whatsapp tidak tersedia');
						}
	                }
	            }
	        });
    	} else { 
    		invalid(field);
    	}
    } else {
    	if(value.length > 2) { 
    		valid(field);
    	} else { 
    		invalid(field);
    	}
    }
}

function valid(field) { $('#is'+field).val(1); }
function invalid(field) { $('#is'+field).val(0); }

function empty(field) { 
    $('#label'+field).removeAttr('class'); 
    if ($('#'+field).val() == '') {
        $('#label'+field).attr('class','color-highlight');
    } else {
        $('#label'+field).attr('class','color-highlight input-style-1-active');
    }
}

function save(){
	if ($('#isusername').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Username tidak tersedia');
	} else if ($('#iscontactEmail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email tidak tersedia');
	} else if ($('#iscontactWa').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor whatsapp tidak tersedia');
	} else if ($('#password').val() != '' && $('#password').val() == '1234') {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin tidak boleh 1234');
    } else if ($('#password').val() != '' && $('#password').val().length < 4) {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin harus 4 digit');
    } else {
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Profile/save/_',
		    data: $('#mainform').serialize(),
		    headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
	            if (response.code == '200') {
					$('#snackbar-success').toast('show');
                    setTimeout(function () {
                        window.location.href = 'setting';
                    }, 100);
	            }else{
					$('#snackbar-error').toast('show');
			        $('#txt-error').html('Gagal, coba lagi nanti.');
	            }
		    },
		});
	}
}

function save_snk(){
	$.ajax({
	    type: 'POST',
	    dataType: 'JSON',
	    url: configUrl + 'Profile/save_snk/_',
	    data: $('#mainform').serialize(),
	    headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    success: function(response) {
            if (response.code == '200') {
				$('#snackbar-success').toast('show');
                setTimeout(function () {
                    window.location.href = 'setting';
                }, 100);
            }else{
				$('#snackbar-error').toast('show');
		        $('#txt-error').html('Gagal, coba lagi nanti.');
            }
	    },
	});
}