$(document).ready(function() {
  get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Home/get_data/_',
    data: {
      id : kurirID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#content').empty();
      if (response.data == null) {
        $('#content').append(`
          <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
            <div class="content" style="margin:5px 15px">
              <table style="width:100%">
                <tr>
                  <td style="padding:5px;text-align:center">Belum ada kategori</td>
                </tr>
              </table>
            </div>
          </div>
      `); 
      } else {
        $.each(response.data, function( i, val ) {
          $('#content').append(`
            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
              <div class="content" style="margin:5px 10px">
                <table style="width:100%">
                  <tr>
                    <td style="padding:5px;width:45%;color:black;font-size:13px;line-height:1.4">`+val.startFrom+`<td>
                    <td style="padding:5px;width:45%;color:black;font-size:13px;line-height:1.4">`+val.destination+`</td>
                    <td style="padding:5px;width:10%;color:black;font-size:13px">`+numberFormat(val.price)+`</td>
                  </tr>
                </table>
              </div>
            </div>
          `);
        });
      }
    },
  });
}