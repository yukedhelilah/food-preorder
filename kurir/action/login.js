var configUrl       = 'https://dapur123.com/server.php/Kurir/';

$('input[name="contactWa"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('kurir_id')) != null && localStorage.getItem(btoa('kurir_username')) != null && localStorage.getItem(btoa('kurir_expired')) != null && localStorage.getItem(btoa('kurir_token')) != null) {
        window.location.href = 'home';
    }
});

function requiredField(val){
    var value   = $.trim(val);
    var number  = value[0];

    if (number == '0') { $('#contactWa').val('62'+$('#contactWa').val().substr(1));}
}

function login(){  
    $('#login').css('display', 'none');
    $('#loadLogin').css('display', 'block');
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/login_pin/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            contactWa   : $("#contactWa").val(),
            password    : $("#password").val(),
        },
        success: function(response) {
            if (response.code == '200') {   
                localStorage.setItem(btoa('kurir_id'), btoa(response.data.id));
                localStorage.setItem(btoa('kurir_username'), btoa(response.data.username));
                localStorage.setItem(btoa('kurir_companyName'), btoa(response.data.companyName));
                localStorage.setItem(btoa('kurir_contactWa'), btoa(response.data.contactWa));
                localStorage.setItem(btoa('kurir_contactEmail'), btoa(response.data.contactEmail));
                localStorage.setItem(btoa('kurir_token'), btoa(response.data.token));
                localStorage.setItem(btoa('kurir_expired'), btoa(response.data.expired));
                if (response.data.password == 'default') {
                    window.location.href = 'change_password';
                } else {
                    window.location.href = 'home';
                }
            } else{
            	alert('Nomor anda belum terdaftar');
                $('#login').css('display', 'block');
                $('#loadLogin').css('display', 'none');
            }
        },
    });
}

function register(){
    window.location.href = '../pages/register';
}

function login_otp() {
    window.location.href = 'login_otp';
}