var configUrl       = 'https://dapur123.com/server.php/Kurir/';

$('input[name="contactWa"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('kurir_id')) != null && localStorage.getItem(btoa('kurir_username')) != null && localStorage.getItem(btoa('kurir_expired')) != null && localStorage.getItem(btoa('kurir_token')) != null) {
        window.location.href = 'home';
    }
});

function requiredField(val){
    var value   = $.trim(val);
    var number  = value[0];

    if (number == '0') { $('#contactWa').val('62'+$('#contactWa').val().substr(1));}
}

function login(){  
    $('#login').css('display', 'none');
    $('#loadLogin').css('display', 'block');
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            contactWa   : $("#contactWa").val(),
        },
        success: function(response) {
            if (response.code == '200') {   
                $('#otp').val(response.data.otp);
                $('#partitioned').val('');
                $('#a_resend').html('Kirim ulang');
                $('#div-login').css('display','none');
                $('#div-verifikasi').css('display','block');
                $('#theTarget').html(59);
                $('#theTargetReal').html(59);
                $('#a_resend').hide();
                var timer = setInterval(function() {
                    var count = parseInt($('#theTargetReal').html());
                    if (count !== 0) {
                        if(count < 10){
                            $('#theTarget').html('0'+(count - 1));
                        }else{
                            $('#theTarget').html(count - 1);
                        }
                        $('#theTargetReal').html(count - 1);
                    } else {
                      clearInterval(timer);
                      $('#a_resend').show();
                    }
                }, 1000);

                $('#a_resend').on('click', function() {
                    sendOTP(response.data.staffID, response.data.staffEmail, response.data.staffWA);
                });

                $('#verifikasi').click(function() {
                    if ($('#partitioned').val() == atob($('#otp').val())) {
                        localStorage.setItem(btoa('kurir_id'), btoa(response.data.id));
                        localStorage.setItem(btoa('kurir_username'), btoa(response.data.username));
                        localStorage.setItem(btoa('kurir_companyName'), btoa(response.data.companyName));
                        localStorage.setItem(btoa('kurir_contactWa'), btoa(response.data.contactWa));
                        localStorage.setItem(btoa('kurir_contactEmail'), btoa(response.data.contactEmail));
                        localStorage.setItem(btoa('kurir_token'), btoa(response.data.token));
                        localStorage.setItem(btoa('kurir_expired'), btoa(response.data.expired));
                        if (response.data.password == 'default') {
                            window.location.href = 'change_password';
                        } else {
                            window.location.href = 'home';
                        }
                    } else {
                        alert('Kode OTP salah');
                        $('#partitioned').val('');
                    }
                });
            } else{
            	alert('Nomor anda belum terdaftar');
                $('#login').css('display', 'block');
                $('#loadLogin').css('display', 'none');
            }
        },
    });
}

function register(){
    window.location.href = '../pages/register';
}

function sendOTP(id, email, wa){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/sendOTP/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token   : '1b787b70ed2e0de697d731f14b5da57b',
            id      : id,
            email   : email,
            wa 		: wa,
        },
        beforeSend: function() {
            $('#a_resend').html('Sedang memuat');
        },
        success: function(response) {
            if (response.code == '200') {   
                $('#otp').val(response.otp);
                $('#partitioned').val('');
                $('#a_resend').html('Kirim ulang');
                $('#div-login').css('display','none');
                $('#div-verifikasi').css('display','block');
                $('#theTarget').html(59);
                $('#theTargetReal').html(59);
                $('#a_resend').hide();
                var timer = setInterval(function() {
                    var count = parseInt($('#theTargetReal').html());
                    if (count !== 0) {
                        if(count < 10){
                            $('#theTarget').html('0'+(count - 1));
                        }else{
                            $('#theTarget').html(count - 1);
                        }
                        $('#theTargetReal').html(count - 1);
                    } else {
                      clearInterval(timer);
                      $('#a_resend').show();
                    }
                }, 1000);
            }else{
                alert('Gagal, coba lagi nanti');
            }
        },
    });
}

function login_pin() {
    window.location.href = 'login';
}