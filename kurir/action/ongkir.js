$(document).ready(function() {
  get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Ongkir/get_data/_',
    data : {
      id : kurirID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) { 
      $('#content').empty();
      if (response.data == null) {
        $('#content').append(`
          <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
            <div class="content" style="margin:10px 15px;text-align:center">
              <h5 class="font-14 font-500 mb-0">Belum ada daftar ongkos kirim</h5>
            </div>
          </div>
        `);
      } else {
        $.each(response.data, function( i, val ) {
        	$('#content').append(`
            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
              <div class="content" style="margin:10px 15px">
                <table style="width:100%">
                  <tr>
                    <td style="width:80%;color:black;font-size:14px">
                      <h5 class="font-13 font-500 mb-0">`+val.startFrom+` - `+val.destination+`</h5>
                      <h5 class="font-13 font-500 mb-0">Rp `+numberFormat(val.price)+`</h5>
                    </td>
                    <td style="text-align:right;width:20%;color:black;font-size:13px">
                      <a href="javascript:;" onclick="edit_data(`+val.id+`, '`+val.startFrom+`', '`+val.destination+`', `+val.price+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                        <i class="fa fa-edit" style="font-size: 10px"></i>
                      </a>&nbsp
                      <a href="javascript:;" onclick="delete_data(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                        <i class="fa fa-trash" style="font-size: 10px"></i>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          `);
        });
      }
    },
  });
}

function new_data(){
  window.location.href = '#';
  $('#act').val('add');
  $('#id').val('');
  $('#kurirID').val(kurirID);
  $('#startFrom').val('');
  $('#span-startFrom').removeAttr('class');
  $('#span-startFrom').attr('class','color-highlight');
  $('#destination').val('');
  $('#span-destination').removeAttr('class');
  $('#span-destination').attr('class','color-highlight');
  $('#price').val('');
  $('#span-price').removeAttr('class');
  $('#span-price').attr('class','color-highlight');
  $('#sheets_ongkir').showMenu();
}

function edit_data(id, startFrom, destination, price){
  window.location.href = '#';
  $('#act').val('edit');
  $('#id').val(id);
  $('#kurirID').val(kurirID);
  $('#startFrom').val(startFrom);
  $('#span-startFrom').removeAttr('class');
  $('#span-startFrom').attr('class','color-highlight input-style-2 input-style-1-active');
  $('#destination').val(destination);
  $('#span-destination').removeAttr('class');
  $('#span-destination').attr('class','color-highlight input-style-2 input-style-1-active');
  $('#price').val(numberFormat(price));
  $('#span-price').removeAttr('class');
  $('#span-price').attr('class','color-highlight input-style-2 input-style-1-active');
  $('#sheets_ongkir').showMenu();
}

function save(){
  if ($('#startFrom').val() == '') {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Lokasi penjemputan harus diisi'); 
  } else if ($('#destination').val() == '') {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Lokasi pengantaran harus diisi'); 
  } else if ($('#price').val() == '') {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Harga harus diisi'); 
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Ongkir/save/_',
      data: $("#mainform").serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        $('#sheets_ongkir').hideMenu();
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          get_data();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}

function delete_data(id){
  if (confirm('Apa anda yakin menghapus lokasi tujuan ini?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Ongkir/delete/_',
      data: {
        id : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          get_data();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}
