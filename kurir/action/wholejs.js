var kurirID         = atob(localStorage.getItem(btoa("kurir_id")));
var kurirUsername   = atob(localStorage.getItem(btoa("kurir_username")));
var kurirName       = atob(localStorage.getItem(btoa("kurir_companyName")));
var kurirWa         = atob(localStorage.getItem(btoa("kurir_contactWa")));
var kurirEmail      = atob(localStorage.getItem(btoa("kurir_contactEmail")));
var token           = atob(localStorage.getItem(btoa("kurir_token")));
var expired         = localStorage.getItem(btoa("kurir_expired"));
var exp             = new Date(atob(expired));
var now             = new Date();
var configUrl       = 'http://dapur123.com/server.php/Kurir/';

var bulan           = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
var bulans          = ["JAN", "FEB", "MAR", "APR", "MEI", "JUN", "JUL", "AGU", "SEP", "OKT", "NOV", "DES"];
var days            = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

if(kurirID == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
  removeLocalstorage();
  window.location.href = '../pages/login';
}

$(document).ready(function() {});

function home(){
  window.location.href = 'home';
}

function ongkir(id) {
  window.location.href = 'ongkir';
}

function setting(){
  window.location.href = 'setting';
}

function account() {
  window.location.href = 'account';
}

function snk() {
  window.location.href = 'snk';
}

function dateFormat(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString();
}

function dateFormat2(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  return date + ' ' + bulans[format.getMonth()];
}

function dateFormat3(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return days[format.getDay()] + ', ' + date + ' ' + bulan[format.getMonth()] + ' ' + format.getFullYear();
}

function dateFormat4(date) {
  var format  = new Date(date);
  var date    = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  var hours   = format.getHours() < 10 ? '0'+format.getHours() : format.getHours();
  var minutes = format.getMinutes() < 10 ? '0'+format.getMinutes() : format.getMinutes(); 

  return date + ' ' + bulans[format.getMonth()] + ' ' + hours +`:`+ minutes;
}

function dateFormat5(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return days[format.getDay()] + ', ' + date + ' ' + bulan[format.getMonth()];
}

function dateFormat6(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return date + ' ' + bulan[format.getMonth()] + ' ' + format.getFullYear();
}

function dateFormat7(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  var hours   = format.getHours() < 10 ? '0'+format.getHours() : format.getHours();
  var minutes = format.getMinutes() < 10 ? '0'+format.getMinutes() : format.getMinutes(); 

  return hours + ':' + minutes + ' ' + date + ' ' + bulans[format.getMonth()];
}

function numberFormat(nStr){
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function getUrlParameter(sParam){
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
}

function logout() {
  if (confirm("Anda yakin ingin keluar?")) {
    removeLocalstorage();
    window.location.href = 'login';
  } 
}

function removeLocalstorage(){
  localStorage.removeItem(btoa('kurir_id'));
  localStorage.removeItem(btoa('kurir_username'));
  localStorage.removeItem(btoa('kurir_companyName'));
  localStorage.removeItem(btoa('kurir_contactWa'));
  localStorage.removeItem(btoa('kurir_contactEmail'));
  localStorage.removeItem(btoa('kurir_token'));
  localStorage.removeItem(btoa('kurir_expired'));
}