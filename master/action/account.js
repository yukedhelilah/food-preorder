$(document).ready(function() {
    setTimeout(function () {
        if (staffName == 'MASTER') {
            $('#menu-staff').css('display','block');
        } else {
            $('#menu-staff').css('display','none');
        }
    }, 100);
    
	get_data();

    $('#changePassword').click(function(){
	    if($(this).is(':checked')){
            $('#isconfirmPassword').val(0);
	    } else {
	    	$('#password').val(''); empty('password');
            $('#confirmPassword').val(''); empty('confirmPassword');
            $('#isconfirmPassword').val(1);
	    }
	});
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Staff/get_details/_',
    data: {
        id: staffID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    	$('#id').val(response.data.id);
    	$('#name').val(response.data.name); empty('name');
    	$('#email').val(response.data.email); empty('email');
    	$('#emailCopy').val(response.data.email);
        $('#no_wa').val(response.data.no_wa); empty('no_wa');
        $('#no_waCopy').val(response.data.no_wa);
        if (response.data.name == 'MASTER') {
            $('#name').prop('readonly',true); 
            $('#labelname').css('background','linear-gradient(white, #e9ecef)'); 
        }
    },
  });
}

function requiredField(val,field){
    var value 	= $.trim(val);

    if (field == 'email') {
        var re = /\S+@\S+\.\S+/;
        if (value.length > 0) {
            if (re.test(value) == true){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    url: configUrl + 'Register/check/_',
                    data: {
                        check : value,
                        type  : 'email',
                    },
                    success: function (responses) {
                        if(responses.code == 200 && responses.total == 0){
                            valid(field);
                            $('#snackbar-error').toast('hide');
                        }else {
                            if (value == $('#emailCopy').val()) {
                                valid(field);
                                $('#snackbar-error').toast('hide');
                            } else {
                                invalid(field);
                                $('#snackbar-error').toast('show');
                                $('#txt-error').html('Email telah digunakan');
                            }
                        }
                    }
                });
            } 
        } else { 
            invalid(field);
            $('#snackbar-error').toast('hide');
        }
    } else if (field == 'no_wa') {
        var number  = value[0];

        if (number == '0') { $('#no_wa').val('62'+$('#no_wa').val().substr(1));}
        if (value.length > 9) { 
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: configUrl + 'Register/check/_',
                data: {
                    check : value,
                    type  : 'no_wa',
                },
                success: function (responses) {
                    if(responses.code == 200 && responses.total == 0){
                        valid(field);
                        $('#snackbar-error').toast('hide');
                    }else {
                        if (value == $('#no_waCopy').val()) {
                            valid(field);
                            $('#snackbar-error').toast('hide');
                        } else {
                            invalid(field);
                            $('#snackbar-error').toast('show');
                            $('#txt-error').html('Nomor Whatsapp telah digunakan');
                        }
                    }
                }
            });
        } else { 
            invalid(field);
        }
    }
}

function valid(field) { $('#is'+field).val(1); }
function invalid(field) { $('#is'+field).val(0); }

function empty(field) { 
    $('#label'+field).removeAttr('class'); 
    if ($('#'+field).val() == '') {
        $('#label'+field).attr('class','color-highlight');
    } else {
        $('#label'+field).attr('class','color-highlight input-style-1-active');
    }
}

function save(){
	if ($('#isemail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email tidak valid');
	} else if ($('#isno_wa').val() == 0) {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor Whatsapp tidak valid');
    } else if ($('#password').val() != '' && $('#password').val() == '1234') {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin tidak boleh 1234');
    } else if ($('#password').val() != '' && $('#password').val().length < 4) {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin harus 4 digit');
    } else {
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Staff/save/_',
		    data: $('#mainform').serialize(),
		    headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
	            if (response.code == '200') {
					$('#snackbar-success').toast('show');
                    setTimeout(function () {
                        window.location.href = 'setting';
                    }, 100);
	            }else{
					$('#snackbar-error').toast('show');
			        $('#txt-error').html('Failed, try again later.');
	            }
		    },
		});
	}
}

function staff(){
    window.location.href = 'staff';
}