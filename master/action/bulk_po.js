$(document).ready(function() {
  $('#restoID').val(restoID);
  get_category();

  for (var i = getYear(); i <= getYearMax(); i++) {
    $('#yearFrom').append(`
      <option value="`+i+`">`+i+`</option>
    `);
    $('#yearTo').append(`
      <option value="`+i+`">`+i+`</option>
    `);
  }

  for (var i = getMonth(); i <= getMonthMax(); i++) {
    $('#monthFrom').append(`
      <option value="`+i+`">`+bulan[i-1]+`</option>
    `);
    $('#monthTo').append(`
      <option value="`+i+`">`+bulan[i-1]+`</option>
    `);
  }

  var d;
  if (getMonth() == 2) {
    d = 28;
  } else if (getMonth() == 4 || getMonth() == 6 || getMonth() == 4 || getMonth() == 9 || getMonth() == 11) {
    d = 30;
  } else {
    d = 31;
  }

  var dateMax = getDate() <= getDateMax() ? getDateMax() : d;
  for (var i = getDate(); i <= dateMax; i++) {
    var date    = i.toString().length == 2 ? i : `0`+i;
    $('#dateFrom').append(`
      <option value="`+i+`">`+date+`</option>
    `);
    $('#dateTo').append(`
      <option value="`+i+`">`+date+`</option>
    `);
  }

  $("#yearFrom").change(function () {
    $('#monthFrom').empty();
    $('#dateFrom').empty();
    if ($(this).val() != getYear()) {
      for (var i = 1; i <= getMonthMax(); i++) {
        $('#monthFrom').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthFrom').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      for (var i = getMonth(); i <= getMonthMax(); i++) {
        $('#monthFrom').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthFrom').val() ? getDateMax() : d2;
      for (var i = getDate(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }


    $('#yearTo').empty();
    $('#monthTo').empty();
    $('#dateTo').empty();
    for (var i = $('#yearFrom').val(); i <= getYearMax(); i++) {
      $('#yearTo').append(`
        <option value="`+i+`">`+i+`</option>
      `);
    }
    if ($(this).val() != getYear()) {
      for (var i = 1; i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      for (var i = getMonth(); i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = getDate(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $('#monthFrom').change(function () {
    $('#dateFrom').empty();
    $('#dateTo').empty();
    if ($('#yearFrom').val() == getYear() && $('#monthFrom').val() == getMonth()) {
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthFrom').val() ? getDateMax() : d2;
      for (var i = getDate(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getDate() != $('#monthFrom').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }

    $('#monthTo').empty();
    if ($('#yearTo').val() > $('#yearFrom').val()) {
      for (var i = 1; i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    } else {
      for (var i = $('#monthFrom').val(); i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    }
  });

  $('#dateFrom').change(function () {
    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = $('#dateFrom').val(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $("#yearTo").change(function () {
    $('#monthTo').empty();
    if ($("#yearTo").val() > $("#yearFrom").val()) {
      for (var i = 1; i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    } else {
      for (var i = $('#monthFrom').val(); i <= getMonthMax(); i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    }

    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = $('#dateFrom').val(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $("#monthTo").change(function () {
    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = $('#dateFrom').val(); i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }

      var dateMax2 = getMonth() != $('#monthTo').val() ? getDateMax() : d2;
      for (var i = 1; i <= dateMax2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });
}); 

function getYear() {
  var format  = new Date();
  return format.getFullYear().toString();
}

function getMonth() {
  var format  = new Date();
  return format.getMonth()+1;
}

function getDate() {
  var format  = new Date();
  return format.getDate();
}

function getYearMax() {
  var maxDate = new Date();
  maxDate.setDate(maxDate.getDate()+14);
  return maxDate.getFullYear().toString();
}

function getMonthMax() {
  var maxDate = new Date();
  maxDate.setDate(maxDate.getDate()+14);
  return maxDate.getMonth()+1;
}

function getDateMax() {
  var maxDate = new Date();
  maxDate.setDate(maxDate.getDate()+14);
  return maxDate.getDate();
}

function get_category(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_data_by_resto/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        $('#categoryID').append(`
          <option value="`+val.id+`">`+val.categoryName+`</option>
        `);
      });
    },
  });
}

function save_data(){
  if($('#allotment').val() == ''){
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Slot harus diisi');
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/save_bulk_po/_',
      data: $('#mainform').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          setTimeout(function () {
            window.location.href = 'product';
          }, 100);
        } else if (response.code == 400) {
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Tidak ada produk pada kategori ini');
        } else {
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}