$(document).ready(function() {
  	get_data();
});

function get_data(){
	$('#content').empty();
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
    	url: configUrl + 'Category/get_data_by_resto/_',
		data: {
			id : restoID,
		},
		headers: {
		    'Content-Type': 'application/x-www-form-urlencoded'
		},
		success: function(response) {
			if (response.data == null) {
				$('#content').append(`
		            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
		              <div class="content" style="margin:5px 15px">
		                <table style="width:100%">
		                  <tr>
		                    <td style="padding:5px;text-align:center">Belum ada kategori</td>
		                  </tr>
		                </table>
		              </div>
		            </div>
		        `);	
			}
		  	$.each(response.data, function( i, val ) {
	          	$('#content').append(`
		            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
		              <div class="content" style="margin:5px 15px">
		                <table style="width:100%">
		                  <tr>
		                    <td style="padding:5px;width:75%">`+val.categoryName+`</td>
		                    <td style="text-align:right;padding:5px;width:25%">
		                      <a href="javascript:;" onclick="edit_data(`+val.id+`, '`+val.categoryName+`')" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
		                        <i class="fa fa-edit" style="font-size: 10px"></i>
		                      </a>&nbsp
		                      <a href="javascript:;" onclick="delete_data(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
		                        <i class="fa fa-trash" style="font-size: 10px"></i>
		                      </a>
		                    </td>
		                  </tr>
		                </table>
		              </div>
		            </div>
	          	`);
		  	});
		},
	});
}

function new_data(){
	window.location.href = '#';
	$('#act').val('add');
	$('#id').val('');
	$('#restoID').val(restoID);
	$('#categoryName').val('');
	$('#span-category').removeAttr('class');
	$('#span-category').attr('class','color-highlight');
	$('#sheets_category').showMenu();
}

function edit_data(id, categoryName){
	window.location.href = '#';
	$('#act').val('edit');
	$('#id').val(id);
	$('#restoID').val(restoID);
	$('#categoryName').val(categoryName);
	$('#span-category').removeAttr('class');
	$('#span-category').attr('class','color-highlight input-style-2 input-style-1-active');
	$('#sheets_category').showMenu();
}

function save_data(){
	$.ajax({
	    type: 'POST',
	    dataType: 'JSON',
	    url: configUrl + 'Category/save/_',
	    data: $("#mainform").serialize(),
	    headers: {
	        'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    success: function(response) {
	    	if (response.code == 200) {
				get_data()
				$('#sheets_category').hideMenu();
				$('#snackbar-success').toast('show');
	    	} else {
	    		alert('failed, try again later');
				$('#sheets_category').hideMenu();
	    	}
	    },
	});
}

function delete_data(id){
	if (confirm('Are you sure to delete this category? Product with this category will be deleted too')) {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		url: configUrl + 'Category/delete/_',
		data: {
			id : id,
		},
		headers: {
		    'Content-Type': 'application/x-www-form-urlencoded'
		},
		success: function(response) {
			if (response.code == 200) {
				get_data()
				$('#sheets_category').hideMenu();
				$('#snackbar-success').toast('show');
			} else {
				alert('failed, try again later');
				$('#sheets_category').hideMenu();
			}
		},
	});
	}
}