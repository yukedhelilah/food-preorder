var configUrl       = 'http://dapur123.com/server.php/Master/';

function save(){
	if ($('#password').val() == '') {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin harus diisi');
	} else if ($('#password').val() == '1234') {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin tidak boleh 1234');
    } else if ($('#password').val().length < 4) {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Pin harus 4 digit');
    } else {
	    $('#login').css('display', 'none');
	    $('#loadLogin').css('display', 'block');
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Staff/change_password/_',
		    data: {
		    	id 			: atob(localStorage.getItem(btoa("resto_staffID"))),
		    	password 	: $('#password').val(),
		    },
		    headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
	            if (response.code == '200') {
					$('#snackbar-success').toast('show');
				  	localStorage.removeItem(btoa('resto_restoID'));
				  	localStorage.removeItem(btoa('resto_restoName'));
				  	localStorage.removeItem(btoa('resto_staffID'));
				  	localStorage.removeItem(btoa('resto_staffName'));
				  	localStorage.removeItem(btoa('resto_staffWA'));
				  	localStorage.removeItem(btoa('resto_staffEmail'));
				  	localStorage.removeItem(btoa('resto_token'));
				  	localStorage.removeItem(btoa('resto_expired'));
                    setTimeout(function () {
                        window.location.href = 'login';
                    }, 2000);
	            }else{
					$('#snackbar-error').toast('show');
        			$('#txt-error').html('Gagal, coba lagi nanti.');
                    setTimeout(function () {
                        window.location.href = 'home';
                    }, 500);
	            }
		    },
		});
	}
}