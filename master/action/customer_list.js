$(document).ready(function() {
  var limit   = 15;
  var start   = 0;
  var action  = 'inactive';

  function get_data(limit, start){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Customer/get_data/_',
      data: {
        id    : restoID,
        start : start,
        limit : limit, 
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.status == true) {
          // $('#content').empty();
          $('#content').append(`
            <div class="accordion" id="accordion"></div>
          `);
          $.each(response.data, function( i, val ) { 
            var margintop = i == 0 ? `margin:10px 0 0 0` : `margin:5px 0 0 0`; 

            $('#accordion').append(`
              <div class="mb-0">
                <button class="btn accordion-btn" data-toggle="collapse" data-target="#customer`+val.guestWa+`" style="background:white;padding: 10px 15px !important;width:100%;border-radius:0;border-bottom: 1px solid #e8e8e8;`+margintop+`">
                  <div class="content" style="margin: 0">
                      <div class="d-flex mb-n1">
                          <div>
                              <h3 style="font-size: 13px;line-height:1;margin:0">`+val.guestName+` / `+val.guestWa+`</h3>
                              <p class="opacity-80 font-12 mt-1">Terakhir pesan : `+ dateFormat2(val.orderDate)+`</p>
                          </div>
                          <div class="ml-auto text-center opacity-70">
                            <table style="width:100%">
                              <tr>
                                <td style="text-align:left;font-size:11px;font-weight:600;line-height:1.5">Order Paid </td>
                                <td style="text-align:right;padding-left:5px;font-size:11px;font-weight:600;line-height:1.5">`+val.paid+`</td>
                              </tr>
                              <tr>
                                <td style="text-align:left;font-size:11px;font-weight:600;line-height:1.5">Order Unpaid </td>
                                <td style="text-align:right;padding-left:5px;font-size:11px;font-weight:600;line-height:1.5">`+val.unpaid+`</td>
                              </tr>
                            </table>
                          </div>
                      </div>
                  </div>
                </button>
                <div id="customer`+val.guestWa+`" class="collapse" data-parent="#accordion" style="background:white">
                  <div class="card card-style" style="margin: 0;border-radius: 0">
                    <div class="content" style="margin: 0">
                      <div class="accordion" id="accordionOrder`+val.guestWa+`"></div>
                    </div>
                  </div>
                </div>
              </div>
            `);
          
            $.each(val.order, function( j, value ) { 
              // var status    = value.isStatus == 'Order' ? `fa-clock color-yellow1-dark` : value.isStatus == 'Paid' ? `fa-check-circle color-green1-dark` : `fa-times-circle color-red2-dark`;
              var discount  = value.discount == 0 ? '' : 
                              `<tr>
                                <td style="text-align: left;"><h5 class="font-500 font-13">Diskon</h5></td>
                                <td style="text-align: right;"><h5 class="font-13">`+numberFormat(value.discount)+`</h5></td>
                              </tr>`;
              var payment   = value.paymentID == 1 ? 
                              `<tr>
                                <td style="text-align: left;">
                                  <h5 class="font-500 font-13 mb-0">`+value.paymentName+`</h5>
                                </td>
                                <td style="text-align: right;">
                                  <h5 class="font-500 font-13 mb-0">`+value.accountName+`</h5>
                                </td>
                              </tr>` : `<tr>
                                <td style="text-align: left;">
                                  <h5 class="font-500 font-13 mb-0">`+value.paymentName+`</h5>
                                </td>
                                <td style="text-align: right;">
                                  <h5 class="font-500 font-13 mb-0">`+value.accountName+` / `+value.accountNo+`</h5>
                                </td>
                              </tr>`;
              var shipment  = value.guestShipment == 'delivery' ?    
                            `<div class="divider" style="margin: 5px 0"></div>
                            <div class="row mb-2 mt-0">        
                              <h5 class="col-12 font-14" style="margin:0">PENGIRIMAN</h5>     
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Alamat</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+value.guestAddress+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Kota</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+value.guestCity+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Keterangan</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+value.guestNote+`</h5>
                            </div>` : '';

              var button    = value.isStatus == 'Order' ? 
                              `<div class="flex-grow-1" style="margin-right:10px">
                                <a href="javascript:;" onclick="confirm_order(`+value.id+`, '`+value.orderNo+`', '`+value.guestShipment+`', '`+value.guestName+`', '`+value.guestAddress+`', `+value.total+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-3">Konfirmasi Pembayaran</a> 
                              </div>
                              <div class="flex-grow-1">
                                <a href="javascript:;" onclick="delete_order(`+value.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-3">Hapus</a> 
                              </div>` : 
                              value.isStatus == 'Unpaid' ?
                              `<div class="flex-grow-1">
                                <a href="javascript:;" onclick="delete_order(`+value.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-3">Hapus</a> 
                              </div>` : '';

              $('#accordionOrder'+val.guestWa).append(`
                <div class="mb-0">
                  <button class="btn accordion-btn"  data-toggle="collapse" data-target="#order`+value.orderNo+`" style="background:#f8f8f8;padding: 10px 15px !important;margin:0;border-bottom:1px solid #e8e8e8 !important">
                    <div class="content" style="margin: 0">
                        <div class="d-flex mb-n1">
                            <div>
                                <h3 style="font-size: 13px;line-height:1;margin:0">`+ dateFormat2(value.orderDate) +` / <span id="badgeStatus`+value.id+`">`+ value.isStatus.toUpperCase() +`</span></h3>
                                <p class="opacity-80 font-12 mt-1">`+ value.orderNo +` / `+ value.guestShipment.toUpperCase() +`</p>
                            </div>
                            <div class="ml-auto text-center opacity-70">
                              <table style="width:100%">
                                <tr>
                                  <td style="text-align:right;padding-left:5px;font-size:18px;font-weight:500;line-height:1.5;color:black">`+ numberFormat(value.total) +`</td>
                                </tr>
                              </table>
                            </div>
                        </div>
                    </div>
                  </button>
                  <div id="order`+value.orderNo+`" class="collapse" data-parent="#accordionOrder`+val.guestWa+`" style="background:white;border:10px solid rgb(240 240 240)">
                    <div class="card card-style" style="margin: 0;border-radius: 0;border-bottom:1px solid #e8e8e8 !important">
                      <div class="content" style="margin: 0;padding:10px">
                        <div class="row mb-2 mt-0">                
                          <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Nomor Pesanan</h5>
                          <h5 class="col-8 text-right font-13 mb-0">`+value.orderNo+`</h5>
                          <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Tanggal</h5>
                          <h5 class="col-8 text-right font-13 mb-0">`+dateFormat3(value.orderDate)+`</h5>
                          <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Pembeli</h5>
                          <h5 class="col-8 text-right font-13 mb-0">`+value.guestName+`</h5>
                          <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Whatsapp</h5>
                          <h5 class="col-8 text-right font-13 mb-0">`+value.guestWa+`</h5>
                          <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Email</h5>
                          <h5 class="col-8 text-right font-13 mb-0">`+value.guestEmail+`</h5>
                        </div>

                        <div class="divider" style="margin: 10px 0"></div>

                        <table style="width: 100%" id="tb_item`+value.id+`">
                          <thead>
                            <tr>
                              <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">DAFTAR PESANAN</h5></td>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                        <table style="width: 100%" id="tb_item_additional`+value.id+`">
                          <tbody>
                          </tbody>
                          <tfoot>`+discount+`</tfoot>
                        </table>
                        <div class="d-flex mb-0">
                          <div><h3 class="font-700 mb-0 font-13">Total</h3></div>
                          <div class="ml-auto"><h3 class="font-13 mb-0">`+numberFormat(value.total)+`</h3></div>
                        </div>

                        `+ shipment +`

                        <div class="divider" style="margin: 5px 0"></div>
                        
                        <table style="width: 100%">
                          <thead>
                            <tr>
                              <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">PEMBAYARAN</h5></td>
                            </tr>
                          </thead>
                          <tbody>
                            `+payment+`
                          </tbody>
                        </table>
                        <div class="d-flex" id="button`+value.id+`">`+button+`<div>
                      </div>
                    </div>
                  </div>
                </div>
              `);

              $('#tb_item'+value.id+' > tbody').empty();
              $('#tb_item_additional'+value.id+' > tbody').empty();
              $.each(value.order_item, function( k, values ) {
                if (values.additional == 'false') {
                  if (values.poID != 0) {
                    $('#tb_item'+value.id+' > tbody').append(`
                      <tr>
                        <td style="text-align: left">
                          <h5 class="font-500 mb-0 font-13">`+values.qty+` x `+values.productName+` (`+dateFormat2(values.poDate)+`)</h5>
                        </td>
                        <td style="text-align: right;">
                          <h5 class="mb-0 font-13">`+numberFormat(values.price)+`</h5>       
                        </td>
                      </tr>
                    `);
                  } else {
                    $('#tb_item'+value.id+' > tbody').append(`
                      <tr>
                        <td style="text-align: left;">
                          <h5 class="font-500 mb-0 font-13">`+values.qty+` x `+values.productName+`</h5>
                        </td>
                        <td style="text-align: right;">
                          <h5 class="mb-0 font-13">`+numberFormat(values.price)+`</h5>       
                        </td>
                      </tr>
                    `);
                  }
                } else {
                  $('#tb_item_additional'+value.id+' > tbody').append(`
                    <tr>
                      <td style="text-align: left;">
                        <h5 class="font-500 mb-0 font-13">`+values.productName+`</h5>
                      </td>
                      <td style="text-align: right;">
                        <h5 class="mb-0 font-13">`+numberFormat(values.price)+`</h5>       
                      </td>
                    </tr>
                  `);
                }
              });
            });
          });

          if(response.data == null){
            action = 'active';
          }else{
            action = "inactive";
          }
        } else if (response.status == false && start == 0) {
          $('#content').append(`
            <div class="card mb-0 mt-2">
              <div class="content" style="margin: 10px 15px;text-align:center">
                Belum ada pesanan
              </div>   
            </div>
          `);
        }
      },
    });
  }

  if(action == 'inactive'){
    action = 'active';
    get_data(limit, start);
  }

  $(window).scroll(function(){
    if($(window).scrollTop() + $(window).height() > $("#content").height() && action == 'inactive'){
      action = 'active';
      start = start + limit;
      setTimeout(function(){
        get_data(limit, start);
      }, 1000);
    }
  });
});

function confirm_order(id, orderNo, guestShipment, guestName, guestAddress, total){
  $('#divConfirm').empty();
  var address = guestShipment == 'delivery' ? 
                `<h5 class="col-4 text-left font-14 opacity-60 font-500">Alamat</h5>
                <h5 class="col-8 text-right font-15">`+guestAddress+`</h5>` : '';
  $('#divConfirm').append(`
    <h5 class="col-5 text-left font-14 opacity-60 font-500">Nomor Pesanan</h5>
    <h5 class="col-7 text-right font-15">`+orderNo+`</h5>
    <h5 class="col-4 text-left font-14 opacity-60 font-500">Pengiriman</h5>
    <h5 class="col-8 text-right font-15">`+guestShipment.toUpperCase() +`</h5>
    <h5 class="col-4 text-left font-14 opacity-60 font-500">Pembeli</h5>
    <h5 class="col-8 text-right font-15">`+guestName+`</h5>
    `+address);
  $('#buttonConfirm').empty();
  $('#buttonConfirm').append(`
    <h5 style="margin: 0 15px">Apakah anda telah menerima pembayaran sebesar Rp. `+ numberFormat(total)+`</h5>
    <div class="d-flex mb-3" style="margin:0 10px">
      <div class="flex-grow-1" style="margin-right:10px">
        <a href="javascript:;" onclick="status(`+id+`, '`+orderNo+`')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-2">Ya</a> 
      </div>
      <div class="flex-grow-1">
        <a href="javascript:;" onclick="orderProcess(`+id+`, '`+orderNo+`', '', 'Unpaid')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Tidak</a> 
      </div>
    </div>`);
  $('#sheets_confirm').showMenu();
}

function status(id, orderNo){
  $('#divConfirm').empty();
  $('#divConfirm').append(`
    <div class="input-style input-style-2 input-required mt-0 mb-0" style="width:100%;margin:0 10px;">
      <input class="form-control" type="name" id="restoNote" name="restoNote" placeholder="Catatan untuk pembeli">
    </div>` 
  );
  $('#buttonConfirm').empty();
  $('#buttonConfirm').append(`
    <div class="d-flex mb-3" style="margin:0 10px">
      <div class="flex-grow-1" style="margin-right:10px">
        <a href="javascript:;" onclick="orderProcess(`+id+`, '`+orderNo+`', '`+ $('#restoNote').val() +`', 'Paid')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-0">Proses Pemesanan</a> 
      </div>
    </div>`);
}

function orderProcess(id, orderNo, restoNote, isStatus) {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order/confirm/_',
    data: {
      id        : id,
      orderNo   : orderNo,
      restoNote : restoNote,
      isStatus  : isStatus,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    beforeSend: function() {
      $('#buttonConfirm').empty();
      $('#buttonConfirm').append(`
        <div class="d-flex justify-content-center">
          <div class="spinner-border color-red2-dark" role="status" style="margin-bottom:10px">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      `);
    },
    success: function(response) {
      if (response.code == '200') {
        $('#snackbar-success').toast('show');
        if (isStatus == 'Paid') {
          // $('#badgeStatus'+id).removeAttr('class');
          // $('#badgeStatus'+id).attr('class','fa fa-check-circle color-green1-dark mt-1');
          $('#badgeStatus'+id).html('PAID');
          $('#button'+id).html('');
        } else {
          // $('#badgeStatus'+id).removeAttr('class');
          // $('#badgeStatus'+id).attr('class','fa fa-times-circle color-red2-dark mt-1');
          $('#badgeStatus'+id).html('UNPAID');
          $('#button'+id).html(`
            <div class="flex-grow-1">
              <a href="javascript:;" onclick="delete_order(`+id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-3">Hapus</a> 
            </div>
          `);
        }
        $('#sheets_confirm').hideMenu();
      }else{
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Gagal, coba lagi nanti');
        $('#sheets_confirm').hideMenu();
      }
    },
  });
}

function delete_order(id) {
  if (confirm('Anda yakin menghapus pesanan ini?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order/delete/_',
      data: {
        id        : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.status == true) {
          $('#snackbar-success').toast('show');
          setTimeout(function () {
            window.location.href = 'order_list';
          }, 100);
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}