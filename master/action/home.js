$(document).ready(function() {
  $("#restoName").html(restoName.toUpperCase());
  get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Home/get_data/_',
    data:{
      id      : restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      var branch = response.data.resto.branchName != '' ? ` - ` + response.data.resto.branchName + `, ` + response.data.resto.cityName : `, ` +response.data.resto.cityName; 
      $('#infoResto').html(`
        <h6 style="color: #2c3033;font-weight: 500;font-size: 13px;margin: 0">Halo, `+ staffName +`</h6>
        <h6 style="color: #2c3033;font-weight: 600;font-size: 13px;margin: 0">`+ restoName +``+ branch +`</h6>
        <div class="divider" style="margin:5px 0"></div>
        <h6 style="color: #2c3033;font-weight: 600;font-size: 11px;margin: 0 0 15px 0;color: blue;">Akun anda saat ini adalah `+ response.data.resto.packageName +`, berlaku sampai `+ dateFormat6(response.data.resto.expiredDate) +`</h6>
      `);

      var total   = response.data.summary == null ? 0 : numberFormat(response.data.summary.total);
      var ordered = response.data.summary == null ? 0 : response.data.summary.ordered;
      var paid    = response.data.summary == null ? 0 : response.data.summary.paid;
      var unpaid  = response.data.summary == null ? 0 : response.data.summary.unpaid;
      $('#infoPenjualan').html(`
        <div class="content" style="margin: 5px 10px">
          <table style="width: 100%;margin: 0">
            <tr>
              <td style="width:1%;padding-right:10px">
                <i class="fa fa-wallet bg-orange-light rounded-s" style="width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
              </td>
              <td style="line-height: 1.3">
                Total penjualan hari ini
                <h4 style="margin: 0">`+ total +`</h4>
              </td>
              <td>
                <p style="margin: 0;line-height: 1.3;font-size: 12px">`+ ordered +` Pesanan</p>
                <p style="margin: 0;line-height: 1.3;font-size: 12px">`+ paid +` Paid</p>
                <p style="margin: 0;line-height: 1.3;font-size: 12px">`+ unpaid +` Unpaid</p>
              </td>
            </tr>
          </table>
        </div>   
      `);

      if (response.data.resto.isRegular == 1) {
        $('#restoPO').css('display', 'block');
        $('#minPO').html(`Pembeli hanya bisa membeli pesanan maksimal H-`+ response.data.resto.cutoffday);
      }

      // if (staffName == 'MASTER') {
      //   $('#buttonSetting').html(`
      //     <div class="col-3" style="padding: 0 10px" onclick="product()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-star bg-green1-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Produk</small>
      //       </div>
      //     </div>
      //     <div class="col-3" style="padding: 0 10px" onclick="order()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-shopping-cart bg-mint-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Pesanan</small>
      //       </div>
      //     </div>
      //     <div class="col-3" style="padding: 0 10px" onclick="schedule()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-clock bg-magenta1-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Operasional</small>
      //       </div>
      //     </div>
      //     <div class="col-3" style="padding: 0 10px" onclick="staff()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-user bg-pink1-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Staff</small>
      //       </div>
      //     </div>
      //   `);
      // } else {
      //   $('#buttonSetting').html(`
      //     <div class="col-4" style="padding: 0 10px" onclick="product()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-star bg-green1-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Produk</small>
      //       </div>
      //     </div>
      //     <div class="col-4" style="padding: 0 10px" onclick="order()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-shopping-cart bg-mint-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Pesanan</small>
      //       </div>
      //     </div>
      //     <div class="col-4" style="padding: 0 10px" onclick="schedule()">
      //       <div class="card card-style" style="margin: 0;padding: 10px 0">
      //         <i class="fa fa-clock bg-magenta1-dark rounded-s" style="margin:auto;width: 32px;height: 32px;line-height: 32px;text-align: center;"></i>
      //         <small style="margin: auto;margin-top: 6px;line-height: 1">Operasional</small>
      //       </div>
      //     </div>
      //   `);
      // }

      $('#listPesanan').empty();
      if (response.data.order == null) {
        $('#listPesanan').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada pesanan
            </div>   
          </div>
        `); 
      } else {
        $('#listPesanan').append(`
          <div class="accordion" id="accordion"></div>
        `);
        $.each(response.data.order, function( i, val ) { 
          var border    = i == 0 ? `border-top:1px solid #cacaca;border-bottom: 1px solid #cacaca` : `border-bottom: 1px solid #cacaca`; 
          var status    = val.isStatus == 'Order' ? `fa-clock color-yellow1-dark` : val.isStatus == 'Paid' ? `fa-check-circle color-green1-dark` : `fa-times-circle color-red2-dark`;
          var shipment  = val.guestShipment == 'deliver' ? val.guestShipment+` `+val.guestCity : val.guestShipment;

          $('#accordion').append(`
            <div class="mb-0">
              <button class="btn accordion-btn" data-toggle="collapse" data-target="#order`+val.orderNo+`" style="background:white;padding: 10px 15px !important;margin:0;border-radius:0;`+border+`">
                <div class="content" style="margin: 0">
                    <div class="d-flex mb-n1">
                        <div>
                          <h6 style="font-size: 15px;font-weight:500;margin:0">`+ dateFormat4(val.orderDate)+` / `+val.orderNo+`</h6>
                          <p class="opacity-80 font-12 mt-0">`+ shipment +`</p>
                        </div>
                        <div class="ml-auto text-center opacity-70">
                          <h3 class="mb-0">`+ numberFormat(val.total) +`</h3>
                        </div>
                    </div>
                </div>
              </button>
            </div>
          `);
        });
      }
    },
  });
}

function schedule(){
  window.location.href = 'schedule';
}

function staff(){
  window.location.href = 'staff';
}

function save_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Home/editPO/_',
    data:{
      id        : restoID,
      cutoffday : $('#cutoffday').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#sheets_edit').hideMenu();
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          get_data();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
    },
  });
}