var display = 0;
$(document).ready(function() {
  $('#search').removeAttr();
  if (restoType == 0) {
    $('#search').attr('onKeyUp', 'get_data()');
    get_data();
  } else {
    $('#search').attr('onKeyUp', 'get_data_po()');
    get_data_po();
  }
  
  $("#restoName").html(restoName.toUpperCase());
  
  $('#noImg').click(function(){
    $('#noImg').css('background','#da4453').css('color', 'white');
    $('#withImg').css('background','none').css('color', 'black');
    display = 1;
    if (restoType == 0) {
      get_data();
    } else {
      get_data_po();
    }
  });
  
  $('#withImg').click(function(){
    $('#withImg').css('background','#da4453').css('color', 'white');
    $('#noImg').css('background','none').css('color', 'black');
    display = 0;
    if (restoType == 0) {
      get_data();
    } else {
      get_data_po();
    }
  });
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_data/_',
    data:{
      id      : restoID,
      search  : $('#search').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#content').empty();
      if (response.status == true) {
        if ($('#search').val() == '') {
          $('#content').append(`
            <div class="accordion" id="accordion1"></div>
          `);
          $.each(response.data, function( i, val ) {
            $('#accordion1').append(`
              <div class="mb-0">
                <button class="btn accordion-btn" data-toggle="collapse" data-target="#category`+val.id+`" style="background:white">
                  `+val.categoryName+`
                  <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </button>
                <div id="category`+val.id+`" class="collapse"  data-parent="#accordion1" style="background:white">
                </div>
              </div>
              
            `);

            if (val.product != 0) {
              $.each(val.product, function( j, value ) {
                var available = value.available == 0 ? `<span class="color-green2-dark">`+numberFormat(value.price)+`</span>` : `<i class="color-red2-dark">SOLD</i>`;
                var img = ''; 
                if (value.img == '') {
                  img = configFile + `no-image.png`;
                } else {
                  img = configFile + `product/`+value.img;
                }

                if (display == 0) {
                  $('#category'+val.id).append(`
                    <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                      <div class="content" style="margin:10px;height: fit-content;min-height: 75px;">
                        <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                          <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:22px !important">
                          <div style="margin-left:105px !important;margin-right: 20px !important;width: -webkit-fill-available !important;">
                            <table style="width:100%">
                              <tr>
                                <td>
                                  <h4>`+value.productName+`</h4>
                                </td>
                                <td style="text-align:right">
                                  <b>`+available+`</b>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2" style="line-height:1.5">
                                  `+value.description+`
                                </td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  `);
                } else {
                  $('#category'+val.id).append(`
                    <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                      <div class="content" style="margin:5px 17px;height: fit-content;">
                        <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                          <div style="margin-left:15px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                            <table style="width:100%">
                              <tr>
                                <td>
                                  `+value.productName+`
                                </td>
                                <td style="text-align:right">
                                  <b>`+available+`</b>
                                </td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  `);
                }
              }); 
            } else {
              $('#category'+val.id).append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px;text-align:center">
                    Belum ada produk dalam kategori ini
                  </div>
                </div>
              `);
            }
          });
        } else {
          if (response.total > 0) {
            $('#content').append(`
              <div style="background:white;text-align:center;padding:10px 0">
                Produk tidak ditemukan
              </div>
            `); 
          } else {
            $.each(response.data, function( i, value ) {
              var available = value.available == 0 ? `<span class="color-green2-dark">`+numberFormat(value.price)+`</span>` : `<i class="color-red2-dark">SOLD</i>`;
              var img = ''; 
              if (value.img == '') {
                img = configFile + `no-image.png`;
              } else {
                img = configFile + `product/`+value.img;
              }
              
              var flag = value.flag == 0 ? `<input type="checkbox" class="android-input" checked id="`+value.id+`" style="display:none">` : `<input type="checkbox" class="android-input" id="`+value.id+`" style="display:none">`;
              
              if (display == 0){
                $('#content').append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:10px;height: fit-content;min-height: 75px;">
                      <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                        <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:22px !important">
                        <div style="margin-left:105px !important;margin-right: 20px !important;width: -webkit-fill-available !important;">
                          <table style="width:100%">
                            <tr>
                              <td>
                                <h4>`+value.productName+`</h4>
                              </td>
                              <td style="text-align:right">
                                <b>`+available+`</b>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="line-height:1.5">
                                `+value.description+`
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              } else {
                $('#content').append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:5px 17px;height: fit-content;">
                      <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                        <div style="margin-left:15px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                          <table style="width:100%">
                            <tr>
                              <td>
                                `+value.productName+`
                              </td>
                              <td style="text-align:right">
                                <b>`+available+`</b>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              }
            });
          }
        } 
      } else {
        $('#content').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada produk
            </div>   
          </div>
        `);
      }
    },
  });
}

function get_data_po(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_data_po/_',
    data:{
      id      : restoID,
      search  : $('#search').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#content').empty();
      if (response.status == true) {
        if ($('#search').val() == '') {
          $('#content').append(`
            <div class="accordion" id="accordion1"></div>
          `);
          $.each(response.data, function( i, val ) {
            $('#accordion1').append(`
              <div class="mb-0">
                <button class="btn accordion-btn" data-toggle="collapse" data-target="#category`+val.id+`" style="background:white">
                  `+val.categoryName+`
                  <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </button>
                <div id="category`+val.id+`" class="collapse"  data-parent="#accordion1" style="background:white">
                </div>
              </div>
              
            `);

            if (val.product != 0) {
              $.each(val.product, function( j, value ) {
                var available = value.available == 0 ? `<span class="color-green2-dark">`+numberFormat(value.price)+`</span>` : `<i class="color-red2-dark">SOLD</i>`;
                var img = ''; 
                if (value.img == '') {
                  img = configFile + `no-image.png`;
                } else {
                  img = configFile + `product/`+value.img;
                }

                if (display == 0) {
                  $('#category'+val.id).append(`
                    <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                      <div class="content" style="margin:10px;height: fit-content;min-height: 75px;">
                        <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                          <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:22px !important">
                          <div style="margin-left:105px !important;margin-right: 20px !important;width: -webkit-fill-available !important;">
                            <table style="width:100%">
                              <tr>
                                <td>
                                  <h4>`+value.productName+`</h4>
                                </td>
                                <td style="text-align:right">
                                  <b>`+available+`</b>
                                </td>
                              </tr>
                            </table>
                            <table style="margin:5px 0 0 0;width:100%;border-collapse:collapse;border:1px solid #dedede" border="1">
                              <thead>
                                <tr>
                                  <td style="padding:5px">
                                    Tanggal
                                  </td>
                                  <td style="padding:5px;text-align:center">
                                    Slot
                                  </td>
                                </tr>
                              </thead>
                              <tbody id="product`+value.id+`"></tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  `);
                } else {
                  $('#category'+val.id).append(`
                    <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                      <div class="content" style="margin:5px 17px;height: fit-content;">
                        <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                          <div style="margin-left:15px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                            <table style="width:100%">
                              <tr>
                                <th style="color:black;">
                                  `+value.productName+`
                                </th>
                                <td style="text-align:right">
                                  <b>`+available+`</b>
                                </td>
                              </tr>
                            </table>
                            <table style="margin:5px 0;width:100%;border-collapse:collapse;border:1px solid #dedede" border="1">
                              <thead>
                                <tr>
                                  <td style="padding:5px">
                                    Tanggal
                                  </td>
                                  <td style="padding:5px;text-align:center">
                                    Slot
                                  </td>
                                </tr>
                              </thead>
                              <tbody id="product`+value.id+`"></tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  `);
                }

                if (value.po != 0) {
                  $.each(value.po, function( k, values ) {
                    $('#product'+value.id).append(`
                      <tr>
                        <th style="padding:5px">
                          `+dateFormat3(values.poDate)+`
                        </th>
                        <th style="padding:5px;text-align:center">
                          `+(values.allotment - values.sold)+`
                        </th>
                      </tr>
                    `);
                  })
                } else {
                  $('#product'+value.id).append(`
                    <tr>
                      <td colspan="2" style="text-align:center;padding:5px">
                        Belum ada jadwal PO
                      </td>
                    </tr>
                  `);
                }
              }); 
            } else {
              $('#category'+val.id).append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px;text-align:center">
                    Belum ada produk dalam kategori ini
                  </div>
                </div>
              `);
            }
          });
        } else {
          if (response.total > 0) {
            $('#content').append(`
              <div style="background:white;text-align:center;padding:10px 0">
                Produk tidak ditemukan
              </div>
            `); 
          } else {
            $.each(response.data, function( i, value ) {
              var available = value.available == 0 ? `<span class="color-green2-dark">`+numberFormat(value.price)+`</span>` : `<i class="color-red2-dark">SOLD</i>`;
              var img = ''; 
              if (value.img == '') {
                img = configFile + `no-image.png`;
              } else {
                img = configFile + `product/`+value.img;
              }
              
              var flag = value.flag == 0 ? `<input type="checkbox" class="android-input" checked id="`+value.id+`" style="display:none">` : `<input type="checkbox" class="android-input" id="`+value.id+`" style="display:none">`;
              
              if (display == 0){
                $('#content').append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:10px;height: fit-content;min-height: 75px;">
                      <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                        <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:22px !important">
                        <div style="margin-left:105px !important;margin-right: 20px !important;width: -webkit-fill-available !important;">
                          <table style="width:100%">
                            <tr>
                              <td>
                                <h4>`+value.productName+`</h4>
                              </td>
                              <td style="text-align:right">
                                <b>`+available+`</b>
                              </td>
                            </tr>
                          </table>
                          <table style="margin:5px 0 0 0;width:100%;border-collapse:collapse;border:1px solid #dedede" border="1">
                            <thead>
                              <tr>
                                <td style="padding:5px">
                                  Tanggal
                                </td>
                                <td style="padding:5px;text-align:center">
                                  Slot
                                </td>
                              </tr>
                            </thead>
                            <tbody id="product`+value.id+`"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              } else {
                $('#content').append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:5px 17px;height: fit-content;">
                      <div class="row mb-0" onclick="view_product(`+value.id+`,'`+value.productName+`','`+img+`','`+value.price+`','`+value.available+`','`+value.description+`')">
                        <div style="margin-left:15px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                          <table style="width:100%">
                            <tr>
                              <th style="color:black;">
                                `+value.productName+`
                              </th>
                              <td style="text-align:right">
                                <b>`+available+`</b>
                              </td>
                            </tr>
                          </table>
                          <table style="margin:5px 0;width:100%;border-collapse:collapse;border:1px solid #dedede" border="1">
                            <thead>
                              <tr>
                                <td style="padding:5px">
                                  Tanggal
                                </td>
                                <td style="padding:5px;text-align:center">
                                  Slot
                                </td>
                              </tr>
                            </thead>
                            <tbody id="product`+value.id+`"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              }

              if (value.po != 0) {
                $.each(value.po, function( k, values ) {
                  $('#product'+value.id).append(`
                    <tr>
                      <th style="padding:5px">
                        `+dateFormat3(values.poDate)+`
                      </th>
                      <th style="padding:5px;text-align:center">
                          `+(values.allotment - values.sold)+`
                      </th>
                    </tr>
                  `);
                })
              } else {
                $('#product'+value.id).append(`
                  <tr>
                    <td colspan="2" style="text-align:center;padding:5px">
                      Belum ada jadwal PO
                    </td>
                  </tr>
                `);
              }
            });
          }
        } 
      } else {
        $('#content').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada produk
            </div>   
          </div>
        `);
      }
    },
  });
}

function view_product(id, productName, productImg, productPrice, productAvailable, productDescription){
  window.location.href = '#';
  $('#modal_menu').showMenu();

  var available = productAvailable == 0 ? `<span class="color-green2-dark">IDR `+numberFormat(productPrice)+`</span>` : `<i class="color-red2-dark">SOLD</i>`;
  $('#productName').html(productName);
  $('#productImg').removeAttr('src');
  $('#productImg').attr('src', productImg);
  $('#productPrice').html(available);
  $('#productDescription').html(productDescription);
}