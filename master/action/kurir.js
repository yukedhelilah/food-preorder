$(document).ready(function() {
  get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Kurir/get_data/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#content').empty();
        $('#content').append(`
          <div class="accordion" id="accordion1"></div>
        `);
        $.each(response.data, function( i, val ) {
          $('#accordion1').append(`
            <div class="mb-0">
              <button class="btn accordion-btn font-15 font-600" data-toggle="collapse" data-target="#company`+val.id+`" style="background:white;padding:10px 15px">
                `+val.companyName+`
                <i class="fa fa-chevron-down font-10 accordion-icon"></i>
              </button>
              <div id="company`+val.id+`" class="collapse"  data-parent="#accordion1" style="background:white">
              </div>
            </div>
          `);

          if (val.ongkir_category != 0) {
            $.each(val.ongkir_category, function( j, value ) {
              $('#company'+val.id).append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px 15px">
                    <table style="width:100%">
                      <tr>
                        <td onclick="ongkir(`+value.id+`)">
                          <h5 class="font-14 font-500 mb-0">`+value.startFrom+`</h5>
                        </td>
                        <td>
                          <i class="fa fa-chevron-right font-10 accordion-icon"></i>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              `);
            });
          } else {
            $('#company'+val.id).append(`
              <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                <div class="content" style="margin:10px;text-align:center">
                  Belum ada daftar ongkos kirim
                </div>
              </div>
            `);
          }
        });
      } else {
        $('#content').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada Kurir
            </div>   
          </div>
        `);
      }
    },
  });
}

function ongkir(id) {
  window.location.href = `ongkir?search=`+btoa(id);
}