var configUrl       = 'https://dapur123.com/server.php/Master/';
// var configUrl       = 'https://creatrixorganizer.com/resto/Master/';

$('input[name="no_wa"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('resto_restoID')) != null && localStorage.getItem(btoa('resto_staffID')) != null && localStorage.getItem(btoa('resto_expired')) != null && localStorage.getItem(btoa('resto_token')) != null) {
        window.location.href = '../pages/home';
    }
});

function requiredField(val){
    var value   = $.trim(val);
    var number  = value[0];

    if (number == '0') { $('#no_wa').val('62'+$('#no_wa').val().substr(1));}
}

function login(){  
    $('#login').css('display', 'none');
    $('#loadLogin').css('display', 'block');
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token   : '1b787b70ed2e0de697d731f14b5da57b',
            no_wa   : $("#no_wa").val(),
        },
        success: function(response) {
            if (response.code == '200') {   
                $('#otp').val(response.data.otp);
                $('#partitioned').val('');
                $('#a_resend').html('Kirim ulang');
                $('#div-login').css('display','none');
                $('#div-verifikasi').css('display','block');
                $('#theTarget').html(59);
                $('#theTargetReal').html(59);
                $('#a_resend').hide();
                var timer = setInterval(function() {
                    var count = parseInt($('#theTargetReal').html());
                    if (count !== 0) {
                        if(count < 10){
                            $('#theTarget').html('0'+(count - 1));
                        }else{
                            $('#theTarget').html(count - 1);
                        }
                        $('#theTargetReal').html(count - 1);
                    } else {
                      clearInterval(timer);
                      $('#a_resend').show();
                    }
                }, 1000);

                $('#a_resend').on('click', function() {
                    sendOTP(response.data.staffID, response.data.staffEmail, response.data.staffWA);
                });

                $('#verifikasi').click(function() {
                    if ($('#partitioned').val() == atob($('#otp').val())) {
                        localStorage.setItem(btoa('resto_restoID'), btoa(response.data.restoID));
                        localStorage.setItem(btoa('resto_restoType'), btoa(response.data.restoType));
                        localStorage.setItem(btoa('resto_restoName'), btoa(response.data.restoName));
                        localStorage.setItem(btoa('resto_staffID'), btoa(response.data.staffID));
                        localStorage.setItem(btoa('resto_staffName'), btoa(response.data.staffName));
                        localStorage.setItem(btoa('resto_staffWA'), btoa(response.data.staffWA));
                        localStorage.setItem(btoa('resto_staffEmail'), btoa(response.data.staffEmail));
                        localStorage.setItem(btoa('resto_token'), btoa(response.data.token));
                        localStorage.setItem(btoa('resto_expired'), btoa(response.data.expired));
                        if (response.data.staffPassword == 'default') {
                            window.location.href = 'change_password';
                        } else {
                            window.location.href = 'home';
                        }
                    } else {
                        alert('Kode OTP salah');
                        $('#partitioned').val('');
                    }
                });
            } else{
            	alert('Nomor anda belum terdaftar');
                $('#login').css('display', 'block');
                $('#loadLogin').css('display', 'none');
            }
        },
    });
}

function register(){
    window.location.href = '../pages/register';
}

function sendOTP(id, email, wa){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/sendOTP/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token   : '1b787b70ed2e0de697d731f14b5da57b',
            id      : id,
            email   : email,
            wa 		: wa,
        },
        beforeSend: function() {
            $('#a_resend').html('Sedang memuat');
        },
        success: function(response) {
            if (response.code == '200') {   
                $('#otp').val(response.otp);
                $('#partitioned').val('');
                $('#a_resend').html('Kirim ulang');
                $('#div-login').css('display','none');
                $('#div-verifikasi').css('display','block');
                $('#theTarget').html(59);
                $('#theTargetReal').html(59);
                $('#a_resend').hide();
                var timer = setInterval(function() {
                    var count = parseInt($('#theTargetReal').html());
                    if (count !== 0) {
                        if(count < 10){
                            $('#theTarget').html('0'+(count - 1));
                        }else{
                            $('#theTarget').html(count - 1);
                        }
                        $('#theTargetReal').html(count - 1);
                    } else {
                      clearInterval(timer);
                      $('#a_resend').show();
                    }
                }, 1000);
            }else{
                alert('Gagal, coba lagi nanti');
            }
        },
    });
}

function login_pin() {
    window.location.href = 'login';
}