$(function(){
    get_membership();
});

function get_membership(){
    $.ajax({
        type: 'GET',
        url: configUrl2 + 'restaurant/getPackageList/_',
        dataType: 'json',
        data: {
            'id'    : CryptoJS.MD5(restoID).toString(),
        },
        success: function(responses) {
            $.each(responses.data, function(i, val){
                var color       = val.packageName == "Trial" ? "gray2" : val.packageName == "Basic" ? "blue1" :  val.packageName == "Premium" ? "green1" : val.packageName == "Plus" ? "magenta1" : "yellow1";
                var activation  = val.isHere == true ? 
                    `<a href="#" class="btn btn-s bg-`+color+`-dark btn-center-l text-uppercase rounded-s font-900">Paketmu saat ini</a>` : 
                    `<div class="input-style input-style-2 mb-2 input-required">
                        <select class="form-control" id="packageType`+ val.id +`" name="packageType">
                            <option value="1">1 Bulan</option>
                            <option value="3">3 Bulan</option>
                            <option value="6">6 Bulan</option>
                            <option value="12">1 Tahun</option>
                        </select>
                    </div>
                    <a href="javascript:;" onclick="extendRestoSave(\'`+ val.id +`,`+ val.expiredFuture +`,`+ val.packageName +`\')" class="btn btn-s bg-`+color+`-dark btn-center-l text-uppercase rounded-s font-900">Beli paket ini</a>`;
                var mt = i == 0 ? `0px !important` : `20px !important`; 
                $('#content').append(`
                    <div class="col-12" style="margin-bottom: 0px; margin-top:`+mt+`">
                        <div class="pricing-4 rounded-m shadow-m bg-theme" style="width: 100%;max-width: -webkit-fill-available;margin:0;padding-bottom: 15px">
                            <h1 class="pricing-title text-center bg-`+color+`-dark text-uppercase" style="padding: 5px 0">`+val.packageName+`</h1>
                            <h3 class="pricing-value text-center bg-`+color+`-light color-white" style="padding: 15px 0;font-size: 30px"><sup>IDR</sup>`+numberFormat(val.price)+`</h3>
                            <ul class="pricing-list text-center">
                                <li><i class="fa fa-check-circle color-green1-dark"></i> 14 Hari</li>
                                <li><i class="fa fa-check-circle color-green1-dark"></i> Gratis Kode QR</li>
                            </ul>
                            `+activation+`
                            <input type="hidden" id="p`+ val.id +`_url_1" value="`+ val.url_1 +`">
                            <input type="hidden" id="p`+ val.id +`_url_3" value="`+ val.url_3 +`">
                            <input type="hidden" id="p`+ val.id +`_url_6" value="`+ val.url_6 +`">
                            <input type="hidden" id="p`+ val.id +`_url_12" value="`+ val.url_12 +`">
                            <input type="hidden" id="p`+ val.id +`_price" value="`+ val.price +`">
                        </div>
                    </div>
                `);
            });
        }
    });
}

function extendRestoSave(data) {
    var dt              = data.split(',');
    var packageID       = dt[0];
    var expiredFuture   = dt[1];
    var packageName     = dt[2];
    var packageType     = $('#packageType'+packageID).val();
    var price           = $('#p'+ packageID +'_price').val();

    if (confirm('Anda yakin ingin upgrade keanggotaan ini?')) {
        $.ajax({
            type: 'POST',
            url: configUrl2 + 'restaurant/inputNota/_',
            dataType: 'json',
            data: {
                'restoID'       : CryptoJS.MD5(restoID).toString(),
                'sellerID'      : CryptoJS.MD5('0').toString(),
                'packageID'     : packageID,
                'price'         : parseInt(price)*packageType,
                'description'   : 'Upgrade membership to '+packageName+' / '+packageType+' month(s)',
                'url'           : $('#p'+ packageID +'_url_'+packageType).val(),
            },
            success: function(responses) {
                if(responses.status == true){
                    $('#payment_nota').html(responses.data.no_nota);
                    // $('#payment_link').html('Klik link ini untuk detail pembayaran <a href="'+$('#p'+ packageID +'_url_'+packageType).val()+'" target="_blank">'+ $('#p'+ packageID +'_url_'+packageType).val() +'</a>');
                    $('#payment_description').html('Upgrade keanggotaan <b>'+packageName+'</b><br>'+packageType+' bulan / sampai '+dateFormat(expiredFuture));
                    $('#payment_price').html('IDR '+numberFormat(parseInt(price)));
                    $('#payment_total').html('IDR '+numberFormat(parseInt(price)*packageType));
                    $('#modal_nota').showMenu();
                }else{
                    $('#snackbar-error').toast('show');
                    $('#txt-error').html(responses.message);
                }
            }
        });
    }
}