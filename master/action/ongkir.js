var id  = atob(getUrlParameter('search'))

$(document).ready(function() {
  get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Kurir/get_detail/_',
    data: {
      id: id,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.data == null) {
        $('#content').append(`
          <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
            <div class="content" style="margin:10px 15px;text-align:center">
              <h5 class="font-14 font-500 mb-0">Belum ada daftar ongkos kirim</h5>
            </div>
          </div>
        `);
      } else {
        $.each(response.data, function( i, val ) {
        	$('#content').append(`
            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
              <div class="content" style="margin:10px 15px">
                <table style="width:100%">
                  <tr>
                    <td>
                      <h5 class="font-14 font-500 mb-0">`+val.destination+`</h5>
                    </td>
                    <td style="text-align:right">
                      <h5 class="font-14 font-500 mb-0">Rp `+numberFormat(val.price)+`</h5>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          `);
        });
      }
    },
  });
}

function kurir() {
  window.location.href = 'kurir';
}