$(document).ready(function() {
  var limit   = 15;
  var start   = 0;
  var action  = 'inactive';

  get_shipment();

  function get_data(limit, start){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order/get_data/_',
      data: {
        id    : restoID,
        start : start,
        limit : limit, 
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.status == true) {
          // $('#content').empty();
          $('#content').append(`
            <div class="accordion" id="accordion"></div>
          `);
          $.each(response.data, function( i, val ) { 
            var margintop = i == 0 ? `margin:0` : `margin:5px 0 0 0`; 
            var discount  = val.discount == 0 ? '' : 
                            `<tr>
                              <td style="text-align: left;"><h5 class="font-500 font-13">Diskon</h5></td>
                              <td style="text-align: right;"><h5 class="font-13">`+numberFormat(val.discount)+`</h5></td>
                            </tr>`;
            var payment   = val.paymentID == 1 ? 
                            `<tr>
                              <td style="text-align: left;">
                                <h5 class="font-500 mb-0 font-13">`+val.paymentName+`</h5>
                              </td>
                              <td style="text-align: right;">
                                <h5 class="font-500 mb-0 font-13">`+val.accountName+`</h5>
                              </td>
                            </tr>` : `<tr>
                              <td style="text-align: left;">
                                <h5 class="font-500 mb-0 font-13">`+val.paymentName+`</h5>
                              </td>
                              <td style="text-align: right;">
                                <h5 class="font-500 mb-0 font-13">`+val.accountName+` / `+val.accountNo+`</h5>
                              </td>
                            </tr>`;
            var shipment  = val.guestShipment == 'delivery' ? 
                            `<div class="divider" style="margin: 5px 0"></div>
                            <div class="row mb-2 mt-0">        
                              <h5 class="col-12 font-14" style="margin:0">PENGIRIMAN</h5>        
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Alamat</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestAddress+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Kota</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestCity+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Catatan</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestNote+`</h5>
                            </div>` : '';

            var button    = val.isStatus == 'Order' ? 
                            `<div class="flex-grow-1" style="margin-right:10px">
                              <a href="javascript:;" onclick="confirm_order(`+val.id+`, '`+val.orderNo+`', '`+val.guestShipment+`', '`+val.guestName+`', '`+val.guestAddress+`', `+val.total+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-2">Konfirmasi Pembayaran</a> 
                            </div>
                            <div class="flex-grow-1">
                              <a href="javascript:;" onclick="delete_order(`+val.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Hapus</a> 
                            </div>` : 
                            val.isStatus == 'Unpaid' ?
                            `<div class="flex-grow-1">
                              <a href="javascript:;" onclick="delete_order(`+val.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Hapus</a> 
                            </div>` : '';

            $('#accordion').append(`
              <div class="mb-0">
                <button class="btn accordion-btn"  data-toggle="collapse" data-target="#order`+val.orderNo+`" style="background:white;padding: 5px 10px 10px 10px !important;border-bottom:1px solid #f0f0f0;margin:5px 0 0 0">
                  <div class="content" style="margin: 0">
                    <table style="width:100%">
                      <tr>
                        <td>
                          <h3 style="font-size: 16px;margin:0">`+val.orderNo+` (<span id="badgeStatus`+val.id+`">`+val.isStatus+`</span>)</h3>
                        </td>
                        <td style="text-align:right">
                          <p class="opacity-80 font-10">`+ dateFormat7(val.orderDate)+`</p>
                        </td>
                      </tr>
                    </table>
                    <table style="width:100%">
                      <tr>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pembeli</td>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pilihan</td>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pendapatan</td>
                      </tr>
                      <tr>
                        <td style="line-height:1;font-size:13px">`+val.guestName+`</td>
                        <td style="line-height:1;font-size:13px">`+val.guestShipment+`</td>
                        <td style="line-height:1;font-size:13px">Rp `+numberFormat(val.total)+`</td>
                      </tr>
                    </table>
                  </div>
                </button>
                <div id="order`+val.orderNo+`" class="collapse"  data-parent="#accordion" style="background:white;margin:10px 10px 5px 10px">
                  <div class="card card-style" style="margin: 0;border-radius: 0;border-bottom:1px solid #f0f0f0 !important">
                    <div class="content" style="margin: 0;padding:10px 15px">
                      <div class="row mb-2 mt-0">                
                        <h5 class="col-5 text-left font-13 opacity-60 font-500 mb-0">Nomor Pesanan</h5>
                        <h5 class="col-7 text-right font-13 mb-0">`+val.orderNo+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Tanggal</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+dateFormat3(val.orderDate)+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Pembeli</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestName+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Whatsapp</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestWa+` &nbsp<a href="https://wa.me/`+val.guestWa+`" target="_blank"><i class="fab fa-whatsapp color-green2-dark"></i></a></h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Email</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestEmail+`</h5>
                      </div>

                      <div class="divider" style="margin: 5px 0"></div>

                      <table style="width: 100%" id="tb_item`+val.id+`">
                        <thead>
                          <tr>
                            <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">DAFTAR PESANAN</h5></td>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <table style="width: 100%" id="tb_item_additional`+val.id+`">
                        <tbody>
                        </tbody>
                        <tfoot>`+discount+`</tfoot>
                      </table>
                      <div class="d-flex mb-0">
                        <div><h3 class="font-700 font-13 mb-0">Total</h3></div>
                        <div class="ml-auto"><h3 class="font-13 mb-0">`+numberFormat(val.total)+`</h3></div>
                      </div>

                      `+ shipment +`

                      <div class="divider" style="margin: 5px 0"></div>

                      <table style="width: 100%">
                        <thead>
                          <tr>
                            <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">PEMBAYARAN</h5></td>
                          </tr>
                        </thead>
                        <tbody>
                          `+payment+`
                        </tbody>
                      </table>
                      <div class="d-flex" id="button`+val.id+`">`+button+`<div>
                    </div>
                  </div>
                </div>
              </div>
            `);

            $('#tb_item'+val.id+' > tbody').empty();
            $('#tb_item_additional'+val.id+' > tbody').empty();
            $.each(val.order_item, function( j, value ) {
              if (value.additional == 'false') {
                if (value.poID != 0) {
                  $('#tb_item'+val.id+' > tbody').append(`
                    <tr>
                      <td style="text-align: left;">
                        <h5 class="font-500 font-13 mb-0">`+value.qty+` x `+value.productName+` (`+dateFormat2(value.poDate)+`)</h5>
                      </td>
                      <td style="text-align: right;">
                        <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                      </td>
                    </tr>
                  `);
                } else {
                  $('#tb_item'+val.id+' > tbody').append(`
                    <tr>
                      <td style="text-align: left;">
                        <h5 class="font-500 font-13 mb-0">`+value.qty+` x `+value.productName+`</h5>
                      </td>
                      <td style="text-align: right;">
                        <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                      </td>
                    </tr>
                  `);
                }
              } else {
                $('#tb_item_additional'+val.id+' > tbody').append(`
                  <tr>
                    <td style="text-align: left;">
                      <h5 class="font-500 font-13 mb-0">`+value.productName+`</h5>
                    </td>
                    <td style="text-align: right;">
                      <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                    </td>
                  </tr>
                `);
              }
            });
          });

          if(response.data == null){
            action = 'active';
          }else{
            action = "inactive";
          }
        } else if (response.status == false && start == 0) {
          $('#content').append(`
            <div class="card mb-0 mt-2">
              <div class="content" style="margin: 10px 15px;text-align:center">
                Belum ada pesanan
              </div>   
            </div>
          `);
        }
      },
    });
  }

  if(action == 'inactive'){
    action = 'active';
    get_data(limit, start);
  }

  $(window).scroll(function(){
    if($(window).scrollTop() + $(window).height() > $("#content").height() && action == 'inactive'){
      action = 'active';
      start = start + limit;
      setTimeout(function(){
        get_data(limit, start);
      }, 1000);
    }
  });
});

function get_shipment(){
  $('#shipmentType').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order/get_shipment/_',
    data: {
      id        : restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.data.isDelivery == 0) {
        $('#shipmentType').append(`<option value="delivery">Delivery</option>`);
      }
      if (response.data.isPickup == 0) {
        $('#shipmentType').append(`<option value="pickup">Pickup</option>`);
      }
      if (response.data.isDineIn == 0) {
        $('#shipmentType').append(`<option value="dinein">Dine In</option>`);
      }
    },
  });
}

function get_data_filter(){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order/get_data_filter/_',
      data: {
        id            : restoID,
        shipmentType  : $('#shipmentType').val(),
        orderFrom     : $('#orderFrom').val(),
        orderTo       : $('#orderTo').val(), 
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        $('#content').empty();
        if (response.status == true) {
          $('#content').append(`
            <div class="accordion" id="accordion"></div>
          `);
          $.each(response.data, function( i, val ) { 
            var margintop = i == 0 ? `margin:0` : `margin:5px 0 0 0`; 
            var discount  = val.discount == 0 ? '' : 
                            `<tr>
                              <td style="text-align: left;"><h5 class="font-500 font-13">Diskon</h5></td>
                              <td style="text-align: right;"><h5 class="font-13">`+numberFormat(val.discount)+`</h5></td>
                            </tr>`;
            var payment   = val.paymentID == 1 ? 
                            `<tr>
                              <td style="text-align: left;">
                                <h5 class="font-500 mb-0 font-13">`+val.paymentName+`</h5>
                              </td>
                              <td style="text-align: right;">
                                <h5 class="font-500 mb-0 font-13">`+val.accountName+`</h5>
                              </td>
                            </tr>` : `<tr>
                              <td style="text-align: left;">
                                <h5 class="font-500 mb-0 font-13">`+val.paymentName+`</h5>
                              </td>
                              <td style="text-align: right;">
                                <h5 class="font-500 mb-0 font-13">`+val.accountName+` / `+val.accountNo+`</h5>
                              </td>
                            </tr>`;
            var shipment  = val.guestShipment == 'delivery' ? 
                            `<div class="divider" style="margin: 5px 0"></div>
                            <div class="row mb-2 mt-0">        
                              <h5 class="col-12 font-14" style="margin:0">PENGIRIMAN</h5>        
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Alamat</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestAddress+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Kota</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestCity+`</h5>
                              <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Catatan</h5>
                              <h5 class="col-8 text-right font-13 mb-0">`+val.guestNote+`</h5>
                            </div>` : '';

            var button    = val.isStatus == 'Order' ? 
                            `<div class="flex-grow-1" style="margin-right:10px">
                              <a href="javascript:;" onclick="confirm_order(`+val.id+`, '`+val.orderNo+`', '`+val.guestShipment+`', '`+val.guestName+`', '`+val.guestAddress+`', `+val.total+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-2">Konfirmasi Pembayaran</a> 
                            </div>
                            <div class="flex-grow-1">
                              <a href="javascript:;" onclick="delete_order(`+val.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Hapus</a> 
                            </div>` : 
                            val.isStatus == 'Unpaid' ?
                            `<div class="flex-grow-1">
                              <a href="javascript:;" onclick="delete_order(`+val.id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Hapus</a> 
                            </div>` : '';

            $('#accordion').append(`
              <div class="mb-0">
                <button class="btn accordion-btn"  data-toggle="collapse" data-target="#order`+val.orderNo+`" style="background:white;padding: 5px 10px 10px 10px !important;border-bottom:1px solid #f0f0f0;margin:5px 0 0 0">
                  <div class="content" style="margin: 0">
                    <table style="width:100%">
                      <tr>
                        <td>
                          <h3 style="font-size: 16px;margin:0">`+val.orderNo+` (<span id="badgeStatus`+val.id+`">`+val.isStatus+`</span>)</h3>
                        </td>
                        <td style="text-align:right">
                          <p class="opacity-80 font-10">`+ dateFormat7(val.orderDate)+`</p>
                        </td>
                      </tr>
                    </table>
                    <table style="width:100%">
                      <tr>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pembeli</td>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pilihan</td>
                        <td style="width:33%;font-weight:400;line-height:1;font-size:11px">Pendapatan</td>
                      </tr>
                      <tr>
                        <td style="line-height:1;font-size:13px">`+val.guestName+`</td>
                        <td style="line-height:1;font-size:13px">`+val.guestShipment+`</td>
                        <td style="line-height:1;font-size:13px">Rp `+numberFormat(val.total)+`</td>
                      </tr>
                    </table>
                  </div>
                </button>
                <div id="order`+val.orderNo+`" class="collapse"  data-parent="#accordion" style="background:white;margin:10px 10px 5px 10px">
                  <div class="card card-style" style="margin: 0;border-radius: 0;border-bottom:1px solid #f0f0f0 !important">
                    <div class="content" style="margin: 0;padding:10px 15px">
                      <div class="row mb-2 mt-0">                
                        <h5 class="col-5 text-left font-13 opacity-60 font-500 mb-0">Nomor Pesanan</h5>
                        <h5 class="col-7 text-right font-13 mb-0">`+val.orderNo+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Tanggal</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+dateFormat3(val.orderDate)+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Pembeli</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestName+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Whatsapp</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestWa+`</h5>
                        <h5 class="col-4 text-left font-13 opacity-60 font-500 mb-0">Email</h5>
                        <h5 class="col-8 text-right font-13 mb-0">`+val.guestEmail+`</h5>
                      </div>

                      <div class="divider" style="margin: 5px 0"></div>

                      <table style="width: 100%" id="tb_item`+val.id+`">
                        <thead>
                          <tr>
                            <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">DAFTAR PESANAN</h5></td>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <table style="width: 100%" id="tb_item_additional`+val.id+`">
                        <tbody>
                        </tbody>
                        <tfoot>`+discount+`</tfoot>
                      </table>
                      <div class="d-flex mb-0">
                        <div><h3 class="font-700 font-13 mb-0">Total</h3></div>
                        <div class="ml-auto"><h3 class="font-13 mb-0">`+numberFormat(val.total)+`</h3></div>
                      </div>

                      `+ shipment +`

                      <div class="divider" style="margin: 5px 0"></div>

                      <table style="width: 100%">
                        <thead>
                          <tr>
                            <td colspan="2"><h5 class="col-12 font-14" style="padding:0;margin:0">PEMBAYARAN</h5></td>
                          </tr>
                        </thead>
                        <tbody>
                          `+payment+`
                        </tbody>
                      </table>
                      <div class="d-flex" id="button`+val.id+`">`+button+`<div>
                    </div>
                  </div>
                </div>
              </div>
            `);

            $('#tb_item'+val.id+' > tbody').empty();
            $('#tb_item_additional'+val.id+' > tbody').empty();
            $.each(val.order_item, function( j, value ) {
              if (value.additional == 'false') {
                if (value.poID != 0) {
                  $('#tb_item'+val.id+' > tbody').append(`
                    <tr>
                      <td style="text-align: left;">
                        <h5 class="font-500 font-13 mb-0">`+value.qty+` x `+value.productName+` (`+dateFormat2(value.poDate)+`)</h5>
                      </td>
                      <td style="text-align: right;">
                        <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                      </td>
                    </tr>
                  `);
                } else {
                  $('#tb_item'+val.id+' > tbody').append(`
                    <tr>
                      <td style="text-align: left;">
                        <h5 class="font-500 font-13 mb-0">`+value.qty+` x `+value.productName+`</h5>
                      </td>
                      <td style="text-align: right;">
                        <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                      </td>
                    </tr>
                  `);
                }
              } else {
                $('#tb_item_additional'+val.id+' > tbody').append(`
                  <tr>
                    <td style="text-align: left;">
                      <h5 class="font-500 font-13 mb-0">`+value.productName+`</h5>
                    </td>
                    <td style="text-align: right;">
                      <h5 class="font-13 mb-0">`+numberFormat(value.price)+`</h5>       
                    </td>
                  </tr>
                `);
              }
            });
          });

          if(response.data == null){
            action = 'active';
          }else{
            action = "inactive";
          }
        } else if (response.status == false) {
          $('#content').append(`
            <div class="card mb-0 mt-2">
              <div class="content" style="margin: 10px 15px;text-align:center">
                Pesanan tidak ditemukan
              </div>   
            </div>
          `);
        }
      },
    });
}

function confirm_order(id, orderNo, guestShipment, guestName, guestAddress, total){
  $('#divConfirm').empty();
  var address = guestShipment == 'delivery' ? 
                `<h5 class="col-4 text-left font-14 opacity-60 font-500">Alamat</h5>
                <h5 class="col-8 text-right font-15">`+guestAddress+`</h5>` : '';
  $('#divConfirm').append(`
    <h5 class="col-5 text-left font-14 opacity-60 font-500">Nomor Pesanan</h5>
    <h5 class="col-7 text-right font-15">`+orderNo+`</h5>
    <h5 class="col-4 text-left font-14 opacity-60 font-500">Pengiriman</h5>
    <h5 class="col-8 text-right font-15">`+guestShipment.toUpperCase() +`</h5>
    <h5 class="col-4 text-left font-14 opacity-60 font-500">Pembeli</h5>
    <h5 class="col-8 text-right font-15">`+guestName+`</h5>
    `+address);
  $('#buttonConfirm').empty();
  $('#buttonConfirm').append(`
    <h5 style="margin: 0 15px">Apakah anda telah menerima pembayaran sebesar Rp. `+ numberFormat(total)+`</h5>
    <div class="d-flex mb-3" style="margin:0 10px">
      <div class="flex-grow-1" style="margin-right:10px">
        <a href="javascript:;" onclick="status(`+id+`, '`+orderNo+`')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-2">Ya</a> 
      </div>
      <div class="flex-grow-1">
        <a href="javascript:;" onclick="orderProcess(`+id+`, '`+orderNo+`', '', 'Unpaid')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-2">Tidak</a> 
      </div>
    </div>`);
  $('#sheets_confirm').showMenu();
}

function status(id, orderNo){
  $('#divConfirm').empty();
  $('#divConfirm').append(`
    <div class="input-style input-style-2 input-required mt-0 mb-0" style="width:100%;margin:0 10px;">
      <input class="form-control" type="name" id="restoNote" name="restoNote" placeholder="Catatan untuk pembeli">
    </div>` 
  );
  $('#buttonConfirm').empty();
  $('#buttonConfirm').append(`
    <div class="d-flex mb-3" style="margin:0 10px">
      <div class="flex-grow-1" style="margin-right:10px">
        <a href="javascript:;" onclick="orderProcess(`+id+`, '`+orderNo+`', '`+ $('#restoNote').val() +`', 'Paid')" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-green2-dark mt-0">Proses Pemesanan</a> 
      </div>
    </div>`);
}

function orderProcess(id, orderNo, restoNote, isStatus) {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order/confirm/_',
    data: {
      id        : id,
      orderNo   : orderNo,
      restoNote : restoNote,
      isStatus  : isStatus,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    beforeSend: function() {
      $('#buttonConfirm').empty();
      $('#buttonConfirm').append(`
        <div class="d-flex justify-content-center">
          <div class="spinner-border color-red2-dark" role="status" style="margin-bottom:10px">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      `);
    },
    success: function(response) {
      if (response.code == '200') {
        $('#snackbar-success').toast('show');
        if (isStatus == 'Paid') {
          // $('#badgeStatus'+id).removeAttr('class');
          // $('#badgeStatus'+id).attr('class','fa fa-check-circle color-green1-dark mt-1');
          
          $('#badgeStatus'+id).html('Paid');
          $('#button'+id).html('');
        } else {
          // $('#badgeStatus'+id).removeAttr('class');
          // $('#badgeStatus'+id).attr('class','fa fa-times-circle color-red2-dark mt-1');
          
          $('#badgeStatus'+id).html('Unpaid');
          $('#button'+id).html(`
            <div class="flex-grow-1">
              <a href="javascript:;" onclick="delete_order(`+id+`)" class="btn btn-full btn-xs rounded-s font-800 text-uppercase bg-red2-dark mt-3">Hapus</a> 
            </div>
          `);
        }
        $('#sheets_confirm').hideMenu();
      }else{
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Gagal, coba lagi nanti');
        $('#sheets_confirm').hideMenu();
      }
    },
  });
}

function delete_order(id) {
  if (confirm('Anda yakin menghapus pesanan ini?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order/delete/_',
      data: {
        id        : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.status == true) {
          $('#snackbar-success').toast('show');
          setTimeout(function () {
            window.location.href = 'order_list';
          }, 100);
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}

function customer_list(){
  window.location.href = 'customer_list';
}