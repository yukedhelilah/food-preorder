$(document).ready(function() {
  get_data();
  get_method();

  if (restoType == 1) {
    $('#divDineIn').css('display','none');
  }
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order_setting/get_data/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.data.acceptOrder == 0) {
        $('#acceptOrder').prop("checked", true);
        $('#div-acceptOrder').attr("class", "switch-is-checked");
      } 

      $('#waNo').val(response.data.waNo); empty('waNo');
      $('#optional_eta').val(response.data.optional_eta); empty('optional_eta');
      $('#optional_desc').val(response.data.optional_desc); empty('optional_desc');

      if (response.data.isDelivery == 0) {
        $('#isDelivery').prop("checked", true);
        $('#div-isDelivery').attr("class", "switch-is-checked");
      }

      if (response.data.isPickup == 0) {
        $('#isPickup').prop("checked", true);
        $('#div-isPickup').attr("class", "switch-is-checked");
      }

      if (response.data.isDineIn == 0) {
        $('#isDineIn').prop("checked", true);
        $('#div-isDineIn').attr("class", "switch-is-checked");
      } 

      // setTimeout(function () {
      //   get_delivery();
      // }, 100);
    },
  });
}

// function get_delivery() {
//   $.ajax({
//     type: 'POST',
//     dataType: 'JSON',
//     url: configUrl + 'Order_setting/get_delivery/_',
//     data: {
//       id: restoID,
//     },
//     headers: {
//       'Content-Type': 'application/x-www-form-urlencoded'
//     },
//     success: function(response) {
//       if (response.status == true) {
//         $('#content_delivery').empty();
//         $.each(response.data, function( i, val ) {
//           var margintop = i == 0 ? `mt-3` : `mt-0`;
//           $('#content_delivery').append(`
//             <div class="`+margintop+` mb-2" style="border: 1px solid rgba(0, 0, 0, 0.1);border-radius: 10px !important;padding: 0 14px;line-height: 45px;height: 50px">
//               <table style="width: 100%">
//                 <tr>
//                   <td style="text-align: left;">
//                     <a href="javascript:;" onclick="edit_delivery(`+val.id+`,'`+val.area+`','`+val.rate+`','`+val.description+`')" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
//                       <i class="fa fa-edit" style="font-size: 10px"></i>
//                     </a>
//                   </td>
//                   <td style="text-align: center;">
//                     `+val.area+`
//                   </td>
//                   <td style="text-align: right;">
//                     <a href="javascript:;" onclick="delete_delivery(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
//                       <i class="fa fa-trash" style="font-size: 10px"></i>
//                     </a>
//                   </td>
//                 </tr>
//               </table>
//             </div>
//           `);
//         });
//       }
//     },
//   });
// }

// function add_delivery(){
//   $('#act').val('add');
//   $('#id').val('');
//   $('#restoID').val(restoID);
//   $('#area').val(''); empty('area');
//   $('#rate').val(''); empty('rate');
//   $('#description').val(''); empty('description');
//   $('#sheets_delivery').showMenu();
// }

// function edit_delivery(id, area, rate, description){
//   $('#act').val('edit');
//   $('#id').val(id);
//   $('#restoID').val(restoID);
//   $('#area').val(area); empty('area');
//   $('#rate').val(numberFormat(rate)); empty('rate');
//   $('#description').val(description); empty('description');
//   $('#sheets_delivery').showMenu();
// }

// function save_delivery(){
//   $.ajax({
//     type: 'POST',
//     dataType: 'JSON',
//     url: configUrl + 'Order_setting/save_delivery/_',
//     data: $('#mainform2').serialize(),
//     headers: {
//       'Content-Type': 'application/x-www-form-urlencoded'
//     },
//     success: function(response) {
//       if (response.code == '200') {
//         $('#snackbar-success').toast('show');
//         $('#sheets_delivery').hideMenu();
//         get_delivery();
//       }else{
//         $('#snackbar-error').toast('show');
//         $('#txt-error').html('Gagal, coba lagi nanti');
//         $('#sheets_delivery').hideMenu();
//         get_delivery();
//       }
//     },
//   });
// }

// function delete_delivery(id){
//   if (confirm('Apa anda yakin menghapus area ini?')) {
//     $.ajax({
//       type: 'POST',
//       dataType: 'JSON',
//       url: configUrl + 'Order_setting/delete_delivery/_',
//       data: {
//         id : id,
//       },
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded'
//       },
//       success: function(response) {
//         if (response.code == '200') {
//           $('#snackbar-success').toast('show');
//           get_delivery();
//         }else{
//           $('#snackbar-error').toast('show');
//           $('#txt-error').html('Gagal, coba lagi nanti');
//           get_delivery();
//         }
//       },
//     });
//   }
// }

function get_method() {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order_setting/get_method/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#content_method').empty();
        $.each(response.data, function( i, val ) {
          var margintop = i == 0 ? `mt-3` : `mt-0`;
          $('#content_method').append(`
            <div class="mt-0 mb-2" style="border: 1px solid rgba(0, 0, 0, 0.1);border-radius: 10px !important;padding: 0 14px;line-height: 45px;height: 50px">
              <table style="width: 100%">
                <tr>
                  <td style="text-align: left;">
                    <a href="javascript:;" onclick="payment_edit(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                      <i class="fa fa-edit" style="font-size: 10px"></i>
                    </a>
                  </td>
                  <td style="text-align: center;">
                    `+val.paymentName+`
                  </td>
                  <td style="text-align: right;">
                    <a href="javascript:;" onclick="delete_method(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                      <i class="fa fa-trash" style="font-size: 10px"></i>
                    </a>
                  </td>
                </tr>
              </table>
            </div>
          `);
        });
      }
    },
  });
}

function delete_method(id){
  if (confirm('Apa anda yakin menghapus metode pembayaran ini?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order_setting/delete_method/_',
      data: {
        id : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          get_method();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
          get_method();
        }
      },
    });
  }
}

function requiredField(val,field){
  var value   = $.trim(val);
  if (field == 'waNo') {
    var number  = value[0];

    if (number == '0') { $('#waNo').val('62'+$('#waNo').val().substr(1));}
  }
}

function empty(field) { 
  $('#label'+field).removeAttr('class'); 
  if ($('#'+field).val() == '') {
    $('#label'+field).attr('class','color-highlight');
  } else {
    $('#label'+field).attr('class','color-highlight input-style-1-active');
  }
}

function save(){
	$.ajax({
     type: 'POST',
     dataType: 'JSON',
     url: configUrl + 'Order_setting/save/_',
     data: $('#mainform').serialize()+"&restoID="+restoID,
     headers: {
       'Content-Type': 'application/x-www-form-urlencoded'
     },
     success: function(response) {
      if (response.code == '200') {
        $('#snackbar-success').toast('show');
        setTimeout(function () {
            window.location.href = 'setting';
        }, 100);
      }else{
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Gagal, coba lagi nanti');
      }
    },
  });
}

function schedule_custom() {
  window.location.href = 'schedule_custom';
}

function payment_new() {
  window.location.href = 'payment_new';
}

function payment_edit(id) {
  window.location.href = 'payment_edit?search='+btoa(id);
}