$(document).ready(function() {
  $('#restoID').val(restoID);
  get_payment();

  $("#file_img1").change(function () {
    uploadImg(1);
  });

  loadImg();
});

function get_payment() {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Order_setting/get_payment/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#paymentID').empty();
        $.each(response.data, function( i, val ) {
          $('#paymentID').append(`
            <option value="`+val.id+`">`+val.paymentName+`</option>
          `);
        });
      }
    },
  });
}

function upload1(){
  $("#file_img1").trigger('click');
}

function uploadImg(no){
  if (($("#file_img"+no))[0].files.length > 0) {
    var file = $("#file_img"+no)[0].files[0];
    var formdata = new FormData();
    formdata.append("file_img", file);
    formdata.append("no", no);
    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler2, false);
    ajax.open("POST", configUrl + "Order_setting/uploadImg/_");
    ajax.send(formdata);
  } else {
    alert("No file chosen!");
  }
}

function completeHandler2(event){
  var data = event.target.responseText.split('*');
  var no = data[2];
  if(data[0]!=''){
    $('#file_img'+no).val('');
    alert("Error! "+ data[1]);
  }else{
    $('#img'+no).val(data[1]);
    loadImg();
    $('#file_img'+no).val('');
  }   
}

function loadImg(){
  if($('#img1').val() == ''){
    $('#div-upload').css('display','block');
    $('#div-img').css('display','none');
  }else{
    $('#div-upload').css('display','none');
    $('#div-img').css('display','block');
    $("#img_product1").attr("src", configFile + "qris/"+$('#img1').val());
  }    
}

function removeImg(no){
  $('#img'+no).val('');
  loadImg();
}

function save(){
  if ($('#methodDelivery').prop('checked') == false && $('#methodPickup').prop('checked') == false && $('#methodDineIn').prop('checked') == false) {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Pilih salah satu metode pemesanan');
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Order_setting/save_method/_',
      data: $('#mainform').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          order_setting();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}

function order_setting() {
  window.location.href = 'order_setting';
}