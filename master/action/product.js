$(document).ready(function() {
  $('#search').removeAttr();
  if (restoType == 0) {
    $('#btnRegular').css('display','block');
    $('#search').attr('onKeyUp', 'get_data()');
    get_data();
  } else {
    $('#btnPO').css('display','block');
    $('#search').attr('onKeyUp', 'get_data_po()');
    get_data_po();
  }
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_data_by_resto/_',
    data:{
      id      : restoID,
      search  : $('#search').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#content').empty();
        if ($('#search').val() == '') {
          $('#content').append(`
            <div class="accordion" id="accordion1"></div>
          `);
          $.each(response.data, function( i, val ) {
          	$('#accordion1').append(`
              <div class="mb-0">
                <button class="btn accordion-btn"  data-toggle="collapse" data-target="#category`+val.id+`" style="background:white">
                  `+val.categoryName+`
                  <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </button>
                <div id="category`+val.id+`" class="collapse"  data-parent="#accordion1" style="background:white">
                </div>
              </div>
            `);

            if (val.product != 0) {
              $.each(val.product, function( j, value ) {
              	var img = ''; 
              	if (value.img == '') {
              		img = configFile + `no-image.png`;
              	} else {
              		img = configFile +  `product/`+value.img;
              	}
                
                var available   = value.available == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
                var active      = value.flag == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
       
                $('#category'+val.id).append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:10px">
                      <div class="row mb-0">
                        <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:15px !important">
                        <a href="javascript:;" onclick="edit_product(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-blue2-dark" style="width: 70px;position: absolute;margin-left: 15px !important;margin-top: 85px !important;padding: 2px !important;">Edit</a>
                        <div style="margin-left:100px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                          <div class="row" style="margin:0;width:100%">
                            <table style="border-bottom: 1px solid #f7f7f7;width:100%">
                              <tr>
                                <td>
                                  <h4>`+value.productName+`</h4>
                                </td>
                                <td style="text-align:right">
                                  <b>`+numberFormat(value.price)+`</b>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2" style="padding-bottom: 10px;">
                                  <p style="line-height:1.5">
                                    <small>`+value.name+` - `+dateFormat(value.createdDate)+`</small>
                                  </p>
                                </td>
                              </tr>
                            </table>
                            <table style="margin-bottom:0;width:100%">
                              <tr>
                                <td style="padding:5px 10px 0 0">
                                  Stok Tersedia
                                </td>
                                <td id="available`+value.id+`" onclick="available(`+value.id+`,'`+value.available+`')" style="text-align:right;padding:5px 0 0 10px">
                                  `+available+`
                                </td>
                              </tr>
                                <td style="padding:5px 10px 0 0">
                                  Aktifkan
                                </td>
                                <td id="active`+value.id+`" onclick="active(`+value.id+`,'`+value.flag+`')" style="text-align:right;padding:5px 0 0 10px">
                                  `+active+`
                                </td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              });
            } else {
              $('#category'+val.id).append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px;text-align:center">
                    Belum ada produk dalam kategori ini
                  </div>
                </div>
              `);
            }
          });
        } else {
          if (response.total == 0) {
            $('#content').append(`
              <div style="background:white;text-align:center;padding:10px 0">
                Produk tidak ditemukan
              </div>
            `); 
          } else {
            $('#content').append(`
              <div id="content-product" style="background:white">
              </div>
            `);

            $.each(response.data, function( i, value ) {
              var img = ''; 
              if (value.img == '') {
                img = configFile + `no-image.png`;
              } else {
                img = configFile +  `product/`+value.img;
              }
              
              var available   = value.available == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
              var active      = value.flag == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
     
              $('#content-product').append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px">
                    <div class="row mb-0">
                      <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:15px !important">
                      <a href="javascript:;" onclick="edit_product(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-blue2-dark" style="width: 70px;position: absolute;margin-left: 15px !important;margin-top: 85px !important;padding: 2px !important;">Edit</a>
                      <div style="margin-left:100px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                        <div class="row" style="margin:0;width:100%">
                          <table onclick="edit_product(`+value.id+`)" style="border-bottom: 1px solid #f7f7f7;width:100%">
                            <tr>
                              <td>
                                <h4>`+value.productName+`</h4>
                              </td>
                              <td style="text-align:right">
                                <b>`+numberFormat(value.price)+`</b>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="padding-bottom: 10px;">
                                <p style="line-height:1.5">
                                  <small>`+value.name+` - `+dateFormat(value.createdDate)+`</small>
                                </p>
                              </td>
                            </tr>
                          </table>
                          <table style="margin-bottom:0;width:100%">
                            <tr>
                              <td style="padding:5px 10px 0 0">
                                AVAILABLE
                              </td>
                              <td id="available`+value.id+`" onclick="available(`+value.id+`,'`+value.available+`')" style="text-align:right;padding:5px 0 0 10px">
                                `+available+`
                              </td>
                            </tr>
                              <td style="padding:5px 10px 0 0">
                                ACTIVE
                              </td>
                              <td id="active`+value.id+`" onclick="active(`+value.id+`,'`+value.flag+`')" style="text-align:right;padding:5px 0 0 10px">
                                `+active+`
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              `);          
            });
          }
        }
      } else {
        $('#content').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada produk
            </div>   
          </div>
        `);
      }
    },
  });
}

function get_data_po(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_data_by_resto_po/_',
    data:{
      id      : restoID,
      search  : $('#search').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#content').empty();
        if ($('#search').val() == '') {
          $('#content').append(`
            <div class="accordion" id="accordion1"></div>
          `);
          $.each(response.data, function( i, val ) {
            $('#accordion1').append(`
              <div class="mb-0">
                <button class="btn accordion-btn"  data-toggle="collapse" data-target="#category`+val.id+`" style="background:white">
                  `+val.categoryName+`
                  <i class="fa fa-chevron-down font-10 accordion-icon"></i>
                </button>
                <div id="category`+val.id+`" class="collapse"  data-parent="#accordion1" style="background:white">
                </div>
              </div>
            `);

            if (val.product != 0) {
              $.each(val.product, function( j, value ) {
                var img = ''; 
                if (value.img == '') {
                  img = configFile + `no-image.png`;
                } else {
                  img = configFile +  `product/`+value.img;
                }
                
                var available   = value.available == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
                var active      = value.flag == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
       
                $('#category'+val.id).append(`
                  <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                    <div class="content" style="margin:10px">
                      <div class="row mb-0">
                        <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:15px !important">
                        <div style="margin-left:100px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                          <div class="row" style="margin:0;width:100%">
                            <table style="border-bottom: 1px solid #f7f7f7;width:100%">
                              <tr>
                                <td>
                                  <h4>`+value.productName+`</h4>
                                </td>
                                <td style="text-align:right">
                                  <b>`+numberFormat(value.price)+`</b>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2" style="padding-bottom: 8px;">
                                  <p style="line-height:1.5">
                                    <small>`+value.name+` - `+dateFormat(value.createdDate)+`</small>
                                  </p>
                                </td>
                              </tr>
                            </table>
                            <table style="margin-bottom:0;border-top:1px solid #dedede;width:100%;">
                              <thead>
                                <tr>
                                  <td style="padding:10px 5px 5px 5px">
                                    <a href="javascript:;" onclick="edit_product(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-blue2-dark" style="padding: 3px 2px !important;font-size:10px !important;font-weight:normal">Edit</a>
                                  </td>
                                  <td style="padding:10px 5px 5px 5px;text-align:center">
                                    <a href="javascript:;" onclick="product_po(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-green2-dark" style="padding: 3px 2px !important;font-size:10px !important;font-weight:normal">Jadwal PO</a>
                                  </td>
                                </tr>
                              </thead>
                              <tbody id="product`+value.id+`"></tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                `);
              });
            } else {
              $('#category'+val.id).append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px;text-align:center">
                    Belum ada produk dalam kategori ini
                  </div>
                </div>
              `);
            }
          });
        } else {
          if (response.total == 0) {
            $('#content').append(`
              <div style="background:white;text-align:center;padding:10px 0">
                Produk tidak ditemukan
              </div>
            `); 
          } else {
            $('#content').append(`
              <div id="content-product" style="background:white">
              </div>
            `);

            $.each(response.data, function( i, value ) {
              var img = ''; 
              if (value.img == '') {
                img = configFile + `no-image.png`;
              } else {
                img = configFile +  `product/`+value.img;
              }
              
              var available   = value.available == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
              var active      = value.flag == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
     
              $('#content-product').append(`
                <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
                  <div class="content" style="margin:10px">
                    <div class="row mb-0">
                      <img src="`+img+`" class="rounded-s mx-auto mt-1" style="width: 70px;position: absolute;margin-left:15px !important">
                      <a href="javascript:;" onclick="edit_product(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-blue2-dark" style="width: 70px;position: absolute;margin-left: 15px !important;margin-top: 85px !important;padding: 2px !important;font-size:10px !important;font-weight:normal">Edit</a>
                      <a href="javascript:;" onclick="product_po(`+value.id+`)" class="btn btn-xs btn-full mb-0 rounded-xs text-uppercase font-900 shadow-s bg-green2-dark" style="width: 70px;position: absolute;margin-left: 15px !important;margin-top: 115px !important;padding: 2px !important;font-size:10px !important;font-weight:normal">Jadwal PO</a>
                      <div style="margin-left:100px !important;margin-right: 15px !important;width: -webkit-fill-available !important;">
                        <div class="row" style="margin:0;width:100%">
                          <table onclick="edit_product(`+value.id+`)" style="border-bottom: 1px solid #f7f7f7;width:100%">
                            <tr>
                              <td>
                                <h4>`+value.productName+`</h4>
                              </td>
                              <td style="text-align:right">
                                <b>`+numberFormat(value.price)+`</b>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2" style="padding-bottom: 10px;">
                                <p style="line-height:1.5">
                                  <small>`+value.name+` - `+dateFormat(value.createdDate)+`</small>
                                </p>
                              </td>
                            </tr>
                          </table>
                          <table style="margin-bottom:0;width:100%;border-collapse:collapse;border:1px solid #dedede" border="1">
                            <thead>
                              <tr>
                                <td style="padding:5px">
                                  Tanggal
                                </td>
                                <td style="padding:5px;text-align:center">
                                  Slot
                                </td>
                                <td style="padding:5px;text-align:center">
                                  Sisa
                                </td>
                              </tr>
                            </thead>
                            <tbody id="product`+value.id+`"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              `); 

              if (value.po != 0) {
                $.each(value.po, function( k, values ) {
                  $('#product'+value.id).append(`
                    <tr>
                      <th style="padding:5px">
                        `+dateFormat5(values.poDate)+`
                      </th>
                      <th style="padding:5px;text-align:center">
                        `+values.allotment+`
                      </th>
                      <th style="padding:5px;text-align:center;color:red">
                        `+(values.allotment - values.sold)+`
                      </th>
                    </tr>
                  `);
                })
              } else {
                $('#product'+value.id).append(`
                  <tr>
                    <td colspan="3" style="text-align:center;padding:5px">
                      Belum ada jadwal PO mendatang
                    </td>
                  </tr>
                `);
              }         
            });
          }
        }
      } else {
        $('#content').append(`
          <div class="card mb-0">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Belum ada produk
            </div>   
          </div>
        `);
      }
    },
  });
}

function addProduct(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_data_by_resto/_',
    data:{
      id  : restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if(response.status == false){
        alert('Buat kategori untuk menambahkan produk baru');
      } else {
        window.location.href = 'product_new';
      }
    },
  });
}

function addCategory(){
  window.location.href = 'category';
}

function bulkPO(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_data_by_resto/_',
    data:{
      id  : restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if(response.status == false){
        alert('Buat kategori untuk menambahkan jadwal PO massal');
      } else {
        window.location.href = 'bulk_po';
      }
    },
  });
}

function active(id, flag){
  var active  = flag == 0 ? 1 : 0;
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/flag_product/_',
    data: {
      id   : id,
      flag : active,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        var active2 = active == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
        $('#active'+id).html(active2);
        $('#active'+id).removeAttr('onclick');
        $('#active'+id).attr('onclick','active('+id+','+active+')');
      } else {
        // $('#snackbar-error').toast('show');
      }
    },
  });
}

function available(id, flag){
  var available  = flag == 0 ? 1 : 0;
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/available_product/_',
    data: {
      id        : id,
      available : available,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        var available2 = available == 0 ? `<i class="fa fa-toggle-on color-green2-dark" style="font-size:25px"></i>` : `<i class="fa fa-toggle-off color-red2-dark" style="font-size:25px"></i>`;
        $('#available'+id).html(available2);
        $('#available'+id).removeAttr('onclick');
        $('#available'+id).attr('onclick','available('+id+','+available+')');
      } else {
        // $('#snackbar-error').toast('show');
      }
    },
  });
}

function edit_product(id){
  window.location.href = `product_edit?search=`+btoa(id);
}

function product_po(id){
  window.location.href = `product_po?search=`+btoa(id);
}