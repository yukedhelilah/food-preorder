var id  = atob(getUrlParameter('search'))

$(document).ready(function() {
	get_category();
  get_details();

  $(".editor").each(function () {
    let id = $(this).attr('id');
    CKEDITOR.replace(id);
  });

  $("#file_img1").change(function () {
    uploadImg(1);
  });

  setTimeout(function () {
    loadImg();
  }, 1000);
});

function get_category(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_data_by_resto/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	$('#categoryID').append(`
          <option value="`+val.id+`">`+val.categoryName+`</option>
        `);
      });
    },
  });
}

function get_details(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_details/_',
    data: {
      id: id,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#id').val(id);
      $('#categoryID').val(response.data.categoryID);
      $('#productName').val(response.data.productName);
      $('#price').val(numberFormat(response.data.price));
      $('#img1').val(response.data.img);
      CKEDITOR.instances['description'].setData(response.data.description);
    },
  });
}

function upload1(){
  $("#file_img1").trigger('click');
}

function uploadImg(no){
  if (($("#file_img"+no))[0].files.length > 0) {
    var file = $("#file_img"+no)[0].files[0];
    var formdata = new FormData();
    formdata.append("file_img", file);
    formdata.append("no", no);
    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler2, false);
    ajax.open("POST", configUrl + "Product/uploadImg/_");
    ajax.send(formdata);
  } else {
    alert("No file chosen!");
  }
}

function completeHandler2(event){
  var data = event.target.responseText.split('*');
  var no = data[2];
  if(data[0]!=''){
    $('#file_img'+no).val('');
    alert("Error! "+ data[1]);
  }else{
    $('#img'+no).val(data[1]);
    loadImg();
    $('#file_img'+no).val('');
  }   
}

function loadImg(){
  if($('#img1').val() == ''){
    $('#div-upload').css('display','block');
    $('#div-img').css('display','none');
  }else{
    $('#div-upload').css('display','none');
    $('#div-img').css('display','block');
    $("#img_product1").attr("src", configFile + "product/"+$('#img1').val());
  }    
}

function removeImg(no){
  $('#img'+no).val('');
  loadImg();
}

function save(){
  if ($('#productName').val() == '') {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Nama produk harus diisi');
  } else if ($('#price').val() == '') {
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Harga harus diisi');
  } else {
    for (instance in CKEDITOR.instances){
      CKEDITOR.instances[instance].updateElement();
    }

    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/save/_',
      data: $('#mainform').serialize()+"&restoID="+restoID+"&staffID="+staffID,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == 200) {
          $('#snackbar-success').toast('show');
          setTimeout(function () {
            window.location.href = 'product';
          }, 100);
        } else {
          $('#snackbar-error').toast('show');
        }
      },
    });
  }
}

function delete_product(){
  if (confirm('Are you sure to delete this product')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/delete/_',
      data: {
        id : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == 200) {
          window.location.href = 'product';
        } else {
          alert('failed, try again later');
          window.location.href = 'product';
        }
      },
    });
  }
}