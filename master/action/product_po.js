var productID   = atob(getUrlParameter('search'))

$(document).ready(function() {
	get_data();

  $("#checkedAll").change(function () {
    if ($("#checkedAll").prop('checked') == true) {
      $('.checkedPO').prop('checked', true);
      $('#btn_deleteSelectedPO').css('display','block');
    } else {
      $('.checkedPO').prop('checked', false);
      $('#btn_deleteSelectedPO').css('display','none');
    } 
  });
}); 

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_po/_',
    data:{
      id      : productID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    $('#content').empty();
      $('#productName').html(response.details.productName);
      if (response.data == null) {
        $('#content').append(`
          <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
            <div class="content" style="margin:5px 15px">
              <table style="width:100%">
                <tr>
                  <td style="text-align:center;">Belum ada jadwal PO</td>
                </tr>
              </table>
            </div>
          </div>
        `);
      } else {
		    $.each(response.data, function( i, val ) {
          $('#content').append(`
            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
              <div class="content" style="margin:5px 15px">
                <table style="width:100%">
                  <tr>
                    <td style="width:10%">
                      <input type="checkbox" class="checkedPO" name="checkedPO[]" id="checkedPO`+val.id+`" value="`+val.id+`" onchange="checkedPO(`+val.id+`)">
                    </td>
                    <td style="padding:5px;width:25%">`+dateFormat2(val.poDate)+`</td>
                    <td style="text-align:center;padding:5px;width:20%">`+val.allotment+`</td>
                    <td class="color-red2-dark" style="text-align:center;padding:5px;width:20%">`+ (val.allotment - val.sold) +`</td>
                    <td style="text-align:center;padding:5px;width:25%">
                      <a href="javascript:;" onclick="product_po_edit(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                        <i class="fa fa-edit" style="font-size: 10px"></i>
                      </a>&nbsp
                      <a href="javascript:;" onclick="delete_po(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
                        <i class="fa fa-trash" style="font-size: 10px"></i>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          `);
		    });
      }
    },
  });
}

function checkedPO(id){
  if ($(".checkedPO").prop('checked') == false) {
    $('#btn_deleteSelectedPO').css('display','none');
  } else {
    $('#btn_deleteSelectedPO').css('display','block');
  } 

  var checked = 0;
  $("input[name='checkedPO[]']:checked").each(function (){
    checked += 1;
  });

  if(checked == 0){
    $("#checkedAll").prop('checked',false);
    $('#btn_deleteSelectedPO').css('display','none');
  } else {
    $('#btn_deleteSelectedPO').css('display','block');
  } 
}

function delete_selected_po() {
  if (confirm('Apa anda yakin menghapus jadwal PO yang telah dipilih?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/delete_selected_po/_',
      data: $('#mainform').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          $("#checkedAll").prop('checked',false);
          $('#btn_deleteSelectedPO').css('display','none');
          get_data();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
          get_data();
        }
      },
    });
  }
}

function delete_po(id){
  if (confirm('Apa anda yakin menghapus jadwal PO ini?')) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/delete_po/_',
      data: {
        id : id,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          get_data();
        }else{
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
          get_data();
        }
      },
    });
  }
}

function product_po_new() {
  window.location.href = 'product_po_new?search='+btoa(productID);
}

function product_po_edit(id) {
  window.location.href = 'product_po_edit?search='+btoa(id);
}