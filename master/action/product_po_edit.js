var id  = atob(getUrlParameter('search'))

$(document).ready(function() {
  get_data();
}); 

function get_data() {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Product/get_po_detail/_',
    data: {
      id : id,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#restoID').val(restoID);
      $('#productID').val(response.data.productID);
      $('#id').val(response.data.id);
      $('#poDate').val(response.data.poDate);
      $('#allotment').val(response.data.allotment);
    },
  });
}

function save(){
  if($('#allotment').val() == ''){
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Slot harus diisi');
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/save_po/_',
      data: $('#mainform').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          product_po();
        } else {
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}

function product_po() {
  window.location.href = 'product_po?search='+btoa($('#productID').val());
}