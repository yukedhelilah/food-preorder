var productID  = atob(getUrlParameter('search'))

$(document).ready(function() {
  $('#restoID').val(restoID);
  $('#productID').val(productID);

  for (var i = getYear(); i <= parseInt(getYear())+3; i++) {
    $('#yearFrom').append(`
      <option value="`+i+`">`+i+`</option>
    `);
    $('#yearTo').append(`
      <option value="`+i+`">`+i+`</option>
    `);
  }

  for (var i = getMonth(); i <= 12; i++) {
    $('#monthFrom').append(`
      <option value="`+i+`">`+bulan[i-1]+`</option>
    `);
    $('#monthTo').append(`
      <option value="`+i+`">`+bulan[i-1]+`</option>
    `);
  }

  var d;
  if (getMonth() == 2) {
    d = 28;
  } else if (getMonth() == 4 || getMonth() == 6 || getMonth() == 4 || getMonth() == 9 || getMonth() == 11) {
    d = 30;
  } else {
    d = 31;
  }

  for (var i = getDate(); i <= d; i++) {
    var date    = i.toString().length == 2 ? i : `0`+i;
    $('#dateFrom').append(`
      <option value="`+i+`">`+date+`</option>
    `);
    $('#dateTo').append(`
      <option value="`+i+`">`+date+`</option>
    `);
  }

  $("#yearFrom").change(function () {
    $('#monthFrom').empty();
    $('#dateFrom').empty();
    if ($(this).val() != getYear()) {
      for (var i = 1; i <= 12; i++) {
        $('#monthFrom').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      for (var i = getMonth(); i <= 12; i++) {
        $('#monthFrom').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = getDate(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }


    $('#yearTo').empty();
    $('#monthTo').empty();
    $('#dateTo').empty();
    for (var i = $('#yearFrom').val(); i <= parseInt($('#yearFrom').val())+3; i++) {
      $('#yearTo').append(`
        <option value="`+i+`">`+i+`</option>
      `);
    }
    if ($(this).val() != getYear()) {
      for (var i = 1; i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      for (var i = getMonth(); i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = getDate(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $('#monthFrom').change(function () {
    $('#dateFrom').empty();
    $('#dateTo').empty();
    if ($('#yearFrom').val() == getYear() && $('#monthFrom').val() == getMonth()) {
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = getDate(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthFrom').val() == 2) {
        d2 = 28;
      } else if ($('#monthFrom').val() == 4 || $('#monthFrom').val() == 6 || $('#monthFrom').val() == 4 || $('#monthFrom').val() == 9 || $('#monthFrom').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateFrom').append(`
          <option value="`+i+`">`+date+`</option>
        `);
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }

    $('#monthTo').empty();
    if ($('#yearTo').val() > $('#yearFrom').val()) {
      for (var i = 1; i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    } else {
      for (var i = $('#monthFrom').val(); i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    }
  });

  $('#dateFrom').change(function () {
    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = $('#dateFrom').val(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $("#yearTo").change(function () {
    $('#monthTo').empty();
    if ($("#yearTo").val() > $("#yearFrom").val()) {
      for (var i = 1; i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    } else {
      for (var i = $('#monthFrom').val(); i <= 12; i++) {
        $('#monthTo').append(`
          <option value="`+i+`">`+bulan[i-1]+`</option>
        `);
      }
    }

    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = $('#dateFrom').val(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });

  $("#monthTo").change(function () {
    $('#dateTo').empty();
    if ($('#yearFrom').val() == $('#yearTo').val() && $('#monthFrom').val() == $('#monthTo').val()) {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = $('#dateFrom').val(); i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    } else {
      var d2;
      if ($('#monthTo').val() == 2) {
        d2 = 28;
      } else if ($('#monthTo').val() == 4 || $('#monthTo').val() == 6 || $('#monthTo').val() == 4 || $('#monthTo').val() == 9 || $('#monthTo').val() == 11) {
        d2 = 30;
      } else {
        d2 = 31;
      }
      for (var i = 1; i <= d2; i++) {
        var date    = i.toString().length == 2 ? i : `0`+i;
        $('#dateTo').append(`
          <option value="`+i+`">`+date+`</option>
        `);
      }
    }
  });
}); 

function getYear() {
  var format  = new Date();
  return format.getFullYear().toString();
}

function getMonth() {
  var format  = new Date();
  return format.getMonth()+1;
}

function getDate() {
  var format  = new Date();
  return format.getDate();
}

function save(){
  if($('#allotment').val() == ''){
    $('#snackbar-error').toast('show');
    $('#txt-error').html('Slot harus diisi');
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Product/save_po/_',
      data: $('#mainform').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(response) {
        if (response.code == '200') {
          $('#snackbar-success').toast('show');
          product_po();
        } else {
          $('#snackbar-error').toast('show');
          $('#txt-error').html('Gagal, coba lagi nanti');
        }
      },
    });
  }
}

function product_po() {
  window.location.href = 'product_po?search='+btoa(productID);
}