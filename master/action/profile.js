$(document).ready(function() {
    get_city();
	get_data();

	$("#file_img1").change(function () {
		uploadImg(1);
	});

    setTimeout(function () {
		loadImg();
    }, 3000);

    $('#isBranch').click(function(){
	    if($(this).is(':checked')){
	    } else {
	    	$('#branchName').val('');
	    }
	});

    if (restoType == 1) {
        $('#divBranch').css('display','none');
    }
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Profile/get_details/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
    	$('#id').val(response.data.id);
    	$('#restoName').val(response.data.restoName); empty('restoName');
    	$('#restoID').val(response.data.username); empty('restoID');
    	$('#restoIDCopy').val(response.data.username);
    	if (response.data.username != '') {$('#isrestoID').val(1);}
    	$('#companyName').val(response.data.companyName).attr("selected", "selected"); empty('companyName');
    	$('#cityID').val(response.data.cityID); empty('cityID');
    	$('#address').val(response.data.address); empty('address');
        if (response.data.isAddress == 0) {
            $('#isAddress').prop("checked", true);
        } else {
            $('#isAddress').prop("checked", false);
        }
        if (response.data.latitude != '') {
            $('.location-show').css('display','block');
            $('#latitude').val(response.data.latitude); empty('latitude');
            $('#longitude').val(response.data.longitude); empty('longitude');
            $('#lokasi').html(`<iframe src="https://maps.google.com/maps?q=`+response.data.latitude+`,`+response.data.longitude+`&hl=en&z=16&amp;'+'&output=embed" width="100%"></iframe>`);
        }
    	$('#email').val(response.data.email); empty('email');
    	$('#phone').val(response.data.phone); empty('phone');
    	$('#cpName').val(response.data.cpName); empty('cpName');
    	$('#cpEmail').val(response.data.cpEmail); empty('cpEmail');
    	$('#cpEmailCopy').val(response.data.cpEmail);
    	if (response.data.cpEmail != '') {$('#iscpEmail').val(1);}
    	$('#cpMobile').val(response.data.cpMobile); empty('cpMobile');
    	$('#cpMobileCopy').val(response.data.cpMobile);
    	if (response.data.cpMobile != '') {$('#iscpMobile').val(1);}
    	$('#img1').val(response.data.logo);
    	$('#branchName').val(response.data.branchName); empty('branchName');
		$('#div-branch').removeAttr("class");
    	if (response.data.isBranch == 1) {
    		$('#isBranch').prop("checked", true);
    		$('#div-branch').attr("class", "switch-is-checked");
    	} else {
    		$('#isBranch').prop("checked", false);
    		$('#div-branch').attr("class", "switch-is-unchecked");
    	}
    },
  });
}

function get_city(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_city/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
        $('#cityID').append(`
          <option value="`+val.id+`">`+val.cityName+`</option>
        `);
      });
    },
  });
}

function requiredField(val,field){
    var value 	= $.trim(val);

    if (field == 'restoID'){
    	if(value.length > 2){
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Register/check/_',
	            data: {
	            	check : value,
	            	type  : 'restoID',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
	                	if (value == $('#restoIDCopy').val()) {
							valid(field);
							$('#snackbar-error').toast('hide');
	                	} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Resto ID already exist');
	                	}
	                }
	            }
	        });	
    	} else {
    		invalid(field);
			$('#snackbar-error').toast('hide');
    	}
    } else if (field == 'cpEmail') {
    	var re = /\S+@\S+\.\S+/;
    	if (re.test(value) == true){
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Register/check/_',
	            data: {
	            	check : value,
	            	type  : 'cpEmail',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
	                	if (value == $('#cpEmail').val()) {
							valid(field);
							$('#snackbar-error').toast('hide');
						} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Email already exist');
						}
	                }
	            }
	        });
        } else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'cpMobile') {
    	if (value.length == 1 && value == '0') { $('#cpMobile').val('62');}
    	if (value.length > 9) { 
    		valid(field);
    	} else { 
    		invalid(field);
    	}
    } else {
    	if(value.length > 2) { 
    		valid(field);
    	} else { 
    		invalid(field);
    	}
    }
}

function valid(field) { $('#is'+field).val(1); }
function invalid(field) { $('#is'+field).val(0); }

function empty(field) { 
    $('#label'+field).removeAttr('class'); 
    if ($('#'+field).val() == '') {
        $('#label'+field).attr('class','color-highlight');
    } else {
        $('#label'+field).attr('class','color-highlight input-style-1-active');
    }
}

function upload1(){
  $("#file_img1").trigger('click');
}

function uploadImg(no){
  if (($("#file_img"+no))[0].files.length > 0) {
    var file = $("#file_img"+no)[0].files[0];
    var formdata = new FormData();
    formdata.append("file_img", file);
    formdata.append("no", no);
    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler2, false);
    ajax.open("POST", configUrl + "Profile/uploadImg/_");
    ajax.send(formdata);
  } else {
    alert("No file chosen!");
  }
}

function completeHandler2(event){
  var data = event.target.responseText.split('*');
  var no = data[2];
  if(data[0]!=''){
    $('#file_img'+no).val('');
    alert("Error! "+ data[1]);
  }else{
    $('#img'+no).val(data[1]);
    loadImg();
    $('#file_img'+no).val('');
  }   
}

function loadImg(){
  if($('#img1').val() == ''){
    $("#img_logo1").attr("src", configFile + "no-image.png");
    $('#remove-logo').css('display','none');
  }else{
    $("#img_logo1").attr("src", configFile + "logo/"+$('#img1').val());
    $('#remove-logo').css('display','block');
  }    
}

function removeImg(no){
  $('#img'+no).val('');
  loadImg();
}

function save(){
	if ($('#isrestoID').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Resto ID is not available/valid');
	} else if ($('#iscpEmail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email is not available/valid');
	} else if ($('#iscpMobile').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Contact person mobile is not valid');
	} else {
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Profile/save/_',
		    data: $('#mainform').serialize(),
		    headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
	            if (response.code == '200') {
					$('#snackbar-success').toast('show');
                    setTimeout(function () {
                        window.location.href = 'setting';
                    }, 100);
	            }else{
					$('#snackbar-error').toast('show');
			        $('#txt-error').html('Failed, try again later.');
	            }
		    },
		});
	}
}

function getPosition() {
    var options = {
        enableHighAccuracy: true
    }

    var onSuccess = function(position) {
        var latitude; var longitude;
        if ($('#latitude').val() == '') {
            latitude    = position.coords.latitude;
            longitude   = position.coords.longitude; 
            $('#latitude').val(position.coords.latitude); empty('latitude');
            $('#longitude').val(position.coords.longitude); empty('longitude');
        } else {
            latitude    = $('#latitude').val();
            longitude   = $('#longitude').val();
        }
        $('.location-show').css('display','block');
        $('#lokasi').html(`<iframe src="https://maps.google.com/maps?q=`+latitude+`,`+longitude+`&hl=en&z=16&amp;'+'&output=embed" width="100%"></iframe>`);
    };

    var onError = function(error) {
        alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
}