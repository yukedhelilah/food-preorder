$(document).ready(function() {
	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Profile/get_details/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
        $('#qr_url').html("<a href='https://dapur123.com/"+response.data.url+"' target='blank'>https://dapur123.com/"+response.data.url+"</a>");
        $('#qrcode').qrcode({width: 200, height: 200, text: "https://dapur123.com/"+response.data.url});
        $('#share').html(`
          Bagikan ke : &nbsp
          <a href="javascript:;" onclick="copyToClipboard('#qr_url')" data-toggle="tooltip" title="Salin link"><i class="fa fa-paperclip font-18 color-dark2-dark"></i></a>&nbsp&nbsp
          <a href="whatsapp://send?text=https://dapur123.com/`+response.data.url+`" data-action="share/whatsapp/share"><i class="fab fa-whatsapp font-20 color-green2-dark"></i></a>&nbsp&nbsp
          <a href="https://www.facebook.com/sharer.php?u=https://dapur123.com/`+response.data.url+`" target="_blank"><i class="fab fa-facebook font-20"></i></a>&nbsp&nbsp
          <a href="https://twitter.com/share?url=https://dapur123.com/`+response.data.url+`" target="_blank"><i class="fab fa-twitter font-20"></i></a>&nbsp&nbsp
          <a href="mailto:?Subject=DAPUR123.COM&amp;Body=https://dapur123.com/`+response.data.url+`" target="_blank"><i class="fa fa-envelope font-20 color-red2-dark"></i></a>&nbsp&nbsp
        `);
    },
  });
}

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  $('#snackbar-success').toast('show');
}