var configUrl       = 'https://dapur123.com/server.php/Master/';
// var configUrl       = 'https://creatrixorganizer.com/resto/Master/';

$(document).ready(function() {
	get_city();
	$('#snackbar-error').toast('hide');

	get_hour();
  	$("#allHourOpen").change(function () {
    	$('#monHourOpen').val($(this).val());
    	$('#tueHourOpen').val($(this).val());
    	$('#wedHourOpen').val($(this).val());
    	$('#thuHourOpen').val($(this).val());
    	$('#friHourOpen').val($(this).val());
    	$('#satHourOpen').val($(this).val());
    	$('#sunHourOpen').val($(this).val());
  	});
  	$("#allHourClose").change(function () {
    	$('#monHourClose').val($(this).val());
    	$('#tueHourClose').val($(this).val());
    	$('#wedHourClose').val($(this).val());
    	$('#thuHourClose').val($(this).val());
    	$('#friHourClose').val($(this).val());
    	$('#satHourClose').val($(this).val());
    	$('#sunHourClose').val($(this).val());
  	});
});

function get_hour(){
	let $hour = $(".hour");

	for (let hr = 0; hr < 24; hr++) {

	  let hrStr = hr.toString().padStart(2, "0") + ":";

	  let val = hrStr + "00";
	  $hour.append('<option val="' + val + '">' + val + '</option>');

	  val = hrStr + "30";
	  $hour.append('<option val="' + val + '">' + val + '</option>')

	}
}

function back2register(){
    window.location.href = 'register';
}

function next(no){
	var next = no+1;
	var re = /\S+@\S+\.\S+/;
	
	if (no == 1) {
		if ($('#isrestoName').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nama usaha harus diisi');
        } else {
			$('.step').css('display','none');
			$('#step'+next).css('display','block');
        }
	} else if (no == 2) {
		if ($('#iscpName').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nama pemilik harus diisi');
		} else if ($('#cpEmail').val() == '' && $('#iscpEmail').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Email harus diisi');
        } else if (re.test($('#cpEmail').val()) == false && $('#iscpEmail').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Email harus diisi dengan benar');
        } else if (re.test($('#cpEmail').val()) == true && $('#iscpEmail').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Email telah terdaftar');
        } else if ($('#cpMobile').val() == '' && $('#iscpMobile').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nomor HP harus diisi');
        } else if ($('#cpMobile').val().length <= 9 && $('#iscpMobile').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nomor HP harus diisi dengan benar');
        } else if ($('#cpMobile').val().length > 9 && $('#iscpMobile').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nomor HP telah terdaftar');
        } else {
			$('.step').css('display','none');
			$('#step'+next).css('display','block');
		}
	} else if (no == 3) {
		if ($('#monOpen').prop('checked')) {
			if ($('#monHourOpen').val() == '' || $('#monHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari senin harus diisi');
			} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#tueOpen').prop('checked')) {
        	if ($('#tueHourOpen').val() == '' || $('#tueHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari selasa harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#wed').prop('checked')) {
        	if ($('#wedHourOpen').val() == '' || $('#wedHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari rabu harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#thuOpen').prop('checked')) {
        	if ($('#thunHourOpen').val() == '' || $('#thuHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari kamis harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#friOpen').prop('checked')) {
        	if ($('#friHourOpen').val() == '' || $('#friHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari jumat harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#satOpen').prop('checked')) {
        	if ($('#satHourOpen').val() == '' || $('#satHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari sabtu harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#sunOpen').prop('checked')) {
        	if ($('#sunHourOpen').val() == '' || $('#sunHourClose').val() == '') {
				$('#snackbar-error').toast('show');
	            $('#txt-error').html('Jam operasional hari minggu harus diisi');
        	} else {
				$('.step').css('display','none');
				$('#step'+next).css('display','block');
			}
        } else if ($('#monOpen').not(':checked') && $('#tueOpen').not(':checked') && $('#wedOpen').not(':checked') && $('#thuOpen').not(':checked') && $('#friOpen').not(':checked') && $('#satOpen').not(':checked') && $('#sunOpen').not(':checked')) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Pilih setidaknya salah satu hari buka');
        } 
	} else if (no == 4) {
		$('.step').css('display','none');
		$('#step'+next).css('display','block');
	} else if (no == 5) {
		if ($('#isPickup').prop('checked') && $('#optional_eta').val() == '') {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Estimasi persiapan harus diisi');
		} else if ($('#waNo').val() == '' && $('#iswaNo').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nomor Whatsapp harus diisi');
        } else if ($('#waNo').val().length <= 9 && $('#iswaNo').val() == 0) {
			$('#snackbar-error').toast('show');
            $('#txt-error').html('Nomor Whatsapp harus diisi dengan benar');
        } else {
			$('.step').css('display','none');
			$('#step'+next).css('display','block');
		}
	}
}

function back(no){
	var back = no-1;
	$('.step').css('display','none');
	$('#step'+back).css('display','block');
}

function regular(type){
	$('#cutoffday').empty();
	if (type == 0) {
		$('#divCutoffday').css('display','none');
    	$('#cutoffday').append(`
	        <option value="0">0</option>
        `);
	} else {
		$('#divCutoffday').css('display','block');
    	$('#cutoffday').append(`
	        <option value="1">1</option>
	        <option value="2">2</option>
	        <option value="3">3</option>
	        <option value="4">4</option>
	        <option value="5">5</option>
	        <option value="6">6</option>
	        <option value="7">7</option>
        `);
	}
}

function get_city(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Category/get_city/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $.each(response.data, function( i, val ) {
      	$('#cityID').append(`
          <option value="`+val.id+`">`+val.cityName+`</option>
        `);
      });
    },
  });
}

function requiredField(val,field){
    var value 	= $.trim(val);

    if (field == 'restoName') {
    	if (value.length == 0){
	    	$('#branchName').val('');
            $('#labelbranchName').css('background','#e9ecef');
	    	$('#labelbranchName').removeAttr('class');
            $('#branchName').attr('readonly', true);
			origin(field);
			origin('branchName');
        } else if (value.length > 3){
            $('#labelbranchName').css('background','white');
            $('#branchName').attr('readonly', false);
			valid(field);
        } else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'branchName') {
    	if (value.length > 2){
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Register/check/_',
	            data: {
	            	check : value+'|'+$('#restoName').val(),
	            	type  : 'branchName',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
						invalid(field);
						$('#snackbar-error').toast('show');
	                    $('#txt-error').html('Resto dengan cabang ini sudah terdaftar');
	                }
	            }
	        });
        } else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'address') {
    	if(value.length > 5) { 
    		valid(field);
    	} else { 
    		invalid(field);
    	}
    } else if (field == 'cpEmail') {
    	var re = /\S+@\S+\.\S+/;
    	if (value.length > 0) {
	    	if (re.test(value) == true){
		        $.ajax({
		            type: 'POST',
		            dataType: 'json',
		            cache: false,
		            url: configUrl + 'Register/check/_',
		            data: {
		            	check : value,
		            	type  : 'cpEmail',
		            },
		            success: function (responses) {
		                if(responses.code == 200 && responses.total == 0){
							valid(field);
							$('#snackbar-error').toast('hide');
		                }else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Email telah terdaftar');
		                }
		            }
		        });
	        } 
    	} else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'cpMobile') {
	    var number  = value[0];

	    if (number == '0') { $('#cpMobile').val('62'+$('#cpMobile').val().substr(1));}
    	if (value.length > 9) { 
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Register/check/_',
	            data: {
	            	check : value,
	            	type  : 'cpMobile',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
						invalid(field);
						$('#snackbar-error').toast('show');
	                    $('#txt-error').html('Nomor HP telah terdaftar');
	                }
	            }
	        });
    	} else { 
    		invalid(field);
    	}
    } else if (field == 'waNo') {
	    var number  = value[0];

	    if (number == '0') { $('#waNo').val('62'+$('#waNo').val().substr(1));}
    	if (value.length > 9) { 
			valid(field);
    	} else { 
    		invalid(field);
    	}
    } else if (field == 'email') {
    	var re = /\S+@\S+\.\S+/;
    	if (value.length > 0) {
	    	if (re.test(value) == true){
		        $.ajax({
		            type: 'POST',
		            dataType: 'json',
		            cache: false,
		            url: configUrl + 'Register/check/_',
		            data: {
		            	check : value,
		            	type  : 'email',
		            },
		            success: function (responses) {
		                if(responses.code == 200 && responses.total == 0){
							valid(field);
							$('#snackbar-error').toast('hide');
		                }else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Email telah terdaftar');
		                }
		            }
		        });
	        } 
    	} else { 
    		invalid(field);
			$('#snackbar-error').toast('hide');
        }
    } else if (field == 'no_wa') {
	    var number  = value[0];

	    if (number == '0') { $('#no_wa').val('62'+$('#no_wa').val().substr(1));}
    	if (value.length > 9) { 
	        $.ajax({
	            type: 'POST',
	            dataType: 'json',
	            cache: false,
	            url: configUrl + 'Register/check/_',
	            data: {
	            	check : value,
	            	type  : 'no_wa',
	            },
	            success: function (responses) {
	                if(responses.code == 200 && responses.total == 0){
						valid(field);
						$('#snackbar-error').toast('hide');
	                }else {
						invalid(field);
						$('#snackbar-error').toast('show');
	                    $('#txt-error').html('Nomor Whatsapp telah terdaftar');
	                }
	            }
	        });
    	} else { 
    		invalid(field);
    	}
    } else {
    	if(value.length > 0) { 
    		valid(field);
    	} else { 
    		invalid(field);
    	}
    }
}

function origin(field){
    $('#is'+field).val(0);
    $('#'+field).css('border-color','rgba(0, 0, 0, 0.1)');
}

function valid(field){
    $('#is'+field).val(1);
    $('#'+field).css('border-color','rgba(0, 0, 0, 0.1)');
}

function invalid(field){
    $('#is'+field).val(0);
    $('#'+field).css('border-color','red');
}

function register(){
	$('#buttonConfirm').empty();
	var re = /\S+@\S+\.\S+/;
	var isBranch 	= 0;
	var branchName	= "";

	if ($('#email').val() == '' && $('#isemail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email harus diisi');
	} else if (re.test($('#email').val()) == false && $('#isemail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email harus diisi dengan benar');
	} else if (re.test($('#email').val()) == true && $('#isemail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email telah terdaftar');
	} else if ($('#no_wa').val() == '' && $('#isno_wa').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor whatsapp harus diisi');
	} else if ($('#no_wa').val().length <= 9 && $('#isno_wa').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor whatsapp harus diisi dengan benar');
	} else if ($('#no_wa').val() > 9 && $('#isno_wa').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor whatsapp telah digunakan');
	} else {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
	        url: configUrl + 'Register/signup/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: $('#mainform').serialize() +
	        		"&isBranchTrue=" + 0 +
	        		"&branchNameTrue=" + "",
	        beforeSend: function() {
		      $('#buttonConfirm').html(`
		        <div class="d-flex justify-content-center">
		          <div class="spinner-border color-red2-dark" role="status" style="margin-bottom:10px">
		            <span class="sr-only">Loading...</span>
		          </div>
		        </div>
		      `);
	        },
	        success: function(response) {
	            if (response.code == '200') {
					$('#snackbar-success').toast('show');
		            $('#buttonConfirm').empty();
			        setTimeout(function () {
	                	window.location.href = 'login';
			        }, 1500);
	            }else{
					$('#snackbar-error').toast('show');
			        $('#txt-error').html('Gagal, coba lagi nanti.');
	            }
	        },
	    });
	}
}