$(document).ready(function() {
});

function get_data(){
  var getData = restoType == 0 ? configUrl + 'Report/get_data/_' : configUrl + 'Report/get_data_po/_'; 
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: getData,
    data: {
      id        : restoID,
      orderDate : $('#orderDate').val(),
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#content').empty();
      $('#divButton').html('');
      if (response.status == true) {
        $('#content').append(`
          <div class="card card-style" style="margin: 10px 0;border-radius: 0;">
            <div class="content" style="margin: 10px 15px">     
              <span id="title"></span>     
              <span id="list"></span>   
            </div>
          </div>
        `);
        $.each(response.data, function( i, val ) { 
          var margintop = i == 0 ? `margin:0` : `margin:10px 0 0 0`; 
          var city      = val.branchName == '' ? ', '+val.cityName : ' - ' +val.branchName + ', ' +val.cityName;
          var orderPO   = restoType == 0 ? ` (`+ val.orderNo +`)` : '';
          
          $('#title').html(`
            <div>
              <h6 style="font-weight:600;font-size:14px;color:black;margin:0">Daftar Pesanan `+ val.restoName +``+ city +`</h6>
              <h6 style="font-weight:400;font-size:13px;margin:0">`+ dateFormat3(val.orderDate) +`</h6>
            </div>
          `);

          var address = val.guestShipment == 'delivery' ? `<h6 style="font-weight:400;font-size:13px;margin:0">Alamat : `+ val.guestAddress +`</h6>` : '';
          $('#list').append(`
            <div>
              <h6 style="font-weight:600;font-size:14px;color:black;margin:10px 0 0 0">`+ val.guestName +` / `+ val.guestWa +` / `+ val.guestShipment +``+orderPO+`</h6>
              `+ address +`
              <table style="line-height:1.3;margin-top:3px">
                <tbody id="tb_order_item`+val.id+`"></tbody>
              </table>
            </div>
          `);

          $.each(val.order_item, function( j, value ) {
            if (value.additional == 'false') {
              $('#tb_order_item'+val.id).append(`
                <tr>
                  <td>`+value.qty+` x </td>
                  <td>`+value.productName+`</td>
                </tr>
              `);
            }
          });
        });

        $('#divButton').html(`
          <a href="javascript:;" onclick="inputWA()" class="btn btn-xs btn-full mb-0 rounded-0 text-uppercase font-900 shadow-s bg-red2-dark" style="margin: 10px 15px;border-radius:10px !important">Kirim Whatsapp</a>
        `);
      } else if (response.status == false) {
        $('#content').append(`
          <div class="card card-style" style="margin: 10px 0;border-radius: 0;">
            <div class="content" style="margin: 10px 15px;text-align:center">
              Tidak ada pesanan
            </div>
          </div>
        `);
      }
    },
  });
}

function inputWA(){
  $('#sheets_wa').showMenu();
  $('#whatsapp').val('');
}

function sendWA() {
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Report/sendWA/_',
    data: {
      id        : restoID,
      orderDate : $('#orderDate').val(),
      whatsapp  : $('#whatsapp').val(),
      restoType : restoType,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    beforeSend: function() {
      $('#send').css('display', 'none');
      $('#loadSend').css('display', 'block');
    },
    success: function(response) {
      if (response.success == true) {
        $('#snackbar-success').toast('show');
        $('#sheets_wa').hideMenu();
        $('#send').css('display', 'block');
        $('#loadSend').css('display', 'none');
      }else{
        $('#snackbar-error').toast('show');
        $('#send').css('display', 'block');
        $('#loadSend').css('display', 'none');
        $('#txt-error').html('Gagal, coba lagi nanti');
      }
    },
  });
}

function requiredField(val){
  var value   = $.trim(val);
  var number  = value[0];

  if (number == '0') { $('#whatsapp').val('62'+$('#whatsapp').val().substr(1));}
}