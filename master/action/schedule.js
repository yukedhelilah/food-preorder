$(document).ready(function() {
	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Schedule/get_data/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      	$('#id').val(response.data.id);
      	$('#monHourOpen').val(response.data.monHourOpen);
      	$('#monHourClose').val(response.data.monHourClose);
    	if (response.data.monOpen == 0) {
    		$('#monOpen').prop("checked", true);
    		$('#div-monOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#monOpen').prop("checked", false);
    		$('#div-monOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#tueHourOpen').val(response.data.tueHourOpen);
      	$('#tueHourClose').val(response.data.tueHourClose);
    	if (response.data.tueOpen == 0) {
    		$('#tueOpen').prop("checked", true);
    		$('#div-tueOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#tueOpen').prop("checked", false);
    		$('#div-tueOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#wedHourOpen').val(response.data.wedHourOpen);
      	$('#wedHourClose').val(response.data.wedHourClose);
    	if (response.data.wedOpen == 0) {
    		$('#wedOpen').prop("checked", true);
    		$('#div-wedOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#wedOpen').prop("checked", false);
    		$('#div-wedOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#thuHourOpen').val(response.data.thuHourOpen);
      	$('#thuHourClose').val(response.data.thuHourClose);
    	if (response.data.thuOpen == 0) {
    		$('#thuOpen').prop("checked", true);
    		$('#div-thuOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#thuOpen').prop("checked", false);
    		$('#div-thuOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#friHourOpen').val(response.data.friHourOpen);
      	$('#friHourClose').val(response.data.friHourClose);
    	if (response.data.friOpen == 0) {
    		$('#friOpen').prop("checked", true);
    		$('#div-friOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#friOpen').prop("checked", false);
    		$('#div-friOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#satHourOpen').val(response.data.satHourOpen);
      	$('#satHourClose').val(response.data.satHourClose);
    	if (response.data.satOpen == 0) {
    		$('#satOpen').prop("checked", true);
    		$('#div-satOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#satOpen').prop("checked", false);
    		$('#div-satOpen').attr("class", "switch-is-unchecked");
    	}
      	$('#sunHourOpen').val(response.data.sunHourOpen);
      	$('#sunHourClose').val(response.data.sunHourClose);
    	if (response.data.sunOpen == 0) {
    		$('#sunOpen').prop("checked", true);
    		$('#div-sunOpen').attr("class", "switch-is-checked");
    	} else {
    		$('#sunOpen').prop("checked", false);
    		$('#div-sunOpen').attr("class", "switch-is-unchecked");
    	}
    },
  });
}

function save(){
	$.ajax({
	    type: 'POST',
	    dataType: 'JSON',
	    url: configUrl + 'Schedule/save/_',
	    data: $('#mainform').serialize(),
	    headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    success: function(response) {
        if (response.code == '200') {
  				$('#snackbar-success').toast('show');
          setTimeout(function () {
              window.location.href = 'setting';
          }, 100);
        }else{
  				$('#snackbar-error').toast('show');
	        $('#txt-error').html('Gagal, coba lagi nanti');
        }
	    },
	});
}

function schedule_custom() {
	window.location.href = 'schedule_custom';
}