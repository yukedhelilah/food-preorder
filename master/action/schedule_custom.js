$(document).ready(function() {
	get_data();
});

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Schedule/get_schedule/_',
    data: {
      id: restoID,
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
		$('#content').empty();
    	if (response.status == true) {
		  	$.each(response.data, function( i, val ) {
		  		var margintop = i == 0 ? '' : `style="margin-top:10px"`;
			  	$('#content').append(`
			  		<div class="card mb-0" `+margintop+`>
			            <div class="content" style="margin: 10px 15px">
			                <table style="width: 100%">
			                    <tr>
			                        <td>
			                            <h5 data-activate="monOpen" class="font-600">`+ dateFormat3(val.closeDate) +`</h5>
			                        </td>
			                        <td style="width: 25%;text-align: right;">
			                            <a href="javascript:;" onclick="edit_data(`+val.int+`,'`+val.closeDate+`','`+val.closeFrom+`','`+val.closeTo+`')" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
			                                <i class="fa fa-edit" style="font-size: 10px"></i>
			                            </a>
			                            <a href="javascript:;" onclick="delete_data(`+val.int+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
			                                <i class="fa fa-trash" style="font-size: 10px"></i>
			                            </a>
			                        </td>
			                    </tr>
			                </table>
			                <div>
			                    <div class="input-style input-style-2 input-required" style="width: 48%;float: left;margin: 15px 0 5px 0">
			                        <span class="active-input" style="background: linear-gradient(white, #e9ecef) !important">Jam Buka</span>
			                        <input type="time" class="form-control" value="`+ val.closeFrom +`" readonly="readonly" style="font-size:15px !important;height:40px">
			                    </div> 
			                    <div class="input-style input-style-2 input-required" style="width: 48%;float: right;margin: 15px 0 5px 0">
			                        <span class="active-input" style="background: linear-gradient(white, #e9ecef) !important">Jam Tutup</span>
			                        <input type="time" class="form-control" value="`+ val.closeTo +`" readonly="readonly" style="font-size:15px !important;height:40px">
			                    </div>
			                </div>
			            </div>   
			        </div>
			    `);
		  	});
    	} else {
		  	$('#content').append(`
		  		<div class="card mb-0">
		            <div class="content" style="margin: 10px 15px;text-align:center">
		            	Tidak ada jadwal untuk acara mendatang
		            </div>   
		        </div>
		    `);
    	}
    },
  });
}

function new_data(){
	window.location.href = '#';
	$('#act').val('add');
	$('#id').val('');
	$('#restoID').val(restoID);
	$('#closeDate').val('');
	$('#closeFrom').val('');
	$('#closeTo').val('');
	$('#sheets_schedule').showMenu();
}

function edit_data(id, closeDate, closeFrom, closeTo){
	window.location.href = '#';
	$('#act').val('edit');
	$('#id').val(id);
	$('#restoID').val(restoID);
	$('#closeDate').val(closeDate);
	$('#closeFrom').val(closeFrom);
	$('#closeTo').val(closeTo);
	$('#sheets_schedule').showMenu();
}

function save_data(){
	$.ajax({
	    type: 'POST',
	    dataType: 'JSON',
	    url: configUrl + 'Schedule/save_schedule/_',
	    data: $('#mainform').serialize(),
	    headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    success: function(response) {
			$('#sheets_schedule').hideMenu();
            if (response.code == '200') {
				$('#snackbar-success').toast('show');
				get_data();
            }else{
				$('#snackbar-error').toast('show');
		        $('#txt-error').html('Gagal, coba lagi nanti');
            }
	    },
	});
}

function delete_data(id){
	if (confirm('Anda yakin menghapus jadwal ini?')) {
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Schedule/delete_schedule/_',
		    data: {
		    	id : id,
		    },
		    headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
				$('#sheets_schedule').hideMenu();
	            if (response.status == true) {
					$('#snackbar-success').toast('show');
					get_data();
	            }else{
					$('#snackbar-error').toast('show');
			        $('#txt-error').html('Gagal, coba lagi nanti');
	            }
		    },
		});
	}
}

function schedule() {
	window.location.href = 'schedule';
}