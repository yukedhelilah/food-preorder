$(document).ready(function() {
});

function profile(){
	window.location.href = 'profile';
}

function account(){
	window.location.href = 'account';
}

function membership(){
	window.location.href = 'membership';
}

function qrcode(){
	window.location.href = 'qrcode';
}

function schedule(){
	window.location.href = 'schedule';
}

function staff(){
	window.location.href = 'staff';
}

function order_setting(){
	window.location.href = 'order_setting';
}

function order_list(){
	window.location.href = 'order_list';
}

function report(){
	window.location.href = 'report';
}

function kurir(){
	window.location.href = 'kurir';
}