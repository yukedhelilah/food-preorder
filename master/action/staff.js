$(document).ready(function() {
	get_data();
});

function get_data(){
	$('#content').empty();
	$.ajax({
	    type: 'POST',
	    dataType: 'JSON',
	    url: configUrl + 'Staff/get_data/_',
	    data: {
			id: restoID,
	    },
	    headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
	    },
	    success: function(response) {
	    	if (response.total == 1) {
				$('#content').append(`
		            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
		              <div class="content" style="margin:5px 15px">
			              <table style="width:100%">
			                <tr>
			                  <td style="text-align:center;">Belum ada Staff</td>
			                </tr>
			              </table>
		              </div>
		            </div>
				`);
	    	} else {
				$.each(response.data, function( i, val ) {
					if (val.name != 'MASTER') {
			          $('#content').append(`
			            <div class="card card-style" style="width: 100%;margin: 0;border-radius: 0;border-top:1px solid #f0f0f0 !important">
			              <div class="content" style="margin:5px 15px">
			                <table style="width:100%">
			                  <tr>
			                    <td style="padding:5px;width:35%">`+val.name+`</td>
			                    <td style="padding:5px;width:40%">`+val.no_wa+`</td>
			                    <td style="text-align:center;padding:5px;width:25%">
			                      <a href="javascript:;" onclick="edit_data(`+val.id+`,'`+val.name+`','`+val.email+`','`+val.no_wa+`')" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="width: 25px;height: 25px;padding: 2px !important;">
			                        <i class="fa fa-edit" style="font-size: 10px"></i>
			                      </a>&nbsp
			                      <a href="javascript:;" onclick="delete_data(`+val.id+`)" class="btn btn-xxs rounded-s text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="width: 25px;height: 25px;padding: 2px !important;">
			                        <i class="fa fa-trash" style="font-size: 10px"></i>
			                      </a>
			                    </td>
			                  </tr>
			                </table>
			              </div>
			            </div>
			          `);
					}
				});
	    	}
	    },
	});
}

function new_data(){
	window.location.href = '#';
	$('#act').val('add');
	$('#id').val('');
	$('#restoID').val(restoID);
	$('#name').val(''); empty('name');
	$('#email').val(''); empty('email');
	$('#emailCopy').val('');
	$('#isemail').val(0);
	$('#no_wa').val(''); empty('no_wa');
	$('#no_waCopy').val('');
	$('#isno_wa').val(0);
	// $('#password').val('Password : 123456');
	// $('#password_add').css('display','block');
	// $('#password_edit').css('display','none');
	$('#sheets_staff').showMenu();
}

function edit_data(id, name, email, wa){
	window.location.href = '#';
	$('#act').val('edit');
	$('#id').val(id);
	$('#restoID').val(restoID);
	$('#name').val(name); empty('name');
	$('#email').val(email); empty('email');
	$('#emailCopy').val(email);
	$('#isemail').val(1);
	$('#no_wa').val(wa); empty('no_wa');
	$('#no_waCopy').val(wa);
	$('#isno_wa').val(1);
	// $('#password').val(123456);
	// $('#changePassword').prop('checked',false);
	// $('#password_add').css('display','none');
	// $('#password_edit').css('display','block');
	$('#sheets_staff').showMenu();
}

function requiredField(val,field){
    var value 	= $.trim(val);

	if (field == 'email') {
        var re = /\S+@\S+\.\S+/;
        if (value.length > 0) {
            if (re.test(value) == true){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    url: configUrl + 'Register/check/_',
                    data: {
                        check : value,
                        type  : 'email',
                    },
                    success: function (responses) {
                        if(responses.code == 200 && responses.total == 0){
                            valid(field);
                            $('#snackbar-error').toast('hide');
                        }else {
		                	if ($('#act').val() == 'edit') {
			                	if (value == $('#emailCopy').val()) {
									valid(field);
									$('#snackbar-error').toast('hide');
			                	} else {
									invalid(field);
									$('#snackbar-error').toast('show');
				                    $('#txt-error').html('Email telah digunakan');
			                	}
		                	} else {
								invalid(field);
								$('#snackbar-error').toast('show');
			                    $('#txt-error').html('Email telah digunakan');
		                	}
                        }
                    }
                });
            } 
        } else { 
            invalid(field);
            $('#snackbar-error').toast('hide');
        }
    } else if (field == 'no_wa') {
        var number  = value[0];

        if (number == '0') { $('#no_wa').val('62'+$('#no_wa').val().substr(1));}
        if (value.length > 9) { 
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: configUrl + 'Register/check/_',
                data: {
                    check : value,
                    type  : 'no_wa',
                },
                success: function (responses) {
                    if(responses.code == 200 && responses.total == 0){
                        valid(field);
                        $('#snackbar-error').toast('hide');
                    }else {
	                	if ($('#act').val() == 'edit') {
		                	if (value == $('#no_waCopy').val()) {
								valid(field);
								$('#snackbar-error').toast('hide');
		                	} else {
								invalid(field);
								$('#snackbar-error').toast('show');
			                    $('#txt-error').html('Nomor Whatsapp telah digunakan');
		                	}
	                	} else {
							invalid(field);
							$('#snackbar-error').toast('show');
		                    $('#txt-error').html('Nomor Whatsapp telah digunakan');
	                	}
                    }
                }
            });
        } else { 
            invalid(field);
        }
    }
}

function valid(field) { $('#is'+field).val(1); }
function invalid(field) { $('#is'+field).val(0); }

function empty(field) { 
    $('#label'+field).removeAttr('class'); 
    if ($('#'+field).val() == '') {
        $('#label'+field).attr('class','color-highlight');
    } else {
        $('#label'+field).attr('class','color-highlight input-style-1-active');
    }
}

function save_data(){
	if ($('#isemail').val() == 0) {
		$('#snackbar-error').toast('show');
        $('#txt-error').html('Email tidak valid');
	} else if ($('#isno_wa').val() == 0) {
        $('#snackbar-error').toast('show');
        $('#txt-error').html('Nomor Whatsapp tidak valid');
    } else {
		$.ajax({
		    type: 'POST',
		    dataType: 'JSON',
		    url: configUrl + 'Staff/save/_',
		    data: $("#mainform").serialize(),
		    headers: {
		        'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(response) {
		    	if (response.code == 200) {
					get_data()
					$('#sheets_staff').hideMenu();
					$('#snackbar-success').toast('show');
		    	} else {
		    		alert('Gagal, coba lagi nanti');
					$('#sheets_staff').hideMenu();
		    	}
		    },
		});
	}
}

function delete_data(id){
	if (confirm('Anda yakin menghapus staff ini?')) {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		url: configUrl + 'Staff/delete/_',
		data: {
			id : id,
		},
		headers: {
		    'Content-Type': 'application/x-www-form-urlencoded'
		},
		success: function(response) {
			if (response.code == 200) {
				get_data()
				$('#sheets_staff').hideMenu();
				$('#snackbar-success').toast('show');
			} else {
				alert('Gagal, coba lagi nanti');
				$('#sheets_staff').hideMenu();
			}
		},
	});
	}
}

function account(){
	window.location.href = 'account';
}