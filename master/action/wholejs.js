var restoID         = atob(localStorage.getItem(btoa("resto_restoID")));
var restoType       = atob(localStorage.getItem(btoa("resto_restoType")));
var restoName       = atob(localStorage.getItem(btoa("resto_restoName")));
var staffID         = atob(localStorage.getItem(btoa("resto_staffID")));
var staffName       = atob(localStorage.getItem(btoa("resto_staffName")));
var staffWA         = atob(localStorage.getItem(btoa("resto_staffWA")));
var staffEmail      = atob(localStorage.getItem(btoa("resto_staffEmail")));
var token           = atob(localStorage.getItem(btoa("resto_token")));
var expired         = localStorage.getItem(btoa("resto_expired"));
var exp             = new Date(atob(expired));
var now             = new Date();
var configUrl       = 'http://dapur123.com/server.php/Master/';
var configUrl2      = 'http://dapur123.com/server.php/reseller/';
var configFile      = 'http://dapur123.com/files/';
// var configUrl       = 'https://creatrixorganizer.com/resto/Master/';
// var configUrl2      = 'https://creatrixorganizer.com/resto/reseller/';
// var configFile      = 'https://creatrixorganizer.com/resto/files/';

var bulan           = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
var bulans          = ["JAN", "FEB", "MAR", "APR", "MEI", "JUN", "JUL", "AGU", "SEP", "OKT", "NOV", "DES"];
var days            = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

if(restoID == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
  removeLocalstorage();
  window.location.href = '../pages/login';
}

$(document).ready(function() {});

function home(){
  window.location.href = 'home';
}

function qrcode(){
  window.location.href = 'qrcode';
}

function product(){
  window.location.href = 'product';
}

function order(){
  window.location.href = 'order_list';
}

function setting(){
  window.location.href = 'setting';
}

function dateFormat(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString();
}

function dateFormat2(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  return date + ' ' + bulans[format.getMonth()];
}

function dateFormat3(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return days[format.getDay()] + ', ' + date + ' ' + bulan[format.getMonth()] + ' ' + format.getFullYear();
}

function dateFormat4(date) {
  var format  = new Date(date);
  var date    = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  var hours   = format.getHours() < 10 ? '0'+format.getHours() : format.getHours();
  var minutes = format.getMinutes() < 10 ? '0'+format.getMinutes() : format.getMinutes(); 

  return date + ' ' + bulans[format.getMonth()] + ' ' + hours +`:`+ minutes;
}

function dateFormat5(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return days[format.getDay()] + ', ' + date + ' ' + bulan[format.getMonth()];
}

function dateFormat6(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();

  return date + ' ' + bulan[format.getMonth()] + ' ' + format.getFullYear();
}

function dateFormat7(date) {
  var format = new Date(date);
  var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
  var hours   = format.getHours() < 10 ? '0'+format.getHours() : format.getHours();
  var minutes = format.getMinutes() < 10 ? '0'+format.getMinutes() : format.getMinutes(); 

  return hours + ':' + minutes + ' ' + date + ' ' + bulans[format.getMonth()];
}

function numberFormat(nStr){
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function getUrlParameter(sParam){
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
}

function logout() {
  if (confirm("Anda yakin ingin keluar?")) {
    removeLocalstorage();
    window.location.href = '../pages/login';
  } 
}

function removeLocalstorage(){
  localStorage.removeItem(btoa('resto_restoID'));
  localStorage.removeItem(btoa('resto_restoName'));
  localStorage.removeItem(btoa('resto_staffID'));
  localStorage.removeItem(btoa('resto_staffName'));
  localStorage.removeItem(btoa('resto_staffWA'));
  localStorage.removeItem(btoa('resto_staffEmail'));
  localStorage.removeItem(btoa('resto_token'));
  localStorage.removeItem(btoa('resto_expired'));
}