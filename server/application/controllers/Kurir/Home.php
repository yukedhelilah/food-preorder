<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Kurir/Home_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data = array(
            'userID'        => $this->input->post('kurirID', true),
            'startFrom'     => $this->input->post('startFrom', true),
            'flag'          => 0,
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $data       = array(
            'flag'  => 1,
        );

        $sql        = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $sql2   = $this->mainmodul->delete_ongkir($data, $this->input->post('id'));

            if($sql2 == true){ 
                $this->response([
                    'code'      => 200,
                ], 200); 
            } else {
                $this->response([
                    'code'      => 500,
                ], 200);
            }
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

