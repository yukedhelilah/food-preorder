<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ongkir extends App_Public {

    public function __construct(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        
        $this->load->model('Kurir/Ongkir_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        // if ($this->input->post('search') == '') {
            $data = $this->mainmodul->get_data($this->input->post('id'));
        // } else {
            // $data = $this->mainmodul->search($this->input->post('id'), $this->input->post('search'));
        // }

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'userID'        => $this->input->post('kurirID', true),
            'startFrom'     => $this->input->post('startFrom', true),
            'destination'   => $this->input->post('destination', true),
            'price'         => str_replace(",","",$this->input->post('price', true)),
            'flag'          => 0,
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $sql        = $this->mainmodul->delete($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

