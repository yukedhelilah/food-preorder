<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Kurir/Profile_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function check_post(){
        $data = $this->mainmodul->check(strtolower($this->input->post('check')), $this->input->post('type'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function save_post(){
        $sql        = true;

        $data = array(
            'username'      => strtolower($this->input->post('username', true)),
            'companyName'   => $this->input->post('companyName', true),
            'phone'         => $this->input->post('phone', true),
            'address'       => $this->input->post('address', true),
            'cityID'        => $this->input->post('cityID', true),
            'contactName'   => $this->input->post('contactName', true),
            'contactWa'     => $this->input->post('contactWa', true),
            'contactEmail'  => $this->input->post('contactEmail', true),
        );

        if ($this->input->post('password') != '') {
            $data['password'] = md5(md5($this->input->post('password', true)));
        }

        $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function save_snk_post(){
        $sql        = true;

        $data = array(
            'note'          => $this->input->post('note', true),
        );

        $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function get_city_post(){
        $data = $this->mainmodul->get_city();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function change_password_post(){
        $sql        = true;

        $data = array(
            'password' => md5(md5($this->input->post('password', true))),
        );
        $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

