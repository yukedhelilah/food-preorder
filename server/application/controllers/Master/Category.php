<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Master/Category_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_by_resto_post(){
        $data = $this->mainmodul->get_data_by_resto($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $arrayUrl   = explode(" ", strtolower($this->input->post('categoryName', true)));
        $url        = implode("-", preg_replace("/[^0-9\pL]+/", "", $arrayUrl));

        $data=array(
            'restoID'       => $this->input->post('restoID', true),
            'categoryName'  => strtoupper($this->input->post('categoryName', true)),
            'shortName'     => $url,
            'img'           => $this->input->post('img1', true),
            'description'   => $this->input->post('description', true),
            'flag'          => 0,
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $data       = array(
            'flag'  => 1,
        );

        $sql        = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $sql2   = $this->mainmodul->delete_product($data, $this->input->post('id'));

            if($sql2 == true){ 
                $this->response([
                    'code'      => 200,
                    // 'total'     => count($sql),
                ], 200); 
            } else {
                $this->response([
                    'code'      => 500,
                    // 'total'     => count($sql),
                ], 200);
            }
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function get_city_post(){
        $data = $this->mainmodul->get_city();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }
}

