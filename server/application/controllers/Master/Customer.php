<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Customer_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id', true), $this->input->post('start'), $this->input->post('limit'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }
}

