<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();   
        $this->load->model('Master/Login_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function login_post(){
        $required = [];
        if(empty($this->input->post('no_wa'))) {$required[] = 'Nomor Whatsapp';}

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' harus diisi',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $data = $this->mainmodul->login($this->input->post('no_wa'));

        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $data['error'],
            ], 200);
        }else if($data == null){
            $this->response([
                'code'      => 500,
                'message'   => 'Nomor anda belum terdaftar',
            ], 200);
        }else{
            $otp            = $this->randomNumber(4);
            $input_otp      = array(
                'otpNo'     => $otp,
                'otpDate'   => date('Y-m-d H:i:s', strtotime('now')), 
            );
            $sql            = $this->mainmodul->edit_staff($input_otp, $data['staffID']);
            
            $data['otp']    = base64_encode($otp);

            if ($sql == true) {
                $phone      = $this->input->post('no_wa');
                $message    = '*VERIFIKASI LOGIN DAPUR123.COM*<br>Kode verifikasi anda : *'.$otp.'*<br>JANGAN BERIKAN KODE RAHASIA INI KEPADA SIAPAPUN';

                $this->single_message($phone, $message);
                $this->sendMail($data['staffEmail'],$otp);
                $this->response([
                    'code'      => 200,
                    'message'   => 'Success',
                    'data'      => $data,
                ], 200); 
            } else {
                $this->response([
                    'code'      => 500,
                    'message'   => 'Failed',
                    'error'     => $this->db->error(),
                ], 200); 
            }
        }
    }
    
    public function login_pin_post(){
        $required = [];
        if(empty($this->input->post('no_wa'))) {$required[] = 'Nomor Whatsapp';}
        else if(empty($this->input->post('password'))) {$required[] = 'Pin';}

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' harus diisi',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }
        
        $data = $this->mainmodul->login_pin($this->input->post('no_wa'), md5(md5($this->input->post('password'))));

        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $data['error'],
            ], 200);
        }else if($data == null){
            $this->response([
                'code'      => 500,
                'message'   => 'Nomor anda belum terdaftar',
            ], 200);
        }else{
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $data,
            ], 200); 
        }
    }

    public function sendOTP_post(){
        $otp            = $this->randomNumber(4);
        $input_otp      = array(
            'otpNo'     => $otp,
            'otpDate'   => date('Y-m-d H:i:s', strtotime('now')), 
        );
        $sql            = $this->mainmodul->edit_staff($input_otp, $this->input->post('id'));

        if ($sql == true) {
            $phone      = $this->input->post('wa');
            $message    = '*VERIFIKASI LOGIN DAPUR123.COM*<br>Kode verifikasi anda : *'.$otp.'*<br>JANGAN BERIKAN KODE RAHASIA INI KEPADA SIAPAPUN';

            $this->single_message($phone, $message);
            $this->sendMail($this->input->post('email'),$otp);
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'otp'       => base64_encode($otp),
            ], 200); 
        } else {
            $this->response([
                'code'      => 500,
                'message'   => 'Failed',
                'error'     => $this->db->error(),
            ], 200); 
        }
    }

    public function activate_post(){
        $sql  = true;

        $data = array(
            'flag'          => 0,
            'expiredDate'   => date('Y-m-d H:i:s', strtotime('+14 day')),
        );
        
        $sql  = $this->mainmodul->activate($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    function single_message($phone, $message){
        $token = "8b171281ea0e8150574f5c0dba8c502e5f60486b11059";
        $uid = "6281395797797";
        $data = [
            'custom_uid' => 'msg_test-'.time(),
            'phone' => $phone,
            'message' => str_replace("<br>", "\n", $message),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/chat");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$uid."&to=".$data['phone']."&custom_uid=".$data['custom_uid']."&text=".$data['message']."");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);

        $response = json_decode(curl_exec($ch),true);
        $info = curl_getinfo($ch);
        curl_close ($ch);

        // echo json_encode($response);
        return [
            'response' => $response,
            'info' => $info,
        ];
    }

    function sendMail($email, $otp){
        $to = $email;
        $subject = "DAPUR123.COM - VERIFIKASI LOGIN";

        $message = "
        <html>
        <head>
            <title>Verifikasi Login - ".date('d-m-Y H:i:s')."</title>
        </head>
        <body>
            <b>VERIFIKASI LOGIN DAPUR123.COM</b><br>
            Kode verifikasi anda : <b>".$otp."<b><br>
            JANGAN BERIKAN KODE RAHASIA INI KEPADA SIAPAPUN
        </body>
        </html>
        ";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // $headers .= 'From: <noreply@gochinatours.com>' . "\r\n";
        $headers .= 'From: itcreatrix@gmail.com' . "\r\n";

        $send = mail($to,$subject,$message,$headers);
        if (!$send) {
            $errorMessage = error_get_last()['message'];
            print_r(error_get_last());
        }
    }
}

