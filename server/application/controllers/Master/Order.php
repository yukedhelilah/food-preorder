<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Order_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id', true), $this->input->post('start'), $this->input->post('limit'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_shipment_post(){
        $data    = $this->mainmodul->get_shipment($this->input->post('id', true));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_data_filter_post(){
        $data    = $this->mainmodul->get_data_filter($this->input->post('id', true), $this->input->post('shipmentType'), $this->input->post('orderFrom'), $this->input->post('orderTo'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function delete_post(){
        $sql    = $this->mainmodul->delete($this->input->post('id'));
        $sql2   = $this->mainmodul->delete_item($this->input->post('id'));

        if ($sql2 == true) {
            $this->response([
                'status'      => $sql2,
            ], 200);
        } else {
            $this->response([
                'status'      => $sql2,
            ], 200);
        }
    }

    public function confirm_post(){
        $sql            = true;

        $data=array(
            'orderNo'   => $this->input->post('orderNo'),
            'restoNote' => $this->input->post('restoNote'),
            'isStatus'  => $this->input->post('isStatus'),
        );
        $sql            = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            if($this->input->post('isStatus') == 'Paid'){
                $this->whatsapp_message('', $this->input->post('orderNo'), 'replay_from_resto_paid');
            }else if($this->input->post('isStatus') == 'Unpaid'){
                $this->whatsapp_message('', $this->input->post('orderNo'), 'replay_from_resto_unpaid');     
            }
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    function whatsapp_message($orderID, $notaNo, $tipe){
        if(!empty($orderID)){
            $data_order     = $this->mainmodul->detilOrder_id($orderID);
        }

        if(!empty($notaNo)){
            $data_order     = $this->mainmodul->detilOrder_code($notaNo);
        }

        $data_resto         = $this->mainmodul->detilResto_url($data_order['url']);

        if($tipe == 'replay_from_resto_paid'){

            $message = '_PESAN INI UNTUK PEMBELI DI DAPUR123_<br><br>';
            $message .= 'Hai '.$data_order['guestName'].', terima kasih telah berbelanja di DAPUR123.<br><br>';
            $message .= 'Pembayaran Anda telah Kami terima.<br>';
            $message .= 'Pesanan Anda dengan nomor *#'.$data_order['orderNo'].'* sedang kami proses, harap menunggu.<br><br>';
            $message .= 'Catatan dari restoran :<br>'.$this->input->post('restoNote').'<br><br>';
            $message .= 'Pesan otomatis dari sistem, jangan balas pesan ini.';

            $whatsapp = $data_order['guestWa'];

        }

        if($tipe == 'replay_from_resto_unpaid'){
            $message = '_PESAN INI UNTUK PEMBELI DI DAPUR123_<br><br>';
            $message .= 'Hai '.$data_order['guestName'].', terima kasih telah berbelanja di DAPUR123.<br><br>';
            $message .= 'Pembayaran Anda belum Kami terima.<br>';
            $message .= 'Lakukan pembayaran sebelum '.date('d-m-Y H:i', strtotime($data_order['limitDate'])).'<br><br>';
            $message .= '- RINCIAN PEMBAYARAN -<br>';
            $message .= $data_order['paymentName'].'<br>';
            $message .= 'Nama rekening: '.$data_order['accountNo'].'<br>';
            $message .= 'Nomor rekening: '.$data_order['accountName'].'<br>';
            $message .= 'Total pesanan: '.number_format($data_order['total']).'<br><br>';
            $message .= 'Klik link berikut apabila anda telah melakukan pembayaran<br>';
            $message .= 'https://dapur123.com/'.$data_resto['url'].'/confirmation/'.$data_order['orderNo'].'<br><br>';
            $message .= 'Abaikan pesan ini apabila anda telah melakukan pembayaran atau DINE IN<br>';
            $message .= 'Pesan otomatis dari sistem, jangan balas pesan ini.';

            $whatsapp = $data_order['guestWa'];

        }

        $this->single_message($whatsapp, $message);
    }

    public function single_message($phone, $message)
    {
        $curl = curl_init();
        $token = "DmytAaVfxfj8ziCsyp5CzKpiQSB39sLuUt577ql7vDM0wyvcpQfYt6C4GXSX3E4R";
        $data = [
            'phone' => $phone,
            'message' => str_replace("<br>", "\n", $message),
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL, "https://console.wablas.com/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
}

