<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_setting extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Order_setting_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id', true));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function uploadImg_post(){
        $uploadpath = 'files/qris/';
        $status = "";
        $msg    = "";
        $file_element_name = 'file_img';

        if ($status != "error")
        {
            $config['upload_path']      = $uploadpath;
            $config['allowed_types']    = 'gif|jpg|jpeg|png';
            $config['max_size']         = 1024 * 8;

            $new_name = 'product_'.time().'_'.$_POST['no'];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
      
            //$this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }

    public function save_post(){
        $acceptOrder    = !empty($this->input->post('acceptOrder')) ? 0 : 1;
        $isDelivery     = !empty($this->input->post('isDelivery')) ? 0 : 1;
        $isPickup       = !empty($this->input->post('isPickup')) ? 0 : 1;
        $isDineIn       = !empty($this->input->post('isDineIn')) ? 0 : 1;

        // echo $acceptOrder.'<br>'; 
        // echo $isDelivery.'<br>'; 
        // echo $isPickup.'<br>'; 
        // echo $isDineIn; exit();

        $data    = array(
            'acceptOrder'   => $acceptOrder,
            'waNo'          => $this->input->post('waNo'),
            'isDelivery'    => $isDelivery,
            'isPickup'      => $isPickup,
            'optional_eta'  => $this->input->post('optional_eta'),
            'isDineIn'      => $isDineIn,
            'optional_desc' => $this->input->post('optional_desc'),
        );
        $sql                = $this->mainmodul->edit($data, $this->input->post('restoID', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function get_delivery_post(){
        $data    = $this->mainmodul->get_delivery($this->input->post('id', true));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_delivery_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'restoID'       => $this->input->post('restoID', true),
            'area'          => $this->input->post('area', true),
            'rate'          => str_replace(",","", $this->input->post('rate', true)),
            'description'   => $this->input->post('description', true),
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add_delivery($data);
        }else{
            $sql                    = $this->mainmodul->edit_delivery($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_delivery_post(){
        $sql        = true;

        $sql        = $this->mainmodul->delete_delivery($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200); 
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function get_payment_post(){
        $data    = $this->mainmodul->get_payment();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_method_post(){
        $data    = $this->mainmodul->get_method($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_method_detail_post(){
        $data    = $this->mainmodul->get_method_detail($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_method_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $methodDelivery     = !empty($this->input->post('methodDelivery')) ? 0 : 1;
        $methodPickup       = !empty($this->input->post('methodPickup')) ? 0 : 1;
        $methodDineIn       = !empty($this->input->post('methodDineIn')) ? 0 : 1;

        $data=array(
            'restoID'           => $this->input->post('restoID', true),
            'paymentID'         => $this->input->post('paymentID', true),
            'methodDelivery'    => $methodDelivery,
            'methodPickup'      => $methodPickup,
            'methodDineIn'      => $methodDineIn,
            'accountName'       => $this->input->post('accountName', true),
            'accountNo'         => $this->input->post('accountNo', true),
            'qrcode'            => $this->input->post('img1'),
            'note'              => $this->input->post('note', true),
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add_method($data);
        }else{
            $sql                    = $this->mainmodul->edit_method($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_method_post(){
        $sql        = true;

        $sql        = $this->mainmodul->delete_method($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200); 
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }
}

