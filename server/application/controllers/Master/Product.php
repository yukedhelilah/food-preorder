<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends App_Public {

    public function __construct(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        
        $this->load->model('Master/Product_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        if ($this->input->post('search') == '') {
            $data = $this->mainmodul->get_data($this->input->post('id'));
        } else {
            $data = $this->mainmodul->search($this->input->post('id'), $this->input->post('search'));
        }

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_by_resto_post(){
        if ($this->input->post('search') == '') {
            $data = $this->mainmodul->get_data_by_resto($this->input->post('id'));
        } else {
            $data = $this->mainmodul->search_by_resto($this->input->post('id'), $this->input->post('search'));
        }

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function uploadImg_post(){
        $uploadpath = 'files/product/';
        $status = "";
        $msg    = "";
        $file_element_name = 'file_img';

        if ($status != "error")
        {
            $config['upload_path']      = $uploadpath;
            $config['allowed_types']    = 'gif|jpg|jpeg|png';
            $config['max_size']         = 1024 * 8;

            $new_name = 'product_'.time().'_'.$_POST['no'];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
      
            //$this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'restoID'           => $this->input->post('restoID', true),
            'categoryID'        => $this->input->post('categoryID', true),
            'productName'       => $this->input->post('productName', true),
            'badgeID'           => 0,
            'img'               => $this->input->post('img1', true),
            'description'       => $this->input->post('description', true),
            'price'             => str_replace(",","",$this->input->post('price', true)),
            'createdDate'       => date('Y-m-d H:i:s', strtotime('now')),
            'createdBy'         => $this->input->post('staffID', true),
            'flag'              => 0,
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function flag_product_post(){
        $sql        = true;

        $data       = array(
            'flag'  => $this->input->post('flag'),
        );

        $sql        = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function available_product_post(){
        $sql  = true;

        $data = array(
            'available' => $this->input->post('available'),
        );

        $sql  = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $sql        = $this->mainmodul->delete($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function get_data_po_post(){
        if ($this->input->post('search') == '') {
            $data = $this->mainmodul->get_data_po($this->input->post('id'));
        } else {
            $data = $this->mainmodul->search_po($this->input->post('id'), $this->input->post('search'));
        }

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_data_by_resto_po_post(){
        if ($this->input->post('search') == '') {
            $data = $this->mainmodul->get_data_by_resto_po($this->input->post('id'));
        } else {
            $data = $this->mainmodul->search_by_resto_po($this->input->post('id'), $this->input->post('search'));
        }

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_po_post(){
        $details    = $this->mainmodul->get_details($this->input->post('id'));
        $data       = $this->mainmodul->get_po($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'details'   => $details,
            'data'      => $data,
        ], 200);
    }

    public function get_po_detail_post(){
        $data       = $this->mainmodul->get_po_detail($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function save_po_post(){
        $sql    = true;
        $act    = $this->input->post('act', true);

        if($act == 'add'){
            $from       = $this->input->post('yearFrom').'-'.$this->input->post('monthFrom').'-'.$this->input->post('dateFrom');
            $poDateFrom = date('Y-m-d', strtotime($from)); 
            $to         = $this->input->post('yearTo').'-'.$this->input->post('monthTo').'-'.$this->input->post('dateTo');
            $poDateTo   = date('Y-m-d', strtotime($to)); 
            $poDate     = $this->getDatesFromRange($from, $to);

            foreach ($poDate as $key => $value) {
                $check = $this->mainmodul->checkPO($this->input->post('productID', true), $value);
                if ($check == 0) {
                    $data   = array(
                        'productID'     => $this->input->post('productID', true),
                        'poDate'        => date('Y-m-d', strtotime($value)),
                        'allotment'     => $this->input->post('allotment', true),
                        'flag'          => 0,
                    );
                    $sql    = $this->mainmodul->add_po($data);
                }
            } 
        }else{
            $data=array(
                'productID'     => $this->input->post('productID', true),
                'poDate'        => date('Y-m-d', strtotime($this->input->post('poDate', true))),
                'allotment'     => $this->input->post('allotment', true),
                'flag'          => 0,
            );
            $sql                = $this->mainmodul->edit_po($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'sql'       => $this->db->last_query(),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function delete_po_post(){
        $sql        = true;

        $sql        = $this->mainmodul->delete_po($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_selected_po_post(){
        $sql        = true;
        
        $checkedPO  = $this->input->post('checkedPO');
        foreach ($checkedPO as $value){ 
            $sql        = $this->mainmodul->delete_po($value);
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function save_bulk_po_post(){
        $product = $this->mainmodul->get_product_by_category($this->input->post('categoryID'));

        $from       = $this->input->post('yearFrom').'-'.$this->input->post('monthFrom').'-'.$this->input->post('dateFrom');
        $poDateFrom = date('Y-m-d', strtotime($from)); 
        $to         = $this->input->post('yearTo').'-'.$this->input->post('monthTo').'-'.$this->input->post('dateTo');
        $poDateTo   = date('Y-m-d', strtotime($to)); 
        $poDate     = $this->getDatesFromRange($from, $to);

        if ($product != 0) {
            foreach ($product as $keys => $values) {
                foreach ($poDate as $key => $value) {
                    $check = $this->mainmodul->checkPO($values->id, $value);

                    if ($check == 0) {
                        $data   = array(
                            'productID'     => $values->id,
                            'poDate'        => date('Y-m-d', strtotime($value)),
                            'allotment'     => $this->input->post('allotment', true),
                            'flag'          => 0,
                        );
                        $sql    = $this->mainmodul->add_po($data);
                    }
                }
            }

            if($sql == true){ 
                $this->response([
                    'code'      => 200,
                ], 200);
            } else {  
                $this->response([
                    'code'      => 500,
                ], 200);
            }
        } else {
            $this->response([
                'code'      => 400,
            ], 200);
        }
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }

        return $array;
    }
}

