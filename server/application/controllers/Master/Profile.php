<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Profile_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function uploadImg_post(){
        $uploadpath = 'files/logo/';
        $status = "";
        $msg    = "";
        $file_element_name = 'file_img';

        if ($status != "error")
        {
            $config['upload_path']      = $uploadpath;
            $config['allowed_types']    = 'gif|jpg|jpeg|png';
            $config['max_size']         = 1024 * 8;

            $new_name = 'product_'.time().'_'.$_POST['no'];
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
      
            //$this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $isAddress = 0;
        if(!empty($this->input->post('isAddress'))){
            $isAddress = 0;
        } else {
            $isAddress = 1;
        }

        $isBranch = 0;
        if(!empty($this->input->post('isBranch'))){
            $isBranch = 1;
        } else {
            $isBranch = 0;
        }
        $data=array(
            'restoName'     => $this->input->post('restoName', true),
            'username'      => strtolower($this->input->post('restoID', true)),
            'logo'          => $this->input->post('img1', true),
            'companyName'   => $this->input->post('companyName', true),
            'cityID'        => $this->input->post('cityID', true),
            'address'       => $this->input->post('address', true),
            'isAddress'     => $isAddress,
            'email'         => $this->input->post('email', true),
            'phone'         => $this->input->post('phone', true),
            'cpName'        => $this->input->post('cpName', true),
            'cpEmail'       => $this->input->post('cpEmail', true),
            'cpMobile'      => $this->input->post('cpMobile', true),
            'isBranch'      => $isBranch,
            'branchName'    => $this->input->post('branchName', true),
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add($data);
        }else{
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

