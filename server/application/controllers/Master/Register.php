<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends App_Public {

    public function __construct(){
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        
        $this->load->model('Master/Register_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function check_post(){
        $data = $this->mainmodul->check(strtolower($this->input->post('check')), $this->input->post('type'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function signup_post(){
        $arrayResto   	= explode(" ", strtolower($this->input->post('restoName', true)));
        $resto        	= implode("-", preg_replace("/[^0-9\pL]+/", "", $arrayResto));
    	$arrayBranch   	= explode(" ", strtolower($this->input->post('branchNameTrue', true)));
    	$branchName		= implode("-", preg_replace("/[^0-9\pL]+/", "", $arrayBranch));
    	$city 			= $this->mainmodul->getCity($this->input->post('cityID', true));

        $url 			=  $this->input->post('isBranchTrue', true) == 1 ? $resto.'-'.$branchName.'-'.$city : $resto.'-'.$city;

        $resto       	= array(
            'url'               => $url,
            'restoName'         => $this->input->post('restoName', true),
            'description'       => $this->input->post('description', true),
            'isBranch'          => $this->input->post('isBranchTrue', true),
            'branchName'        => $this->input->post('branchNameTrue', true),
            'isRegular'         => $this->input->post('isRegular', true),
            'cutoffday'         => $this->input->post('cutoffday', true),
            'cityID'            => $this->input->post('cityID', true),
            'address'           => $this->input->post('address', true),
            'cpName'            => $this->input->post('cpName', true),
            'cpEmail'           => $this->input->post('cpEmail', true),
            'cpMobile'          => $this->input->post('cpMobile', true),
            'registeredDate'    => date('Y-m-d H:i:s', strtotime('now')),
            'expiredDate'       => date('Y-m-d H:i:s', strtotime('+30 day')),
            'flag'              => 0,
        );
        $sql_resto		= $this->mainmodul->add_resto($resto);

        if($sql_resto == true){
            $monOpen = !empty($this->input->post('monOpen')) ? 0 : 1;  
            $tueOpen = !empty($this->input->post('tueOpen')) ? 0 : 1;
            $wedOpen = !empty($this->input->post('wedOpen')) ? 0 : 1;
            $thuOpen = !empty($this->input->post('thuOpen')) ? 0 : 1;
            $friOpen = !empty($this->input->post('friOpen')) ? 0 : 1;
            $satOpen = !empty($this->input->post('satOpen')) ? 0 : 1;
            $sunOpen = !empty($this->input->post('sunOpen')) ? 0 : 1;

            $operational        = array(
                'restoID'       => $sql_resto,
                'monOpen'       => $monOpen,
                'monHourOpen'   => date('H:i:s', strtotime($this->input->post('monHourOpen'))),
                'monHourClose'  => date('H:i:s', strtotime($this->input->post('monHourClose'))),
                'tueOpen'       => $tueOpen,
                'tueHourOpen'   => date('H:i:s', strtotime($this->input->post('tueHourOpen'))),
                'tueHourClose'  => date('H:i:s', strtotime($this->input->post('tueHourClose'))),
                'wedOpen'       => $wedOpen,
                'wedHourOpen'   => date('H:i:s', strtotime($this->input->post('wedHourOpen'))),
                'wedHourClose'  => date('H:i:s', strtotime($this->input->post('wedHourClose'))),
                'thuOpen'       => $thuOpen,
                'thuHourOpen'   => date('H:i:s', strtotime($this->input->post('thuHourOpen'))),
                'thuHourClose'  => date('H:i:s', strtotime($this->input->post('thuHourClose'))),
                'friOpen'       => $friOpen,
                'friHourOpen'   => date('H:i:s', strtotime($this->input->post('friHourOpen'))),
                'friHourClose'  => date('H:i:s', strtotime($this->input->post('friHourClose'))),
                'satOpen'       => $satOpen,
                'satHourOpen'   => date('H:i:s', strtotime($this->input->post('satHourOpen'))),
                'satHourClose'  => date('H:i:s', strtotime($this->input->post('satHourClose'))),
                'sunOpen'       => $sunOpen,
                'sunHourOpen'   => date('H:i:s', strtotime($this->input->post('sunHourOpen'))),
                'sunHourClose'  => date('H:i:s', strtotime($this->input->post('sunHourClose'))),
            );
            $sql_operational    = $this->mainmodul->add_operational($operational);

            $isDelivery = !empty($this->input->post('isDelivery')) ? 0 : 1;
            $isPickup   = !empty($this->input->post('isPickup')) ? 0 : 1;
            $isDineIn   = !empty($this->input->post('isDineIn')) ? 0 : 1;

            $shipment = array(
                'restoID'       => $sql_resto,
                'acceptOrder'   => 0,
                'waNo'          => $this->input->post('waNo', true),
                'isDelivery'    => $isDelivery,
                'isPickup'      => $isPickup,
                'optional_eta'  => $this->input->post('optional_eta'),
                'isDineIn'      => $isDineIn,
                'optional_desc' => $this->input->post('optional_desc'),
            );
            $sql_shipment       = $this->mainmodul->add_shipment($shipment);

            $staff = array(
                'restoID'   => $sql_resto,
                'name'      => 'MASTER',
                'no_wa'     => $this->input->post('no_wa', true),
                'email'     => $this->input->post('email', true),
                'password'  => md5(md5(1234)),
                'flag'      => 0,
            );
            $sql_staff      = $this->mainmodul->add_staff($staff);

            if($sql_operational == true && $sql_shipment == true && $sql_staff == true){
                $sendWA     = $this->single_message($resto['restoName'],$staff['no_wa']);
                $sendEmail  = $this->sendMail($resto['restoName'],$staff['email'],$staff['no_wa']);

                $this->response([
                    'code'      => 200,
                ], 200);
            } else {  
                $this->response([
                    'code'      => 500,
                ], 200);
            }
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    function sendMail($restoName, $email, $wa){
        $to = $email;
        $subject = "DAPUR123.COM - REGISTRASI AKUN";

        $message = "
        <html>
        <head>
            <title>Registrasi Akun - ".date('d-m-Y H:i:s')."</title>
        </head>
        <body>
            <b>REGISTRASI AKUN DAPUR123.COM</b><br><br>
            Hai, ".$restoName.",<br>
            Selamat anda telah berhasil registrasi di Dapur123.com<br><br>
            Untuk masuk menggunakan kode otp, gunakan nomor whatsapp yang telah didaftarkan : <b>".$wa."<b><br>
            Untuk masuk menggunakan pin, gunakan nomor whatsapp yang telah didaftarkan : <b>".$wa."<b> dengan pin default : <b>1234<b><br><br>
            Terimakasih.
        </body>
        </html>
        ";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // $headers .= 'From: <noreply@gochinatours.com>' . "\r\n";
        $headers .= 'From: itcreatrix@gmail.com' . "\r\n";

        $send = mail($to,$subject,$message,$headers);
        if (!$send) {
            $errorMessage = error_get_last()['message'];
            print_r(error_get_last());
        }
    }

    function single_message($restoName, $phone){
        $message = "*REGISTRASI AKUN DAPUR123.COM*<br><br>Hai, ".$restoName.",<br>Selamat anda telah berhasil registrasi di Dapur123.com<br><br>Untuk masuk, gunakan nomor whatsapp yang telah didaftarkan : *".$phone."*<br>Untuk masuk menggunakan pin, gunakan nomor whatsapp yang telah didaftarkan : *".$phone."* dengan pin default : *1234*<br><br>Terimakasih.";

        $token = "8b171281ea0e8150574f5c0dba8c502e5f60486b11059";
        $uid = "6281395797797";
        $data = [
            'custom_uid' => 'msg_test-'.time(),
            'phone' => $phone,
            'message' => str_replace("<br>", "\n", $message),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/chat");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$uid."&to=".$data['phone']."&custom_uid=".$data['custom_uid']."&text=".$data['message']."");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);

        $response = json_decode(curl_exec($ch),true);
        $info = curl_getinfo($ch);
        curl_close ($ch);

        // echo json_encode($response);
        return [
            'response' => $response,
            'info' => $info,
        ];
    }
}