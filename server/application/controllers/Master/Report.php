<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Report_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id', true), $this->input->post('orderDate'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function get_data_po_post(){
        $data    = $this->mainmodul->get_data_po($this->input->post('id', true), $this->input->post('orderDate'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'data'      => $data,
        ], 200);
    }

    public function sendWA_post(){
        if ($this->input->post('restoType') == 0) { 
            $data   = $this->mainmodul->get_data($this->input->post('id', true), $this->input->post('orderDate'));

            $resto  = $data[0]['branchName'] == '' ? $data[0]['restoName'].', '.$data[0]['cityName'] : $data[0]['restoName'].' - '.$data[0]['branchName'].', '.$data[0]['cityName']; 

            $message = '*Daftar Pesanan '.$resto.'*<br>';
            $message .= $this->FormatDate($this->input->post('orderDate'));

            foreach ($data as $key => $value) {
                $customer = $value['guestShipment'] == 'delivery' ? '<br><br>*'.$value['guestName'].' / '.$value['guestWa'].' / '.$value['guestShipment'].' ('.$value['orderNo'].')*<br>Alamat : '.$value['guestAddress'] : '<br><br>*'.$value['guestName'].' / '.$value['guestWa'].' / '.$value['guestShipment'].' ('.$value['orderNo'].')*';

                $message .= $customer;

                foreach ($value['order_item'] as $keys => $values) {
                    if ($values->additional == 'false') {
                        $message .= '<br>'.$values->qty.'x '.$values->productName;
                    }
                }
            }
        } else {
            $data   = $this->mainmodul->get_data_po($this->input->post('id', true), $this->input->post('orderDate'));
        
            $resto  = $data[0]['branchName'] == '' ? $data[0]['restoName'].', '.$data[0]['cityName'] : $data[0]['restoName'].' - '.$data[0]['branchName'].', '.$data[0]['cityName']; 

            $message = '*Daftar Pesanan '.$resto.'*<br>';
            $message .= $this->FormatDate($this->input->post('orderDate'));

            foreach ($data as $key => $value) {
                $customer = $value['guestShipment'] == 'delivery' ? '<br><br>*'.$value['guestName'].' / '.$value['guestWa'].' / '.$value['guestShipment'].'*<br>Alamat : '.$value['guestAddress'] : '<br><br>*'.$value['guestName'].' / '.$value['guestWa'].' / '.$value['guestShipment'].'*';

                $message .= $customer;

                foreach ($value['order_item'] as $keys => $values) {
                    if ($values->additional == 'false') {
                        $message .= '<br>'.$values->qty.'x '.$values->productName;
                    }
                }
            }
        }


        $whatsapp = $this->input->post('whatsapp');

        $send = $this->single_message($whatsapp, $message);
        print_r($send);
    }

    // public function single_message($phone, $message){
    //     $curl = curl_init();
    //     $token = "DmytAaVfxfj8ziCsyp5CzKpiQSB39sLuUt577ql7vDM0wyvcpQfYt6C4GXSX3E4R";
    //     $data = [
    //         'phone' => $phone,
    //         'message' => str_replace("<br>", "\n", $message),
    //     ];

    //     curl_setopt($curl, CURLOPT_HTTPHEADER,
    //         array(
    //             "Authorization: $token",
    //         )
    //     );
    //     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    //     curl_setopt($curl, CURLOPT_URL, "https://console.wablas.com/api/send-message");
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    //     $result = curl_exec($curl);
    //     curl_close($curl);
    //     return $result;
    // }

    function single_message($phone, $message){
        $token = "8b171281ea0e8150574f5c0dba8c502e5f60486b11059";
        $uid = "6281395797797";
        $data = [
            'custom_uid' => 'msg_test-'.time(),
            'phone' => $phone,
            'message' => str_replace("<br>", "\n", $message),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/chat");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$uid."&to=".$data['phone']."&custom_uid=".$data['custom_uid']."&text=".$data['message']."");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);

        //$response = curl_exec($ch);
        $response = json_decode(curl_exec($ch),true);
        $info = curl_getinfo($ch);
        curl_close ($ch);

        echo json_encode($response);
        /*return [
            'response' => $response,
            'info' => $info,
        ];*/

    }

    function FormatDate($waktu){
        $hari_array = array(
            'Minggu',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu'
        );
        $hr = date('w', strtotime($waktu));
        $hari = $hari_array[$hr];
        $tanggal = date('j', strtotime($waktu));
        $bulan_array = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
        $bl = date('n', strtotime($waktu));
        $bulan = $bulan_array[$bl];
        $tahun = date('Y', strtotime($waktu));
        $jam = date( 'H:i', strtotime($waktu));

        return "$hari, $tanggal $bulan $tahun";
    }
}

