<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Schedule_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id', true));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sunOpen = !empty($this->input->post('sunOpen')) ? 0 : 1;
        $monOpen = !empty($this->input->post('monOpen')) ? 0 : 1;
        $tueOpen = !empty($this->input->post('tueOpen')) ? 0 : 1;
        $wedOpen = !empty($this->input->post('wedOpen')) ? 0 : 1;
        $thuOpen = !empty($this->input->post('thuOpen')) ? 0 : 1;
        $friOpen = !empty($this->input->post('friOpen')) ? 0 : 1;
        $satOpen = !empty($this->input->post('satOpen')) ? 0 : 1;

        // echo $monOpen;exit();

        $data    = array(
            'restoID'       => $this->input->post('id'),
            'sunOpen'       => $sunOpen,
            'sunHourOpen'   => date("H:i:s", strtotime($this->input->post('sunHourOpen'))),
            'sunHourClose'  => date("H:i:s", strtotime($this->input->post('sunHourClose'))),
            'monOpen'       => $monOpen,
            'monHourOpen'   => date("H:i:s", strtotime($this->input->post('monHourOpen'))),
            'monHourClose'  => date("H:i:s", strtotime($this->input->post('monHourClose'))),
            'tueOpen'       => $tueOpen,
            'tueHourOpen'   => date("H:i:s", strtotime($this->input->post('tueHourOpen'))),
            'tueHourClose'  => date("H:i:s", strtotime($this->input->post('tueHourClose'))),
            'wedOpen'       => $wedOpen,
            'wedHourOpen'   => date("H:i:s", strtotime($this->input->post('wedHourOpen'))),
            'wedHourClose'  => date("H:i:s", strtotime($this->input->post('wedHourClose'))),
            'thuOpen'       => $thuOpen,
            'thuHourOpen'   => date("H:i:s", strtotime($this->input->post('thuHourOpen'))),
            'thuHourClose'  => date("H:i:s", strtotime($this->input->post('thuHourClose'))),
            'friOpen'       => $friOpen,
            'friHourOpen'   => date("H:i:s", strtotime($this->input->post('friHourOpen'))),
            'friHourClose'  => date("H:i:s", strtotime($this->input->post('friHourClose'))),
            'satOpen'       => $satOpen,
            'satHourOpen'   => date("H:i:s", strtotime($this->input->post('satHourOpen'))),
            'satHourClose'  => date("H:i:s", strtotime($this->input->post('satHourClose'))),
        );
        $sql                = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function get_schedule_post(){
        $data = $this->mainmodul->get_schedule($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_schedule_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'restoID'       => $this->input->post('restoID', true),
            'closeDate'     => date('Y-m-d', strtotime($this->input->post('closeDate', true))),
            'closeFrom'     => date('H:i:s', strtotime($this->input->post('closeFrom', true))),
            'closeTo'       => date('H:i:s', strtotime($this->input->post('closeTo', true))),
        );

        if($act == 'add'){
            $sql                    = $this->mainmodul->add_schedule($data);
        }else{
            $sql                    = $this->mainmodul->edit_schedule($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_schedule_post(){
        $data = $this->mainmodul->delete_schedule($this->input->post('id'));

        if ($data == true) {
            $this->response([
                'status'      => $data,
            ], 200);
        } else {
            $this->response([
                'status'      => $data,
            ], 200);
        }
    }
}

