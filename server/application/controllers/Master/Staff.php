<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Master/Staff_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function check_post(){
        $data = $this->mainmodul->check(strtolower($this->input->post('check')), $this->input->post('restoID'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'name'      => strtoupper($this->input->post('name', true)),
            'email'     => $this->input->post('email', true),
            'no_wa'     => $this->input->post('no_wa', true),
        );

        if($act == 'add'){
            $data['restoID']    = $this->input->post('restoID');
            $data['password']   = md5(md5(1234));
            $sql                = $this->mainmodul->add($data);
        }else{
            if ($this->input->post('password') != '') {
                $data['password']   = md5(md5($this->input->post('password')));
            }
            $sql                    = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql        = true;

        $data=array(
            'flag'      => 1,
        );
        $sql    = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                // 'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                // 'total'     => count($sql),
            ], 200);
        }
    }

    public function change_password_post(){
        $sql    = true;

        $data   = array(
            'password' => md5(md5($this->input->post('password'))),
        );
        $sql    = $this->mainmodul->edit($data, $this->input->post('id', true));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

