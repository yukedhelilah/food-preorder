<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{   

    function login($contactWa){
        $this->db->select('a.*');
        $this->db->from('user_jasakirim a');
        $this->db->where('a.contactWa', $contactWa);
        $this->db->where('a.flag', 0);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey   = $row['username'] . '|' . date('Y-m-d H:i:s');
            $password   = $row['password'] == md5(md5(1234)) ? 'default' : 'updated';
            return [
                'id'            => $row['id'],
                'username'      => $row['username'],
                'companyName'   => $row['companyName'],
                'contactWa'     => $row['contactWa'],
                'contactEmail'  => $row['contactEmail'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function login_pin($contactWa, $password){
        $this->db->select('a.*');
        $this->db->from('user_jasakirim a');
        $this->db->where('a.contactWa', $contactWa);
        $this->db->where('a.password', $password);
        $this->db->where('a.flag', 0);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey   = $row['username'] . '|' . date('Y-m-d H:i:s');
            $password   = $row['password'] == md5(md5(1234)) ? 'default' : 'updated';
            return [
                'id'            => $row['id'],
                'username'      => $row['username'],
                'companyName'   => $row['companyName'],
                'contactWa'     => $row['contactWa'],
                'contactEmail'  => $row['contactEmail'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function edit_staff($data, $id){
        $this->db->where('id', $id);
        $this->db->update('user_jasakirim', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}