<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Ongkir_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id){
        $this->db->select('*');
        $this->db->from('ongkos_kirim');
        $this->db->where('flag', 0);
        $this->db->where('userID', $id);
        $this->db->order_by('startFrom', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function search($id, $search){
        $this->db->select('*');
        $this->db->from('ongkos_kirim');
        $this->db->where('flag', 0);
        $this->db->where('categoryID', $id);
        $this->db->where('destination LIKE "%'.$search.'%"');
        $this->db->order_by('destination', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('*');
        $this->db->from('ongkos_kirim');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('ongkos_kirim', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('ongkos_kirim', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('ongkos_kirim');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}