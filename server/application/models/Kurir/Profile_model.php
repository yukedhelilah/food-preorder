<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Profile_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_details($id){
        $this->db->select('a.*, b.cityName');
        $this->db->from('user_jasakirim a');
        $this->db->join('ms_city b', 'a.cityID=b.id');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function check($check, $type){
        $this->db->select('a.*');
        if ($type == 'username') {
            $this->db->from('user_jasakirim a');
            $this->db->where('a.username', $check);
        } else if ($type == 'contactWa') {
            $this->db->from('user_jasakirim a');
            $this->db->where('a.contactWa', $check);
        } else if ($type == 'contactEmail') {
            $this->db->from('user_jasakirim a');
            $this->db->where('a.contactEmail', $check);
        } 
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            if ($type == 'username') {
                return [
                    'id'        => $row['id'],
                    'username'  => $row['username'],
                ];
            } else if ($type == 'contactWa') {
                return [
                    'id'            => $row['id'],
                    'contactWa'     => $row['contactWa'],
                ];
            } else if ($type == 'contactEmail') {
                return [
                    'id'            => $row['id'],
                    'contactEmail'  => $row['contactEmail'],
                ];
            }
        }
        
        return [];
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('user_jasakirim', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_city(){
        $this->db->select('*');
        $this->db->from('ms_city');
        $this->db->order_by('cityName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}