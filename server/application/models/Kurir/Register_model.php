<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Register_model extends CI_Model
{     
    function check($check, $type){
        $cek = explode("|", $check);
        $this->db->select('a.*');
        if ($type == 'branchName') {
            $this->db->from('resto a');
            $this->db->where(strtolower('a.restoName'), strtolower($cek[1]));
            $this->db->where(strtolower('a.branchName'), strtolower($cek[0]));
        } else if ($type == 'cpEmail') {
            $this->db->from('resto a');
            $this->db->where('a.cpEmail', $check);
        } else if ($type == 'cpMobile') {
            $this->db->from('resto a');
            $this->db->where('a.cpMobile', $check);
        } else if ($type == 'email') {
            $this->db->from('resto_staff a');
            $this->db->where('a.email', $check);
        } else if ($type == 'no_wa') {
            $this->db->from('resto_staff a');
            $this->db->where('a.no_wa', $check);
        }
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            if ($type == 'branchName') {
                return [
                    'id'            => $row['id'],
                    'restoName'     => $row['restoName'],
                    'branchName'    => $row['branchName'],
                ];
            } else if ($type == 'cpEmail') {
                return [
                    'id'        => $row['id'],
                    'cpEmail'   => $row['cpEmail'],
                ];
            } else if ($type == 'cpMobile') {
                return [
                    'id'        => $row['id'],
                    'cpMobile'  => $row['cpMobile'],
                ];
            } else if ($type == 'email') {
                return [
                    'id'        => $row['id'],
                    'email'     => $row['email'],
                ];
            } else if ($type == 'no_wa') {
                return [
                    'id'        => $row['id'],
                    'no_wa'     => $row['no_wa'],
                ];
            }
        }
        
        return [];
    }

    function getCity($id){
        $this->db->select('*');
        $this->db->from('ms_city');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if ($query->num_rows()) {
            $row = $query->row_array();
        }
        
        return $row['shortName'];
    }

    function add_resto($data){
        $this->db->insert('resto', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function add_staff($data){
        $this->db->insert('resto_staff', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function add_operational($data){
        $this->db->insert('resto_operational', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function add_shipment($data){
        $this->db->insert('resto_shipment', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
}