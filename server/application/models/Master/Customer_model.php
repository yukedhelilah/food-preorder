<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id, $limit, $start){
        $this->db->select('a.guestName, a.guestWa');
        $this->db->from('order a');
        $this->db->where('a.restoID', $id);
        $this->db->group_by('a.guestWa');
        $this->db->order_by('a.orderDate', 'desc');
        $this->db->limit($start, $limit);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.accountName, b.accountNo, c.paymentName');
                $this->db->from('order a');
                $this->db->join('resto_payment b', 'a.paymentID=b.id');
                $this->db->join('ms_payment c', 'b.paymentID=c.id');
                $this->db->where('a.restoID', $id);
                $this->db->where('a.guestWa', $value->guestWa);
                $this->db->order_by('a.orderDate', 'desc');

                $query_order    = $this->db->get();
                $order          = $query_order->result();

                $paid = 0; $unpaid = 0; $guestName; $orderDate; 
                foreach ($order as $keys => $val) {
                    $this->db->select('a.*, b.poDate');
                    $this->db->from('order_item a');
                    $this->db->join('product_po b', 'b.id=a.poID','left');
                    $this->db->where('a.orderID', $val->id);

                    $query_order_item    = $this->db->get();
                    $order_item          = $query_order_item->result();

                    $order[$keys] = [
                        'id'            => $val->id,
                        'orderNo'       => $val->orderNo,
                        'guestName'     => $val->guestName,
                        'guestWa'       => $val->guestWa,
                        'guestEmail'    => $val->guestEmail,
                        'guestAddress'  => $val->guestAddress, 
                        'guestCity'     => $val->guestCity,
                        'guestNote'     => $val->guestNote,
                        'guestShipment' => $val->guestShipment,
                        'paymentID'     => $val->paymentID,
                        'subtotal'      => $val->subtotal,
                        'discount'      => $val->discount,
                        'shipment'      => $val->shipment,
                        'total'         => $val->total,
                        'isStatus'      => $val->isStatus,
                        'orderDate'     => $val->orderDate,
                        'paymentName'   => $val->paymentName,
                        'accountName'   => $val->accountName,
                        'accountNo'     => $val->accountNo,
                        'order_item'    => $order_item
                    ];

                    if ($val->guestWa == $value->guestWa && $val->isStatus == 'Paid') { $paid += 1; }
                    if ($val->guestWa == $value->guestWa && $val->isStatus == 'Unpaid') { $unpaid += 1; }
                    if ($keys == 0) { $guestName = $val->guestName; $orderDate = $val->orderDate; }

                    $array[$key] = array(
                        'guestWa'       => $value->guestWa,
                        'guestName'     => $guestName,
                        'orderDate'     => $orderDate,
                        'paid'          => $paid,
                        'unpaid'        => $unpaid,
                        'order'         => $order,
                    );
                }
            }
            return $array;
        }
    }
}