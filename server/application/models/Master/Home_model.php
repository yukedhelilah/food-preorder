<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Home_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_resto($id){
        $this->db->select('a.branchName, b.cityName, a.isRegular, a.cutoffday, a.expiredDate, c.packageName');
        $this->db->from('resto a');
        $this->db->join('ms_city b','a.cityID=b.id');
        $this->db->join('ms_membership c','a.membershipID=c.id');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function get_summary($id){
        $this->db->select("(SELECT IFNULL(SUM(b.total), 0) FROM `order` b WHERE b.isStatus='Paid' AND DATE(b.orderDate) = '".date('Y-m-d', strtotime('now'))."') AS total");
        $this->db->select("(SELECT IFNULL(COUNT(b.id), 0) FROM `order` b WHERE DATE(b.orderDate) = '".date('Y-m-d', strtotime('now'))."') AS ordered");
        $this->db->select("(SELECT IFNULL(COUNT(b.id), 0) FROM `order` b WHERE b.isStatus='Paid' AND DATE(b.orderDate) = '".date('Y-m-d', strtotime('now'))."') AS paid");
        $this->db->select("(SELECT IFNULL(COUNT(b.id), 0) FROM `order` b WHERE b.isStatus='Unpaid' AND DATE(b.orderDate) = '".date('Y-m-d', strtotime('now'))."') AS unpaid");
        $this->db->from('order a');
        $this->db->where('a.restoID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function get_order($id){
        $this->db->select('a.*, b.accountName, b.accountNo, c.paymentName');
        $this->db->from('order a');
        $this->db->join('resto_payment b', 'a.paymentID=b.id');
        $this->db->join('ms_payment c', 'b.paymentID=c.id');
        $this->db->where('a.restoID', $id);
        $this->db->order_by('a.orderDate', 'desc');
        $this->db->limit(3);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.poDate');
                $this->db->from('order_item a');
                $this->db->join('product_po b', 'b.id=a.poID','left');
                $this->db->where('orderID', $value->id);

                $query2 = $this->db->get();
                $row2 = $query2->result();

                $array[$key] = array(
                    'id'            => $value->id,
                    'orderNo'       => $value->orderNo,
                    'guestName'     => $value->guestName,
                    'guestWa'       => $value->guestWa,
                    'guestEmail'    => $value->guestEmail,
                    'guestAddress'  => $value->guestAddress, 
                    'guestCity'     => $value->guestCity,
                    'guestNote'     => $value->guestNote,
                    'guestShipment' => $value->guestShipment,
                    'paymentID'     => $value->paymentID,
                    'subtotal'      => $value->subtotal,
                    'discount'      => $value->discount,
                    'shipment'      => $value->shipment,
                    'total'         => $value->total,
                    'isStatus'      => $value->isStatus,
                    'orderDate'     => $value->orderDate,
                    'paymentName'   => $value->paymentName,
                    'accountName'   => $value->accountName,
                    'accountNo'     => $value->accountNo,
                    'order_item'    => $row2,
                );
            }
            return $array;
        }
    }

    function editPO($data, $id){
        $this->db->where('id', $id);
        $this->db->update('resto', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}