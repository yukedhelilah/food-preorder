<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Kurir_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data(){
        $this->db->select('*');
        $this->db->from('user_jasakirim');
        $this->db->order_by('companyName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.*');
                $this->db->from('ongkos_kirim_category a');
                $this->db->where('a.userID', $value->id);
                $this->db->order_by('a.startFrom', 'asc');
                
                $query2 = $this->db->get();

                if (!$query2) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query2->num_rows()>0) {
                    $ongkir_category    = $query2->result();

                    $arr[$key] = [
                        'id'                => $value->id,
                        'companyName'       => $value->companyName,
                        'ongkir_category'   => $ongkir_category,
                    ];
                } else {
                    $arr[$key] = [
                        'id'                => $value->id,
                        'companyName'       => $value->companyName,
                        'ongkir_category'   => 0,
                    ];
                }
            }

            return $arr;
        }
    }

    function get_detail($id){
        $this->db->select('*');
        $this->db->from('ongkos_kirim');
        $this->db->where('categoryID', $id);
        $this->db->order_by('destination', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}