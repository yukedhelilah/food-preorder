<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{   

    function login($no_wa){
        $this->db->select('a.*, b.id as staffID, b.name as staffName, b.no_wa as staffWA, b.email as staffEmail, b.password');
        $this->db->from('resto a');
        $this->db->join('resto_staff b','a.id=b.restoID');
        $this->db->where('b.no_wa', $no_wa);
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey   = $row['username'] . '|' . date('Y-m-d H:i:s');
            $password   = $row['password'] == md5(md5(1234)) ? 'default' : 'updated';
            return [
                'restoID'       => $row['id'],
                'restoType'     => $row['isRegular'],
                'restoName'     => $row['restoName'],
                'staffID'       => $row['staffID'],
                'staffName'     => $row['staffName'],
                'staffWA'       => $row['staffWA'],
                'staffEmail'    => $row['staffEmail'],
                'staffPassword' => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function login_pin($no_wa, $password){
        $this->db->select('a.*, b.id as staffID, b.name as staffName, b.no_wa as staffWA, b.email as staffEmail, b.password');
        $this->db->from('resto a');
        $this->db->join('resto_staff b','a.id=b.restoID');
        $this->db->where('b.no_wa', $no_wa);
        $this->db->where('b.password', $password);
        $this->db->where('a.flag', 0);
        $this->db->where('b.flag', 0);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey   = $row['username'] . '|' . date('Y-m-d H:i:s');
            $password   = $row['password'] == md5(md5(1234)) ? 'default' : 'updated';
            return [
                'restoID'       => $row['id'],
                'restoType'     => $row['isRegular'],
                'restoName'     => $row['restoName'],
                'staffID'       => $row['staffID'],
                'staffName'     => $row['staffName'],
                'staffWA'       => $row['staffWA'],
                'staffEmail'    => $row['staffEmail'],
                'staffPassword' => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function activate($data, $id){
        $this->db->where('md5(md5(id))', $id);
        $this->db->update('resto', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function edit_staff($data, $id){
        $this->db->where('id', $id);
        $this->db->update('resto_staff', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}