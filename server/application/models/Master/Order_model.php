<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Order_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id, $limit, $start){
        $this->db->select('a.*, b.accountName, b.accountNo, c.paymentName');
        $this->db->from('order a');
        $this->db->join('resto_payment b', 'a.paymentID=b.id');
        $this->db->join('ms_payment c', 'b.paymentID=c.id');
        $this->db->where('a.restoID', $id);
        $this->db->order_by('a.orderDate', 'desc');
        $this->db->limit($start, $limit);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.poDate');
                $this->db->from('order_item a');
                $this->db->join('product_po b', 'b.id=a.poID','left');
                $this->db->where('orderID', $value->id);

                $query2 = $this->db->get();
                $row2 = $query2->result();

                $array[$key] = array(
                    'id'            => $value->id,
                    'orderNo'       => $value->orderNo,
                    'guestName'     => $value->guestName,
                    'guestWa'       => $value->guestWa,
                    'guestEmail'    => $value->guestEmail,
                    'guestAddress'  => $value->guestAddress, 
                    'guestCity'     => $value->guestCity,
                    'guestNote'     => $value->guestNote,
                    'guestShipment' => $value->guestShipment,
                    'paymentID'     => $value->paymentID,
                    'subtotal'      => $value->subtotal,
                    'discount'      => $value->discount,
                    'shipment'      => $value->shipment,
                    'total'         => $value->total,
                    'isStatus'      => $value->isStatus,
                    'orderDate'     => $value->orderDate,
                    'paymentName'   => $value->paymentName,
                    'accountName'   => $value->accountName,
                    'accountNo'     => $value->accountNo,
                    'order_item'    => $row2,
                );
            }
            return $array;
        }
    }

    function get_shipment($id){
        $this->db->select('a.*');
        $this->db->from('resto_shipment a');
        $this->db->where('a.restoID', $id);

        $query = $this->db->get();
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function get_data_filter($id, $guestShipment, $from, $to){
        $this->db->select('a.*, b.accountName, b.accountNo, c.paymentName');
        $this->db->from('order a');
        $this->db->join('resto_payment b', 'a.paymentID=b.id');
        $this->db->join('ms_payment c', 'b.paymentID=c.id');
        $this->db->where('a.restoID', $id);
        $this->db->where('DATE(a.orderDate) >=', date('Y-m-d', strtotime($from)));
        $this->db->where('DATE(a.orderDate) <=', date('Y-m-d', strtotime($to)));
        $this->db->where('a.restoID', $id);
        $this->db->where('a.guestShipment', $guestShipment);
        $this->db->order_by('a.orderDate', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.poDate');
                $this->db->from('order_item a');
                $this->db->join('product_po b', 'b.id=a.poID','left');
                $this->db->where('orderID', $value->id);

                $query2 = $this->db->get();
                $row2 = $query2->result();

                $array[$key] = array(
                    'id'            => $value->id,
                    'orderNo'       => $value->orderNo,
                    'guestName'     => $value->guestName,
                    'guestWa'       => $value->guestWa,
                    'guestEmail'    => $value->guestEmail,
                    'guestAddress'  => $value->guestAddress, 
                    'guestCity'     => $value->guestCity,
                    'guestNote'     => $value->guestNote,
                    'guestShipment' => $value->guestShipment,
                    'paymentID'     => $value->paymentID,
                    'subtotal'      => $value->subtotal,
                    'discount'      => $value->discount,
                    'shipment'      => $value->shipment,
                    'total'         => $value->total,
                    'isStatus'      => $value->isStatus,
                    'orderDate'     => $value->orderDate,
                    'paymentName'   => $value->paymentName,
                    'accountName'   => $value->accountName,
                    'accountNo'     => $value->accountNo,
                    'order_item'    => $row2,
                );
            }
            return $array;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('order');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_item($id){
        $this->db->where('orderID',$id);
        $this->db->delete('order_item');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('order', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function detilOrder_id($md5_id){
        $this->db->select('a.*, b.accountName, b.accountNo, b.note, c.paymentName, CONCAT("https://dapur123.com/files/logo-payment/",c.paymentLogo) paymentLogo, c.isTimelimit, d.restoName, d.url, e.waNo');
        $this->db->from('order a');
        $this->db->join('resto_payment b','a.paymentID=b.id');
        $this->db->join('ms_payment c','b.paymentID=c.id');
        $this->db->join('resto d','a.restoID=d.id');
        $this->db->join('resto_shipment e','e.restoID=a.restoID');
        $this->db->where('MD5(a.id)', $md5_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detilOrder_code($orderNo){
        $this->db->select('a.*, b.accountName, b.accountNo, b.note, c.paymentName, CONCAT("https://dapur123.com/files/logo-payment/",c.paymentLogo) paymentLogo, c.isTimelimit, d.restoName, d.url, e.waNo');
        $this->db->from('order a');
        $this->db->join('resto_payment b','a.paymentID=b.id');
        $this->db->join('ms_payment c','b.paymentID=c.id');
        $this->db->join('resto d','a.restoID=d.id');
        $this->db->join('resto_shipment e','e.restoID=a.restoID');
        $this->db->where('a.orderNo', $orderNo);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detilResto_url($url){
        $this->db->select('MD5(a.id) ids, a.id as restoID, a.url, a.restoName, a.logo, a.description, IF(a.isBranch=1, CONCAT("Cabang ",a.branchName),"") branch, b.cityName, a.address, a.latitude, a.longitude, a.email, a.phone, c.waNo, a.isRegular, a.cutoffday');
        $this->db->from('resto a');
        $this->db->join('ms_city b','a.cityID=b.id');
        $this->db->join('resto_shipment c','a.id=c.restoID');
        $this->db->where('a.url', $url);
        $this->db->where('a.expiredDate >=', date('Y-m-d'));
        $this->db->order_by('a.restoName', 'asc');
        $query = $this->db->get();
        return $query->row_array();
    }
}