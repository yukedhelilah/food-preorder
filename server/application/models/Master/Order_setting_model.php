<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Order_setting_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id){
        $this->db->select('*');
        $this->db->from('resto_shipment');
        $this->db->where('restoID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function edit($data, $id){
        $this->db->where('restoID', $id);
        $this->db->update('resto_shipment', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_delivery($id){
        $this->db->select('*');
        $this->db->from('resto_shipment_optional');
        $this->db->where('restoID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_delivery($data){
        $this->db->insert('resto_shipment_optional', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit_delivery($data, $id){
        $this->db->where('id', $id);
        $this->db->update('resto_shipment_optional', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_delivery($id){
        $this->db->where('id',$id);
        $this->db->delete('resto_shipment_optional');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_payment(){
        $this->db->select('*');
        $this->db->from('ms_payment');
        $this->db->order_by('paymentName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_method($id){
        $this->db->select('a.*, b.paymentName');
        $this->db->from('resto_payment a');
        $this->db->join('ms_payment b','a.paymentID=b.id');
        $this->db->where('a.restoID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_method_detail($id){
        $this->db->select('a.*, b.paymentName');
        $this->db->from('resto_payment a');
        $this->db->join('ms_payment b','a.paymentID=b.id');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add_method($data){
        $this->db->insert('resto_payment', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit_method($data, $id){
        $this->db->where('id', $id);
        $this->db->update('resto_payment', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_method($id){
        $this->db->where('id',$id);
        $this->db->delete('resto_payment');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}