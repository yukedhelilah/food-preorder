<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->order_by('categoryName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.name');
                $this->db->from('product a');
                $this->db->join('resto_staff b','a.createdBy=b.id');
                $this->db->where('a.flag', 0);
                $this->db->where('a.restoID', $id);
                $this->db->where('a.categoryID', $value->id);
                $this->db->order_by('a.productName', 'asc');
                
                $query2 = $this->db->get();

                if (!$query2) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query2->num_rows()>0) {
                    $product    = $query2->result();

                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => $product,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => 0,
                    ];
                }
            }

            return $arr;
        }
    }

    function search($id, $search){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->where('productName LIKE "%'.$search.'%"');
        $this->db->order_by('productName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_data_by_resto($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->order_by('categoryName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.name');
                $this->db->from('product a');
                $this->db->join('resto_staff b','a.createdBy=b.id');
                $this->db->where('a.restoID', $id);
                $this->db->where('a.categoryID', $value->id);
                $this->db->order_by('a.productName', 'asc');
                
                $query2 = $this->db->get();

                if (!$query2) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query2->num_rows()>0) {
                    $product    = $query2->result();

                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => $product,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => 0,
                    ];
                }
            }

            return $arr;
        }
    }

    function search_by_resto($id, $search){
        $this->db->select('a.*, b.name');
        $this->db->from('product a');
        $this->db->join('resto_staff b','a.createdBy=b.id');
        $this->db->where('a.restoID', $id);
        $this->db->where('a.productName LIKE "%'.$search.'%"');
        $this->db->order_by('a.productName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('product', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('product', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('product');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_data_po($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->order_by('categoryName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.name');
                $this->db->from('product a');
                $this->db->join('resto_staff b','a.createdBy=b.id');
                $this->db->where('a.flag', 0);
                $this->db->where('a.restoID', $id);
                $this->db->where('a.categoryID', $value->id);
                $this->db->order_by('a.productName', 'asc');
                
                $query_product = $this->db->get();

                if (!$query_product) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query_product->num_rows()>0) {
                    $product    = $query_product->result();

                    foreach ($product as $keys => $val) {
                        $this->db->select('a.*');
                        $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
                        $this->db->from('product_po a');
                        $this->db->where('a.productID', $val->id);
                        $this->db->order_by('a.poDate', 'asc');
                        $this->db->group_by('a.id');

                        $query_po = $this->db->get();

                        if ($query_po->num_rows() > 0) {
                            $po = $query_po->result();

                            $product[$keys] = [
                                'id'            => $val->id,
                                'restoID'       => $val->restoID,
                                'categoryID'    => $val->categoryID,
                                'productName'   => $val->productName,
                                'img'           => $val->img,
                                'description'   => $val->description,
                                'price'         => $val->price,
                                'createdDate'   => $val->createdDate,
                                'createdBy'     => $val->createdBy,
                                'available'     => $val->available,
                                'flag'          => $val->flag,
                                'po'            => $po,
                            ];
                        } else {
                            $product[$keys] = [
                                'id'            => $val->id,
                                'restoID'       => $val->restoID,
                                'categoryID'    => $val->categoryID,
                                'productName'   => $val->productName,
                                'img'           => $val->img,
                                'description'   => $val->description,
                                'price'         => $val->price,
                                'createdDate'   => $val->createdDate,
                                'createdBy'     => $val->createdBy,
                                'available'     => $val->available,
                                'flag'          => $val->flag,
                                'po'            => 0,
                            ];
                        }
                    }

                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => $product,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => 0,
                    ];
                }
            }

            return $arr;
        }
    }

    function search_po($id, $search){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->where('productName LIKE "%'.$search.'%"');
        $this->db->order_by('productName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $val) {
                $this->db->select('a.*');
                $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
                $this->db->from('product_po a');
                $this->db->where('a.productID', $val->id);
                $this->db->order_by('a.poDate', 'asc');
                $this->db->group_by('a.id');
                
                $query_po = $this->db->get();

                if ($query_po->num_rows() > 0) {
                    $po = $query_po->result();

                    $arr[$key] = [
                        'id'            => $val->id,
                        'restoID'       => $val->restoID,
                        'categoryID'    => $val->categoryID,
                        'productName'   => $val->productName,
                        'img'           => $val->img,
                        'description'   => $val->description,
                        'price'         => $val->price,
                        'createdDate'   => $val->createdDate,
                        'createdBy'     => $val->createdBy,
                        'available'     => $val->available,
                        'flag'          => $val->flag,
                        'po'            => $po,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $val->id,
                        'restoID'       => $val->restoID,
                        'categoryID'    => $val->categoryID,
                        'productName'   => $val->productName,
                        'img'           => $val->img,
                        'description'   => $val->description,
                        'price'         => $val->price,
                        'createdDate'   => $val->createdDate,
                        'createdBy'     => $val->createdBy,
                        'available'     => $val->available,
                        'flag'          => $val->flag,
                        'po'            => 0,
                    ];
                }
            }
            
            return $arr;
        }
    }

    function get_data_by_resto_po($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->order_by('categoryName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $value) {
                $this->db->select('a.*, b.name');
                $this->db->from('product a');
                $this->db->join('resto_staff b','a.createdBy=b.id');
                $this->db->where('a.restoID', $id);
                $this->db->where('a.categoryID', $value->id);
                $this->db->order_by('a.productName', 'asc');
                
                $query_product = $this->db->get();

                if (!$query_product) {
                    return ['error' => $this->db->error()];
                }
                
                if ($query_product->num_rows()>0) {
                    $product    = $query_product->result();

                    foreach ($product as $keys => $val) {
                        $this->db->select('a.*');
                        $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
                        $this->db->from('product_po a');
                        $this->db->where('a.productID', $val->id);
                        $this->db->where('a.poDate >=', date('Y-m-d', strtotime('now')));
                        $this->db->order_by('a.poDate', 'asc');
                        $this->db->group_by('a.id');
                        
                        $query_po = $this->db->get();

                        if ($query_po->num_rows() > 0) {
                            $po = $query_po->result();

                            $product[$keys] = [
                                'id'            => $val->id,
                                'restoID'       => $val->restoID,
                                'categoryID'    => $val->categoryID,
                                'productName'   => $val->productName,
                                'img'           => $val->img,
                                'description'   => $val->description,
                                'price'         => $val->price,
                                'createdDate'   => $val->createdDate,
                                'createdBy'     => $val->createdBy,
                                'available'     => $val->available,
                                'name'          => $val->name,
                                'flag'          => $val->flag,
                                'po'            => $po,
                            ];
                        } else {
                            $product[$keys] = [
                                'id'            => $val->id,
                                'restoID'       => $val->restoID,
                                'categoryID'    => $val->categoryID,
                                'productName'   => $val->productName,
                                'img'           => $val->img,
                                'description'   => $val->description,
                                'price'         => $val->price,
                                'createdDate'   => $val->createdDate,
                                'createdBy'     => $val->createdBy,
                                'available'     => $val->available,
                                'name'          => $val->name,
                                'flag'          => $val->flag,
                                'po'            => 0,
                            ];
                        }
                    }

                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => $product,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $value->id,
                        'categoryName'  => $value->categoryName,
                        'product'       => 0,
                    ];
                }
            }

            return $arr;
        }
    }

    function search_by_resto_po($id, $search){
        $this->db->select('a.*, b.name');
        $this->db->from('product a');
        $this->db->join('resto_staff b','a.createdBy=b.id');
        $this->db->where('a.restoID', $id);
        $this->db->where('a.productName LIKE "%'.$search.'%"');
        $this->db->order_by('a.productName', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            foreach ($row as $key => $val) {
                $this->db->select('a.*');
                $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
                $this->db->from('product_po a');
                $this->db->where('a.productID', $val->id);
                $this->db->where('a.poDate >=', date('Y-m-d', strtotime('now')));
                $this->db->order_by('a.poDate', 'asc');
                $this->db->group_by('a.id');
                
                $query_po = $this->db->get();

                if ($query_po->num_rows() > 0) {
                    $po = $query_po->result();

                    $arr[$key] = [
                        'id'            => $val->id,
                        'restoID'       => $val->restoID,
                        'categoryID'    => $val->categoryID,
                        'productName'   => $val->productName,
                        'img'           => $val->img,
                        'description'   => $val->description,
                        'price'         => $val->price,
                        'createdDate'   => $val->createdDate,
                        'createdBy'     => $val->createdBy,
                        'available'     => $val->available,
                        'name'          => $val->name,
                        'flag'          => $val->flag,
                        'po'            => $po,
                    ];
                } else {
                    $arr[$key] = [
                        'id'            => $val->id,
                        'restoID'       => $val->restoID,
                        'categoryID'    => $val->categoryID,
                        'productName'   => $val->productName,
                        'img'           => $val->img,
                        'description'   => $val->description,
                        'price'         => $val->price,
                        'createdDate'   => $val->createdDate,
                        'createdBy'     => $val->createdBy,
                        'available'     => $val->available,
                        'name'          => $val->name,
                        'flag'          => $val->flag,
                        'po'            => 0,
                    ];
                }
            }
            
            return $arr;
        }
    }

    function get_po($id){
        $this->db->select('a.*');
        $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
        $this->db->from('product_po a');
        $this->db->where('a.productID', $id);
        $this->db->where('a.poDate >=', date('Y-m-d', strtotime('now')));
        $this->db->order_by('poDate', 'desc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_po_detail($id){
        $this->db->select('a.*');
        $this->db->select("(SELECT IFNULL(SUM(b.qty), 0) FROM order_item b WHERE b.poID=a.id) AS sold");
        $this->db->from('product_po a');
        $this->db->where('a.id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add_po($data){
        $this->db->insert('product_po', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit_po($data, $id){
        $this->db->where('id', $id);
        $this->db->update('product_po', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_po($id){
        $this->db->where('id',$id);
        $this->db->delete('product_po');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function checkPO($id, $poDate){
        $this->db->select('*');
        $this->db->from('product_po');
        $this->db->where('productID', $id);
        $this->db->where('poDate', date('Y-m-d', strtotime($poDate)));

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        } else {
            return 0;
        }
    }

    function get_product_by_category($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('categoryID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        } else {
            return 0;
        }
    }
}