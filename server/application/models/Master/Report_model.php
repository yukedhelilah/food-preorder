<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Report_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id, $orderDate){
        $this->db->select('a.*, b.restoName, b.branchName, c.cityName');
        $this->db->from('order a');
        $this->db->join('resto b', 'a.restoID=b.id');
        $this->db->join('ms_city c', 'b.cityID=c.id');
        $this->db->where('a.restoID', $id);
        $this->db->where('a.isStatus', 'Paid');
        $this->db->where('DATE(a.orderDate)', date('Y-m-d', strtotime($orderDate)));
        $this->db->order_by('a.orderDate', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.*');
                $this->db->from('order_item a');
                $this->db->where('a.orderID', $value->id);

                $query2 = $this->db->get();
                $row2 = $query2->result();

                $array[$key] = array(
                    'id'            => $value->id,
                    'restoName'     => $value->restoName,
                    'branchName'    => $value->branchName,
                    'cityName'      => $value->cityName,
                    'orderNo'       => $value->orderNo,
                    'guestName'     => $value->guestName,
                    'guestWa'       => $value->guestWa,
                    'guestShipment' => $value->guestShipment,
                    'guestAddress'  => $value->guestAddress,
                    'isStatus'      => $value->isStatus,
                    'orderDate'     => $value->orderDate,
                    'order_item'    => $row2,
                );
            }
            return $array;
        }
    }

    function get_data_po($id, $orderDate){
        $this->db->select('c.id, d.restoName, d.branchName, e.cityName, c.guestName, c.guestWa, c.guestShipment, c.guestAddress, c.isStatus, a.poDate');
        $this->db->from('product_po a');
        $this->db->join('order_item b', 'b.poID=a.id');
        $this->db->join('order c', 'c.id=b.orderID');
        $this->db->join('resto d', 'd.id=c.restoID');
        $this->db->join('ms_city e', 'd.cityID=e.id');
        $this->db->where('c.restoID', $id);
        $this->db->where('DATE(a.poDate)', date('Y-m-d', strtotime($orderDate)));
        $this->db->where('c.isStatus', 'Paid');
        $this->db->group_by('c.guestWa');
        $this->db->order_by('c.orderDate', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();

            $array = [];
            foreach ($row as $key => $value) {
                $this->db->select('a.productName, a.qty, a.additional');
                $this->db->from('order_item a');
                $this->db->join('order b', 'a.orderID=b.id');
                $this->db->join('product_po c', 'a.poID=c.id');
                $this->db->join('product d', 'd.id=c.productID');
                $this->db->where('DATE(c.poDate)', date('Y-m-d', strtotime($orderDate)));
                $this->db->where('b.restoID', $id);
                $this->db->where('b.isStatus', 'Paid');
                $this->db->where('b.guestWa', $value->guestWa);

                $query2 = $this->db->get();
                $row2 = $query2->result();

                $array[$key] = array(
                    'id'            => $value->id,
                    'restoName'     => $value->restoName,
                    'branchName'    => $value->branchName,
                    'cityName'      => $value->cityName,
                    'guestName'     => $value->guestName,
                    'guestWa'       => $value->guestWa,
                    'guestShipment' => $value->guestShipment,
                    'guestAddress'  => $value->guestAddress,
                    'isStatus'      => $value->isStatus,
                    'orderDate'     => $value->poDate,
                    'order_item'    => $row2,
                );
            }
            // echo $this->db->last_query();exit();
            return $array;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('order');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_item($id){
        $this->db->where('orderID',$id);
        $this->db->delete('order_item');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('order', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function detilOrder_id($md5_id){
        $this->db->select('a.*, b.accountName, b.accountNo, b.note, c.paymentName, CONCAT("https://dapur123.com/files/logo-payment/",c.paymentLogo) paymentLogo, c.isTimelimit, d.restoName, d.url, e.waNo');
        $this->db->from('order a');
        $this->db->join('resto_payment b','a.paymentID=b.id');
        $this->db->join('ms_payment c','b.paymentID=c.id');
        $this->db->join('resto d','a.restoID=d.id');
        $this->db->join('resto_shipment e','e.restoID=a.restoID');
        $this->db->where('MD5(a.id)', $md5_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detilOrder_code($orderNo){
        $this->db->select('a.*, b.accountName, b.accountNo, b.note, c.paymentName, CONCAT("https://dapur123.com/files/logo-payment/",c.paymentLogo) paymentLogo, c.isTimelimit, d.restoName, d.url, e.waNo');
        $this->db->from('order a');
        $this->db->join('resto_payment b','a.paymentID=b.id');
        $this->db->join('ms_payment c','b.paymentID=c.id');
        $this->db->join('resto d','a.restoID=d.id');
        $this->db->join('resto_shipment e','e.restoID=a.restoID');
        $this->db->where('a.orderNo', $orderNo);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detilResto_url($url){
        $this->db->select('MD5(a.id) ids, a.id as restoID, a.url, a.restoName, a.logo, a.description, IF(a.isBranch=1, CONCAT("Cabang ",a.branchName),"") branch, b.cityName, a.address, a.latitude, a.longitude, a.email, a.phone, c.waNo, a.isRegular, a.cutoffday');
        $this->db->from('resto a');
        $this->db->join('ms_city b','a.cityID=b.id');
        $this->db->join('resto_shipment c','a.id=c.restoID');
        $this->db->where('a.url', $url);
        $this->db->where('a.expiredDate >=', date('Y-m-d'));
        $this->db->order_by('a.restoName', 'asc');
        $query = $this->db->get();
        return $query->row_array();
    }
}