<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Schedule_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id){
        $this->db->select('*');
        $this->db->from('resto_operational');
        $this->db->where('restoID', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function edit($data, $id){
        $this->db->where('restoID', $id);
        $this->db->update('resto_operational', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function get_schedule($id){
        $this->db->select('*');
        $this->db->from('resto_operational_close');
        $this->db->where('restoID', $id);
        $this->db->order_by('closeDate', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    // function get_details($id){
    //     $this->db->select('a.*, b.cityName');
    //     $this->db->from('resto a');
    //     $this->db->join('ms_city b', 'a.cityID=b.id');
    //     $this->db->where('a.id', $id);

    //     $query = $this->db->get();
        
    //     if (!$query) {
    //         return ['error' => $this->db->error()];
    //     }
        
    //     if ($query->num_rows()>0) {
    //         $row = $query->row_array();
    //         return $row;
    //     }
    // }

    function add_schedule($data){
        $this->db->insert('resto_operational_close', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit_schedule($data, $id){
        $this->db->where('int', $id);
        $this->db->update('resto_operational_close', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_schedule($id){
        $this->db->where('int',$id);
        $this->db->delete('resto_operational_close');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}