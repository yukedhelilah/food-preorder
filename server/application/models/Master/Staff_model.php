<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Staff_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function check($check, $restoID){
        $this->db->select('*');
        $this->db->from('resto_staff');
        $this->db->where('restoID', $restoID);
        $this->db->where('username', $check);
        
        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()) {
            $row = $query->row_array();
            return [
                'id'        => $row['id'],
                'username'  => $row['username'],
            ];
        }
        
        return [];
    }

    function get_data($id){
        $this->db->select('*');
        $this->db->from('resto_staff');
        $this->db->where('flag', 0);
        $this->db->where('restoID', $id);
        $this->db->order_by('name', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('*');
        $this->db->from('resto_staff');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function add($data){
        $this->db->insert('resto_staff', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('resto_staff', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('resto_staff');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}