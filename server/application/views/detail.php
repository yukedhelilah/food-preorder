<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Foogra - Discover & Book the best restaurants at the best price">
    <meta name="author" content="Ansonika">
   

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="css/bootstrap_customized.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="css/detail-page.css" rel="stylesheet">
    <title><?=$detail['restoName'] . ' ' . $detail['cityName']?></title>
</head>
<header class="header clearfix element_to_stick">
		<div class="container">
		<div id="logo">
			<a href="index.html">
				<img src="img/logo.svg" width="140" height="35" alt="" class="logo_normal">
				<img src="img/logo_sticky.svg" width="140" height="35" alt="" class="logo_sticky">
			</a>
		</div>
		<ul id="top_menu">
      <!-- <li><a href="#sign-in-dialog" id="sign-in" class="login">Sign In</a></li>
      <li><a href="wishlist.html" class="wishlist_bt_top" title="Your wishlist">Your wishlist</a></li> -->
    </ul>
    <!-- /top_menu -->
    <a href="#0" class="open_close">
      <img src="img/123.png"  width="35" height="35" onclick="history.back();" class="btn-two green small"></a></i>
    </a>
	</div>
	</header>
	<!-- /header -->
	
<body>
	<main>
		<div class="hero_in detail_page background-image" data-background="url(img/hai.jpg)">
			<div class="wrapper opacity-mask" data-opacity-mask="rgba(0, 0, 0, 0.5)">
				<div class="container">
					<div class="main_info">
						<div class="row">
						</div>
						<!-- /row -->
					</div>
					<!-- /main_info -->
				</div>
			</div>
		</div>
		<!--/hero_in-->
		<div class="container margin_detail">
		  <div class="row">
		    <div class="col-lg-8">
        <div class="search_bar_list">
            <input type="text" class="form-control" placeholder="Enter the menu you want" id="search">
            <input type="submit" value="Search" onclick="search()">
          </div>
        <div class="tabs_detail">
        <div class="tab-content" role="tablist">
          <div id="menuList"></div>
		    </div>
		  </div>
		</div>
<div class="modal fade" id="detil-menu" tabindex="-1" role="dialog" aria-labelledby="dm_productName" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="text-center">
            <img src="img/thumb_detail_1.jpg" width="70%" title="Photo title" data-effect="mfp-zoom-in" id="dm_img">
            <div class="card-body info_content">                
              <h2 id="dm_productName">...</h2>
              <div class="menu_item">                    
                <p id="dm_description"></p>
              </div>
              <h3 id="dm_price" style="color: #8FBC8F;"></h3>
              <!-- <button type="button" class="btn_1 full-width mb_5">Order</button> -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="javascript:;" class="gplus-share" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>

	</main>
  <img src="img/top.png"  width="50" height="50" onclick="topFunction()" id="myBtn" title="Go to top" style="float:right; margin: 20px"  >
		<!--form -->
	</div>
  </div>


    <script src="js/common_scripts.min.js"></script>
    <script src="js/common_func.js"></script>
    <script src="assets/validate.js"></script>

    <!-- SPECIFIC SCRIPTS -->
    <script src="js/sticky_sidebar.min.js"></script>
    <script src="js/specific_detail.js"></script>
	<script src="js/datepicker.min.js"></script>
	<script src="js/datepicker_func_1.js"></script>

  <script src="function.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
      'use strict';
    //   $.cookieBar({
    //     fixed: true
    //   });

      detailResto();
    });

    function topFunction() {
        document.body.scrollTop = 0; //
        document.documentElement.scrollTop = 0; // 
    }

    function detailResto(){
      if(typeof getUrlParameter('id') === "undefined"){
        alert('Error! This restaurant is not exist');
        window.location = 'home';
      }else{
        $.ajax({
          type: 'GET',
          url: getUrlServer('server')+'main/getRestoDetail',
          dataType: 'json',
          data: {
            'code'  : getUrlParameter('id')
          },
          success: function(responses) {
            if(responses.status == true){
              var data = responses.data;
              $(document).attr("title", data.restoName+' '+data.cityName); 
              $('#appName').html('&nbsp;&nbsp;'+data.restoName.toUpperCase());
              $('#txt_restoName').html(data.restoName.toUpperCase());
              $('#txt_restoAddress').html(data.address);
              var logo = data.logo == '' ? 'assets/img/placehold.jpg' : 'http://creatrixorganizer.com/resto/files/logo/'+data.logo;
              $('#txt_logo').attr("src",logo);
              $('#txt_restoDescription').html(`
                <p>`+ data.restoName +`<br>
                  `+ data.cityName +`</p>`);

              allMenues();
            }else{
              alert('Error! This restaurant is not exist');
              window.location = 'index';
            }
          }
        });
      }
    }

    function allMenues(){
      $.ajax({
        type: 'GET',
        url: getUrlServer('server')+'main/getCategory',
        dataType: 'json',
        data: {
          'id'  : getUrlParameter('id')
        },
        success: function(responses) {
          $('#menuList').empty();
          if(responses.status == true){
            $.each(responses.data, function(a, b){
              $('#menuList').append(`
              <p>
              <a href="menu?id=`+ b.id +`">
                <h4>`+ b.categoryName +`</h4>
                </div>
                </p>
              `);
            });
          }
        }
      });
    }

    function detilmenu(id){
      $.ajax({
        type: 'GET',
        url: getUrlServer('server')+'main/getManuDetail',
        dataType: 'json',
        data: {
          'id'  : id
        },
        success: function(responses) {
          var data = responses.data;
          $('#dm_productName').html(data.productName);
          $('#dm_description').html(data.description);
          $('#dm_price').html(data.price);
          var img = data.img == '' ? 'img/lazy-placeholder.png' : 'http://creatrixorganizer.com/resto/files/product/'+data.img;
          $('#dm_img').attr('src', img);
        }
      });
      $('#detil-menu').modal('show');
    }

    function category_detail(id){
      $.ajax({
        type: 'GET',
        url: getUrlServer('server')+'main/getManues',
        dataType: 'json',
        data: {
          'id'  : id
        },
        success: function(responses) {
          $('#menuList').empty();
          if(responses.status == true){
            $.each(responses.data, function(a, b){
              $('#menuList').append(`
                <a href="menu?id=`+ b.id +`">
                  <h3>`+ b.categoryName +`</h3>
                </a>
              `);
            });
          }
        },
      });
    }

    function search(){
      $.ajax({
        type: 'GET',
        url: getUrlServer('server')+'main/searchMenu',
        dataType: 'json',
        data: {
          'search'  : $('#search').val(),
          'resto'  : getUrlParameter('id')
        },
        success: function(responses) {
          $('#menuList').empty();
          if (responses.total != 0){
            $.each(responses.data, function(a, b){
              $('#menuList').append(`
              <div style="float:right;width:100%;padding:2%;" onclick="detilmenu(`+b.id+`)">
              <table style="width:100%" >
              <tr>
                <td>`+ b.productName +`</td>
                <td style="text-align:right"><b>`+ b.price +`</b></td>
              </tr>
              </table>
		          </div>
		            </div>`);
            });
          } else {
            $('#menuList').append(`
		            <div class="menu_item">
                <div style="float:right;width:100%;padding:2%;" >
                  <a>Menu not available</a>
		            </div>`);
          }
        },
      });
    }
  </script>
</body>
</html>